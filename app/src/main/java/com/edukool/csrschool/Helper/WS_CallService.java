package com.edukool.csrschool.Helper;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */
public class WS_CallService {
    Context ctx;

    static String response;
    JSONObject json;
    static HttpResponse httpResponse;
    public String responserstring;
    static InputStream inputstream;
    static String json_ar;
    int i = 0;

    public WS_CallService(Context ctx) {
        this.ctx = ctx;
    }




    public String getJSONObjestString(List<NameValuePair> select, String url)
            throws JSONException {
        try {
            json_ar = null;
            httpResponse = null;
            responserstring = "";
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("WSH",
                    "U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
            httpPost.setEntity(new UrlEncodedFormEntity(select));
            HttpParams httpParameters = httpPost.getParams();
            int timeoutConnection = 60 * 10000;
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 60 * 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            inputstream = httpEntity.getContent();

            Log.e("URL is ","-->"+url);

        } catch (Exception e) {
            //e.printStackTrace();
            if(e!=null) {
                System.out.println("Exception-->" + e.toString());

            }else{
                System.out.println("Exception-->" + "null");
            }
        }
        try {
//            if (httpResponse.getStatusLine().getStatusCode() != 500) {
            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(inputstream, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                inputstream.close();
                response = sb.toString();
//                    Log.e("tage", response);
            } catch (Exception e) {
                if(e!=null) {
                    System.out.println("Exception-->" + e.toString());
                }else{
                    System.out.println("Exception-->" + "null");
                }
            }
            json_ar = new String(response);
            responserstring = ""
                    + httpResponse.getStatusLine().getStatusCode();
            System.out.println("Json string-->"+json_ar);
            return json_ar;
//            } else {
//                responserstring = ""
//                        + httpResponse.getStatusLine().getStatusCode();
//                System.out.println("Json string-->"+json_ar);
//                return json_ar;
//            }
        } catch (Exception e) {
            return json_ar;
        }
    }

    public String getJSONObjestStringNew(List<NameValuePair> select, String url)
            throws JSONException {
        try {
            json_ar = null;
            httpResponse = null;
            responserstring = "";
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type",
                    "application/x-www-form-urlencoded");
            httpPost.setEntity(new UrlEncodedFormEntity(select));
            HttpParams httpParameters = httpPost.getParams();
            int timeoutConnection = 60 * 10000;
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 60 * 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            inputstream = httpEntity.getContent();


        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (httpResponse.getStatusLine().getStatusCode() != 500) {
                try {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputstream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputstream.close();
                    response = sb.toString();
//                    Log.e("tage", response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                json_ar = new String(response);
                responserstring = ""
                        + httpResponse.getStatusLine().getStatusCode();
                System.out.println("Json string-->"+json_ar);
                return json_ar;
            } else {
                responserstring = ""
                        + httpResponse.getStatusLine().getStatusCode();
                System.out.println("Json string-->"+json_ar);
                return json_ar;
            }
        } catch (Exception e) {
            return json_ar;
        }
    }

}
