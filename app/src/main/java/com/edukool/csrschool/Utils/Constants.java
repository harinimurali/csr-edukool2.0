package com.edukool.csrschool.Utils;

/**
 * Created by keerthana on 8/24/2018.
 */

public class Constants {
    public static final String NUMBER = "PhoneNumber";
    public static final String SCHOOLID = "schoolid";
    public static final String INTENT_COUNTRY_CODE = "code";


    //Appreferences data
    public static final String FATHERNAME = "fathername";
    public static final String PROFILEIMAGE = "profileimage";
    public static final String CHILDREN_PROFILE = "children_profile";
    public static final String LANGUAGE_CHANGE = "change";
    public static final String LANGUAGE = "language";
    public static final String FCMTOKEN = "fcmtoken";
    public static final String SCHOOL_NAME = "schoolname";
    public static final String NOTIFICATION_COUNT = "notification";
    public static final String VERSION_CODE = "versioncode";

    //Tamil url
    public static final String News_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getnewstam";
    public static final String Message_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getmessagestam&studID=";
    public static final String Contact_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/contacts/getcontacttam&studID=";
    public static final String Timetable_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/gettimetabletam&studID=";
    public static final String Voice_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getvoicenotetam&studID=";
    public static final String Gallery_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getalbum";
    public static final String ExamSchedule_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getexamscheduletam&studID=";
    public static final String Profile_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getprofiledatatam&studID=";
    public static final String LiveStream_tamil_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getlivestreaming";
    // public static final String LiveStream_tamil_url = "";
    public static final String About_tamil_url = "http://www.csrschoolindia.org/";

    //English url
    public static final String News_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getnews";
    public static final String Message_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getmessages&studID=";
    public static final String Contact_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/contacts/getcontact&studID=";
    public static final String Timetable_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/gettimetable&studID=";
    public static final String Voice_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getvoicenote&studID=";
    public static final String Gallery_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getalbum";
    public static final String ExamSchedule_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getexamschedule&studID=";
    public static final String Profile_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getprofiledata&studID=";
    public static final String LiveStream_english_url = "http://csrschool.edukool.com/edukool/web/index.php?r=admin/news/getlivestreaming";
    // public static final String Bustracking_tamil_url = "";
    public static final String About_english_url = "http://www.csrschoolindia.org/";


    //Tamil url primary
    public static final String News_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getnewstam";
    public static final String Message_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getmessagestam&studID=";
    public static final String Contact_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/contacts/getcontacttam&studID=";
    public static final String Timetable_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/gettimetabletam&studID=";
    public static final String Voice_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getvoicenotetam&studID=";
    public static final String Gallery_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getalbum";
    public static final String ExamSchedule_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getexamscheduletam&studID=";
    public static final String Profile_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getprofiledatatam&studID=";
    public static final String LiveStream_tamil_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getlivestreaming";
    // public static final String LiveStream_tamil_url_prm = "";

    //English url primary
    public static final String News_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getnews";
    public static final String Message_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getmessages&studID=";
    public static final String Contact_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/contacts/getcontact&studID=";
    public static final String Timetable_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/gettimetable&studID=";
    public static final String Voice_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getvoicenote&studID=";
    public static final String Gallery_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getalbum";
    public static final String ExamSchedule_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getexamschedule&studID=";
    public static final String Profile_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getprofiledata&studID=";
    public static final String LiveStream_english_url_prm = "http://csrprimaryschool.edukool.com/edukool/web/index.php?r=admin/news/getlivestreaming";
    // public static final String Bustracking_tamil_url_prm = "";

}

