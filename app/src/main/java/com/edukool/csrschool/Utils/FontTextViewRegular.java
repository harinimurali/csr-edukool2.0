package com.edukool.csrschool.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by keerthana on 8/24/2018.
 */

public class FontTextViewRegular extends TextView {
    public FontTextViewRegular(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }

    public FontTextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }

    public FontTextViewRegular(Context context, AttributeSet attrs,
                               int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }
}