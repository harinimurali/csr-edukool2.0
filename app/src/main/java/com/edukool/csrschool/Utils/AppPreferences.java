package com.edukool.csrschool.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import com.edukool.csrschool.models.ChildrenProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class AppPreferences {
    public static void setParentName(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.FATHERNAME, value);
        ed.commit();
    }

    public static String getParentName(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.FATHERNAME, "");

    }

    public static void setParentNumber(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(PreferenceKey.PHONE_NUMBER, value);
        ed.commit();
    }

    public static String getParentNumber(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PreferenceKey.PHONE_NUMBER, "");

    }

    public static void setStudentPosition(Context context, int value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putInt(PreferenceKey.STUDENT_POSITION, value);
        ed.commit();
    }

    public static int getSudentPosition(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt(PreferenceKey.STUDENT_POSITION, 0);

    }

    public static void setParentImgae(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.PROFILEIMAGE, value);
        ed.commit();
    }

    public static String getFatherImage(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.PROFILEIMAGE, "");

    }

    public static void setversioncode(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.VERSION_CODE, String.valueOf(value));
        ed.commit();
    }

    public static String getversioncode(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.VERSION_CODE, "");

    }

    public static void setLanguage(Context context, int value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.LANGUAGE_CHANGE, String.valueOf(value));
        ed.commit();
    }

    public static String getLanguage(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.LANGUAGE_CHANGE, "");

    }

    public static void setFCMToken(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.FCMTOKEN, String.valueOf(value));
        ed.commit();
    }

    public static String getFcmToken(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.FCMTOKEN, "");

    }

    public static void setSchool_Name(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.SCHOOL_NAME, String.valueOf(value));
        ed.commit();
    }

    public static String getSchool_Name(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.SCHOOL_NAME, "");

    }

    public static void setNotificationCount(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.NOTIFICATION_COUNT, value);
        ed.commit();
    }

    public static String getNotificationCount(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.NOTIFICATION_COUNT, "0");

    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PreferenceKey.LOGIN_STATUS, false);
    }

    public static void saveLoginData(Context context, boolean isLoggedIn, String parentname,
                                     String profile, String id, String classid, String classD,
                                     String section, String fees, String phonenumber, String encrypId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(PreferenceKey.LOGIN_STATUS, isLoggedIn);
        ed.putString(PreferenceKey.ID, id);
        ed.putString(PreferenceKey.PARENT_NAME, parentname);
        ed.putString(PreferenceKey.PROFILE_IMAGE, profile);
        ed.putString(PreferenceKey.CLASSID, classid);
        ed.putString(PreferenceKey.CLASS, classD);
        ed.putString(PreferenceKey.SECTION, section);
        ed.putString(PreferenceKey.ENCRYPSTUDENTID, encrypId);
      /*  ed.putString(PreferenceKey.FEESID, feesid);
        ed.putString(PreferenceKey.FEES_AMOUNT, feesamt);
        ed.putString(PreferenceKey.FEESHOWFROM, feefrom);
        ed.putString(PreferenceKey.FEESHOWTO, feesto);*/
        ed.putString(PreferenceKey.FEES_STATUS, fees);
        ed.putString(PreferenceKey.FEES_STATUS, phonenumber);
        ed.commit();
    }


    public static void setchildrenProfile(Context c, List<ChildrenProfile> cards) {
        if (cards != null) {
            String json = new Gson().toJson(cards, new TypeToken<List<ChildrenProfile>>() {
            }.getType());
            PreferenceManager.getDefaultSharedPreferences(c).
                    edit().putString(Constants.CHILDREN_PROFILE, json).commit();
        }
    }

    public static List<ChildrenProfile> getchildrenProfile(Context c) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
        String placesJson = sp.getString(Constants.CHILDREN_PROFILE, "");
        List<ChildrenProfile> cards;
        cards = new Gson().fromJson(placesJson, new TypeToken<List<ChildrenProfile>>() {
        }.getType());
        return cards;
    }

}

