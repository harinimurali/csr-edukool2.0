package com.edukool.csrschool.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by keerthana on 8/24/2018.
 */

public class FontEditText extends EditText {

    public FontEditText(Context context) {
        super(context);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }

    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }

    public FontEditText(Context context, AttributeSet attrs,
                        int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(HelveticaFont.getInstance(context).getRegularTypeFace());
    }

}
