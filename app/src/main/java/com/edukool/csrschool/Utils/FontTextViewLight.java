package com.edukool.csrschool.Utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by keerthana on 8/24/2018.
 */

public class FontTextViewLight extends android.support.v7.widget.AppCompatTextView {
    public FontTextViewLight(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getLightTypeFace());
    }

    public FontTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getLightTypeFace());
    }

    public FontTextViewLight(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getLightTypeFace());
    }
}
