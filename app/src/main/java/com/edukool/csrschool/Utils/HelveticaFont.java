package com.edukool.csrschool.Utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by harini on 6/12/2018.
 */

public class HelveticaFont {
    private static HelveticaFont instance;
    private static Typeface typeface;
    private static Typeface typeface_semi_bold;
    private static Typeface typeface_medium;
    private static Typeface typeface_light;


    public static HelveticaFont getInstance(Context context) {
        synchronized (HelveticaFont.class) {
            if (instance == null) {
                instance = new HelveticaFont();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "helvetica_regular.TTF");
                typeface_semi_bold = Typeface.createFromAsset(context.getResources().getAssets(), "helvetica_bold.TTF");
                typeface_light = Typeface.createFromAsset(context.getResources().getAssets(), "helvetica_light.TTF");
                typeface_medium= Typeface.createFromAsset(context.getResources().getAssets(), "helvetica_bold.TTF");
            }
            return instance;
        }
    }

    public Typeface getRegularTypeFace() {
        return typeface;
    }
    public Typeface getSemiBoldTypeFace() {
        return typeface_semi_bold;
    }
    public Typeface getLightTypeFace() {
        return typeface_light;
    }
    public Typeface getMediumBoldTypeFace() {
        return typeface_medium;
    }
}
