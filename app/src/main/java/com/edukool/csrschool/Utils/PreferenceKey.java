package com.edukool.csrschool.Utils;

/**
 * Created by keerthana on 8/24/2018.
 */

public class PreferenceKey {
    public static final String LOGIN_STATUS = "login_status";
    public static final String ID = "id";
    public static final String PARENT_NAME = "parent_name";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String CLASSID = "classid";
    public static final String CLASS = "class";
    public static final String SECTION = "section";
    public static final String FEESID = "feesid";
    public static final String FEES_AMOUNT = "feesamount";
    public static final String FEESHOWFROM = "feeshowfrom";
    public static final String FEESHOWTO = "feeshowto";
    public static final String FEES_STATUS = "fees";
    public static final String PHONE_NUMBER = "phonenumber";
    public static final String STUDENT_POSITION = "studentposition";
    public static final String ENCRYPSTUDENTID = "encrypstudentid";
}