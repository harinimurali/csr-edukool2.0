package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewRegular;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.LeaveRequestList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.MyViewHolder> {
    Context context;

    private List<LeaveRequestList> moviesList;
    AddTouchListen listen;



    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
        //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.listen = addTouchListen;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewMedium message;
        public FontTextViewRegular remark, date;

        public FontTextViewSemibold status;
        RelativeLayout layout;

        public MyViewHolder(View view) {
            super(view);
            message = view.findViewById(R.id.message);
            remark = view.findViewById(R.id.remarks);
            status = view.findViewById(R.id.status);
            date = view.findViewById(R.id.date);
            layout = view.findViewById(R.id.layout1);
            // mremarks = (FontTextViewLight) view.findViewById(R.id.remarks);

        }
    }

    public LeaveAdapter(Context context, List<LeaveRequestList> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        LeaveRequestList movie = moviesList.get(position);

        //  holder.mSubject.setText(movie.getSubject());
        holder.message.setText(movie.getMessage());
        if (movie.getApprovedStatus().equals("0")) {
            holder.status.setText(context.getResources().getString(R.string.rejected));
        } else if (movie.getApprovedStatus().equals("1")) {
            holder.status.setText(context.getResources().getString(R.string.approved));
        } else {
            holder.status.setText(context.getResources().getString(R.string.inprogress));
        }
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        Date date1 = null;
        String str = null;
        String str1 = null;

        try {
            date = inputFormat.parse(movie.getFromDate());
            date1 = inputFormat.parse(movie.getToDate());
            str = outputFormat.format(date);
            str1 = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.date.setText(str + " - " + str1);
        // holder.date.setText(movie.getFromDate() + " to " + movie.getToDate());
        if (movie.getTeacherRemarks().equals("")) {
            holder.remark.setText("Teacher Remark: No remarks");
        } else {
            holder.remark.setText("Teacher Remark: " + movie.getTeacherRemarks());
        }
       /* holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });*/
        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listen.onTouchClick(position);
               /* AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.delete);
                builder.setMessage(R.string.sure)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                listen.onTouchClick(position);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();*/
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
        // return 3;
    }
}
