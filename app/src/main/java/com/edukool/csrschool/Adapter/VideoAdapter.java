package com.edukool.csrschool.Adapter;

import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.models.YoutubeVideos;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by keerthana on 8/24/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {
    List<YoutubeVideos> youtubeVideoList;
    private YoutubeVideos movie;
    Context mCtx;
    private MediaController mediacontroller;
    private Uri uri;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private Lifecycle lifecycle;

    public VideoAdapter() {
    }

    public VideoAdapter(Context context, List<YoutubeVideos> youtubeVideoList) {
        this.youtubeVideoList = youtubeVideoList;
        this.mCtx = context;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, final int position) {
     /*   mediacontroller = new MediaController(mCtx);
        mediacontroller.setAnchorView(holder.videoWeb);*/
     /*  webview work holder.videoWeb.setWebChromeClient(new WebChromeClient() {
        });
        holder.videoWeb.getSettings().setJavaScriptEnabled(true);
        holder.videoWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        holder.videoWeb.getSettings().setSupportMultipleWindows(true);
        holder.videoWeb.setWebChromeClient(new WebChromeClient());
        holder.videoWeb.setHorizontalScrollBarEnabled(false);*/
        //String playVideo= "<html><body>Youtube video .. <br> <iframe class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"385\" src=\""+youtubeVideoList.get(position).getVideoUrl()+" frameborder=\"0\"></body></html>";

        //  holder.videoWeb.loadData(playVideo, "text/html", "utf-8");


      /*  holder.videoWeb.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = holder.videoWeb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        holder.videoWeb.loadUrl(youtubeVideoList.get(position).getVideoUrl());
     //   holder.videoWeb.loadData(youtubeVideoList.get(position).getVideoUrl(), "text/html; video/mpeg", "UTF-8");
        // holder.videoWeb.setWebViewClient(new DetailWebViewClient());

        //  holder.videoWeb.loadData(" <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe> ","text/html" , "utf-8" );
       *//* holder.videoWeb.setVideoPath(youtubeVideoList.get(position).getVideoUrl());
        holder.videoWeb.start();*//*
        holder.videoWeb.setWebChromeClient(new WebChromeClient() {
        });
        holder.videoWeb.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
*/
        //  holder.videoWeb.loadUrl(youtubeVideoList.get(position).getVideoUrl());
        //   holder.videoWeb.loadData(youtubeVideoList.get(position).getVideoUrl(), "text/html", "utf-8");

        // holder.videoWeb.loadData( youtubeVideoList.get(position).getVideoUrl(), "text/html" , "utf-8" );
        holder.videotitle.setText(youtubeVideoList.get(position).getVideotitle());
        holder.videotitle1.setText(youtubeVideoList.get(position).getVideotitle());
        String input = youtubeVideoList.get(position).getDate();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mstart_date = null;

        try {
            mstart_date = inputFormatter.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        String output = outputFormatter.format(mstart_date); // Output : 01/20/2012
        Log.d("date", "onBindViewHolder: " + output);
        holder.shortdesc1.setText(output);
        // holder.shortdesc2.setText(youtubeVideoList.get(position).getShortdesc2());
        //   holder.videoimage.setImageDrawable(mCtx.getResources().getDrawable(VideoList.getVideoimage()));
       /* holder.jzVideoPlayerStandard.setUp((youtubeVideoList.get(position).getVideoUrl())
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, youtubeVideoList.get(position).getVideotitle());
*/
        //  mCtx.getLifecycle().addObserver(holder.youtubePlayerView);
        // extractYTId(youtubeVideoList.get(position).getVideoUrl());
        holder.youtubePlayerView.getPlayerUIController().showFullscreenButton(false);

       // lifecycle.addObserver( holder.youtubePlayerView);


        holder.youtubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        // String videoId ="0NUr7p7Hknk";
                        String videoId = getYoutubeID(youtubeVideoList.get(position).getVideoUrl());
                       // youTubePlayer = initializedYouTubePlayer;
                        initializedYouTubePlayer.cueVideo(videoId, 0);
                    }
                });
            }
        }, true);

    }
    public static String getYoutubeID(String youtubeUrl) {

        if (TextUtils.isEmpty(youtubeUrl)) {
            return "";
        }
        String video_id = "";

        String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        CharSequence input = youtubeUrl;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            String groupIndex1 = matcher.group(7);
            if (groupIndex1 != null && groupIndex1.length() == 11)
                video_id = groupIndex1;
        }
        if (TextUtils.isEmpty(video_id)) {
            if (youtubeUrl.contains("youtu.be/")  ) {
                String spl = youtubeUrl.split("youtu.be/")[1];
                if (spl.contains("\\?")) {
                    video_id = spl.split("\\?")[0];
                }else {
                    video_id =spl;
                }
            }
        }

        return video_id;
    }

    @Override
    public int getItemCount() {
        return youtubeVideoList.size();
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        WebView videoWeb;
        TextView videotitle,videotitle1, shortdesc1, shortdesc2;
        ImageView videoimage;
        YouTubePlayerView youtubePlayerView;

        public VideoViewHolder(View itemView) {
            super(itemView);
            // videoWeb = (WebView) itemView.findViewById(R.id.videoWebView);
            youtubePlayerView = itemView.findViewById(R.id.youtube_player_view);
            videotitle = (TextView) itemView.findViewById(R.id.video_title_tit);
            videotitle1 = (TextView) itemView.findViewById(R.id.video_title);
            shortdesc1 = (TextView) itemView.findViewById(R.id.shortdesc1);
            shortdesc2 = (TextView) itemView.findViewById(R.id.shortdesc2);

            //  videoimage = (ImageView) itemView.findViewById(R.id.video_img);
           /* videoWeb.getSettings().setJavaScriptEnabled(true);
            videoWeb.setWebChromeClient(new WebChromeClient() {

            } );*/
        }


    }


}


