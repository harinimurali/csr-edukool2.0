package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.models.ChildrenProfile;

import java.util.List;


/**
 * Created by keerthana on 8/24/2018.
 */

public class PayFeesAdapter extends RecyclerView.Adapter<PayFeesAdapter.MyViewHolder> {

    private List<ChildrenProfile> moviesList;
    AddTouchListener addTouchListen;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, amount, paid;
        public LinearLayout linearLayout;
        Button makepayment;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            amount = view.findViewById(R.id.amount);
            paid = view.findViewById(R.id.paid);
            makepayment = view.findViewById(R.id.payment_button);

        }
    }

    public PayFeesAdapter(Context context, List<ChildrenProfile> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
    }

    public void setAddTouchListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_pay_fees_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    public interface AddTouchListener {
        void onTouchClick(int position);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ChildrenProfile movie = moviesList.get(position);
        holder.title.setText(movie.getFeestremname());
        holder.amount.setText("Amount :  Rs. " + movie.getFeesAmt());
        if (movie.getFeesstatus().equals("Paid")) {
            holder.paid.setVisibility(View.VISIBLE);
            holder.makepayment.setVisibility(View.GONE);
        } else {
            holder.paid.setVisibility(View.GONE);
            holder.makepayment.setVisibility(View.VISIBLE);
        }

        holder.makepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}