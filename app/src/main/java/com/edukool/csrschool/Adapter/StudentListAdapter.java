package com.edukool.csrschool.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.models.ChildrenProfile;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {

    private List<ChildrenProfile> moviesList;
    AddTouchListen addTouchListen;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView name;
        LinearLayout layout;

        public MyViewHolder(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.img);
            name = (TextView) view.findViewById(R.id.name);
            layout = view.findViewById(R.id.layout);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public StudentListAdapter(List<ChildrenProfile> moviesList) {
        this.moviesList = moviesList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ChildrenProfile movie = moviesList.get(position);
        Picasso.get().load(movie.getProfileImage()).placeholder(R.drawable.student_default).into(holder.img);
        holder.name.setText(movie.getName() + ", " + movie.getClasses()+" - "+movie.getSection());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

