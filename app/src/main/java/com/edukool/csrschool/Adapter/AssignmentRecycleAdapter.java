package com.edukool.csrschool.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.FontTextViewLight;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewRegular;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.Assignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */
public class AssignmentRecycleAdapter extends RecyclerView.Adapter<AssignmentRecycleAdapter.MyViewHolder> {


    private List<Assignment> assignlist;
    public AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewRegular subject, desrp, submissiontype;
        public FontTextViewMedium date;
        public FontTextViewSemibold name;
        public FontTextViewLight classname;
        LinearLayout assignment_layout;

        public MyViewHolder(View view) {
            super(view);
            subject = view.findViewById(R.id.subject);
            desrp = view.findViewById(R.id.descrp);
            submissiontype = view.findViewById(R.id.submisstiontype);
            date = view.findViewById(R.id.date);
            name = view.findViewById(R.id.name);
            classname = view.findViewById(R.id.classes);
            assignment_layout = view.findViewById(R.id.assignment_layout);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public AssignmentRecycleAdapter(List<Assignment> assignlist) {
        this.assignlist = assignlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.assignment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Assignment movie = assignlist.get(position);
        holder.subject.setText(movie.getSubject());
        holder.desrp.setText(movie.getDescription());
        holder.name.setText(movie.getName());
        holder.classname.setText(movie.getClassname());
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        Date date1 = null;
        String str = null;
        String str1 = null;

        try {
            date = inputFormat.parse(movie.getStartDate());
            date1 = inputFormat.parse(movie.getEndDate());
            str = outputFormat.format(date);
            str1 = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.date.setText(str + " to " + str1);
        //  holder.date.setText(movie.getStartDate()+" - "+movie.getEndDate());
        holder.submissiontype.setText(movie.getSubmissionType());
        holder.assignment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return assignlist.size();
    }
}
