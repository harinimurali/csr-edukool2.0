package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edukool.csrschool.R;

/**
 * Created by keerthana on 8/24/2018.
 */

public class DrawerAdapter extends BaseAdapter {

    Context ctx;

    //    String[] values = new String[]
//            {
//                    "Home", "Attendance", "Recent Activities", "Assignment", "Results", "Circulars", "Polls", "Pay Fees", "Language", "Logout"
//                    ,};
    private ListView listView;
    private Context context;
    String a[];

    Integer[] imgs = new Integer[]
            {
                    R.drawable.home_demo_icon,
                    R.drawable.attendance_demo_icon,
                    R.drawable.assignment_demo_icon,
                    R.drawable.result_demo_icon,
                    R.drawable.payfee_demo_icon,
                    R.drawable.leaverequest_demo_icon,
                    R.drawable.timetable_demo_icon,
                    R.drawable.exam_demo_icon,
                    R.drawable.recenactivity_demo_icon,
                    R.drawable.news_demo_icon,
                    R.drawable.polls_demo_icon,
                    R.drawable.circular_demo_icon,
                    R.drawable.contact_demo_icon,
                    R.drawable.message_demo_icon,
                    R.drawable.voice_demo_icon,
                    R.drawable.gallery_demo_icon,
                    R.drawable.video_demo_icon,
                    R.drawable.livestream_demo_icon,
                    R.drawable.calendar_demo_icon,
                    R.drawable.profile_demo_icon,
                    R.drawable.about_demo_icon,
                    R.drawable.bus_demo_icon,
                    R.drawable.lang_demo_icon,
                    R.drawable.logout_demo_icon,
                        /*android.R.drawable.ic_menu_camera,
                        android.R.drawable.ic_menu_gallery,
                        android.R.drawable.ic_menu_manage,
                        android. R.drawable.ic_menu_slideshow,
                        android. R.drawable.ic_menu_share,
                        android.R.drawable.ic_menu_camera,
                        android.R.drawable.ic_menu_share,
                        android. R.drawable.ic_menu_slideshow,
                        android.R.drawable.ic_menu_camera,

                        // R.drawable.chat,
                        android. R.drawable.ic_menu_camera,*/
            };

    public DrawerAdapter(Context ctx, String[] sidemenu) {
        this.ctx = ctx;
        this.a = sidemenu;
    }

    @Override
    public int getCount() {
        return a.length;
    }

    @Override
    public Object getItem(int position) {
        return a[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {

            LayoutInflater vi = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.drawer_row, null);
        }


//              listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                //                    Intent ieventreport = new Intent(context,MainActivity.class);
////                    context.startActivity(ieventreport);
//                if (position == 0) {
//                   // Toast.makeText()
//                }
//
//                }
//        });
//
//


        TextView buttonFlat = (TextView) rowView.findViewById(R.id.text);

        buttonFlat.setText(a[position]);

        ImageView img = (ImageView) rowView.findViewById(R.id.img);

        img.setBackgroundResource(imgs[position]);
        img.setScaleType(ImageView.ScaleType.FIT_XY);


        return rowView;

    }

}



