package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.models.ChildrenProfile;

import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<ChildrenProfile> items;
    private final int mResource;
    AddTouchListen addTouchListen;

    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
        //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        public void onSelectClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    public CustomSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        FontTextViewMedium offTypeTv = (FontTextViewMedium) view.findViewById(R.id.spinner_text);

        ChildrenProfile offerData = items.get(position);

        offTypeTv.setText(offerData.getName());
        /*offTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onSelectClick(position);
                }
            }
        });*/
        return view;
    }
}