package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.models.ResultModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {


    private List<ResultModel> assignlist;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView subject, classname, submissiontype, grade, date;
//        public FontTextViewMedium date;
//        public FontTextViewSemibold name;
//        public FontTextViewLight classname;

        public MyViewHolder(View view) {
            super(view);
            subject = view.findViewById(R.id.subject);
            classname = view.findViewById(R.id.classname);
            grade = view.findViewById(R.id.grade);
            // submissiontype = (FontTextViewRegular) view.findViewById(R.id.submisstiontype);
            date = view.findViewById(R.id.date);
            //name = (FontTextViewSemibold) view.findViewById(R.id.name);
            // classname = (FontTextViewLight) view.findViewById(R.id.classes);

        }
    }


    public ResultAdapter(List<ResultModel> assignlist, Context context) {
        this.assignlist = assignlist;
        this.context = context;
    }

    @Override
    public ResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_result, parent, false);

        return new ResultAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResultAdapter.MyViewHolder holder, int position) {
        ResultModel movie = assignlist.get(position);
        holder.subject.setText(movie.getSubject());

        /*Grade for Marks:
| 100-91  | A1
| 90-81  | A2
| 80-71  | B1
| 70-61  | B2
| 60-51  | C1
| 50-41  | C2
| 40-31  | D
| 30-21  | E1
| 20-0  | E2*/
        try {
            if (!movie.getGrade().isEmpty()) {
                try {
                    int a = Integer.parseInt(movie.getGrade());
                    //100-90
                    if (a >= 90 && a <= 100) {
                        holder.grade.setText("A1");
                    } else if (a >= 81 && a <= 90) {
                        holder.grade.setText("A2");
                    } else if (a >= 71 && a <= 80) {
                        holder.grade.setText("B1");
                    } else if (a >= 61 && a <= 70) {
                        holder.grade.setText("B2");
                    } else if (a >= 51 && a <= 60) {
                        holder.grade.setText("C1");
                    } else if (a >= 41 && a <= 50) {
                        holder.grade.setText("C2");
                    } else if (a >= 31 && a <= 40) {
                        holder.grade.setText("D");
                    } else if (a >= 21 && a <= 30) {
                        holder.grade.setText("E1");
                    } else if (a >= 0 && a <= 20) {
                        holder.grade.setText("E2");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    holder.grade.setText(movie.getGrade());
                    holder.grade.setTextColor(context.getResources().getColor(R.color.clr1));
                }
            } else {
                holder.grade.setText("-");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // holder.grade.setText(movie.getGrade());
        //  holder.desrp.setText(movie.getDescription());
        holder.classname.setText(movie.getClassname() + "-" + movie.getSection());
        //  holder.classname.setText(movie.getClassname());

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(movie.getDate());
            str = outputFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(str);
        //   holder.submissiontype.setText(movie.getSubmissionType());

    }

    @Override
    public int getItemCount() {
        return assignlist.size();
    }
}