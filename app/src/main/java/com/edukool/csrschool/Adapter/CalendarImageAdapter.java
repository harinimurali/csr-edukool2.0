package com.edukool.csrschool.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edukool.csrschool.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;


/**
 * Created by harini on 9/18/2018.
 */
public class CalendarImageAdapter extends RecyclerView.Adapter<CalendarImageAdapter.MyViewHolder> {
    List<String> listView;
    Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView attach_layout;

        public MyViewHolder(View view) {
            super(view);
            attach_layout = (ImageView) view.findViewById(R.id.image);

        }
    }

    public CalendarImageAdapter(List<String> assignlist, Context context) {
        this.listView = assignlist;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_calendar, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String baseurl = "http://csrschool.edukool.com";
        if (listView.size() != 0) {
            try {
                String afterDecode = URLDecoder.decode(listView.get(position), "UTF-8");
                String url = baseurl + afterDecode;
               // http://www.demo.edukool.com/edukool/assets/admin/news/modules/push-yourself-because-no-one-260nw-198183143.jpg
                Picasso.get().load(url.trim()).resize(500,400).into(holder.attach_layout, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.e("success", "picaso");
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("error", "picaso"+e);
                    }
                });
              //  Picasso.get().load(url.trim()).into(holder.attach_layout);
            //    Picasso.get().setLoggingEnabled(true);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public int getItemCount() {
        return listView.size();
    }
}
