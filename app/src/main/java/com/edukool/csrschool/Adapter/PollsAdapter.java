package com.edukool.csrschool.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewRegular;
import com.edukool.csrschool.models.PollsList;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class PollsAdapter extends RecyclerView.Adapter<PollsAdapter.MyViewHolder> {
    List<PollsList> pollslist;
    AddTouchListen addTouchListen;

    private LinearLayout pol;
    private FontTextViewRegular like2;
    private ImageView likeimg;
    private ImageView commentimg;

    private LinearLayout line;
    //  private  FontTextViewRegular mcomment;
    private FontTextViewRegular like;
    private RadioGroup radiogp;
    private List<PollsList> moviesList;
    private RadioGroup radioGroup;
    private ImageView post1;
    private TextView tv1;

    private int position;

    private KeyListener originalKeyListener;
    private TextView cmt;
    private Context ctx;
    public String comment = "0";
    public String liketext = "0";
    private FontTextViewRegular commenttext;
    public String likegray;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public FontTextViewMedium mSubject;
        //    public FontTextViewRegular mClass1;
        public RelativeLayout mline;
        public ImageView img;
        private ImageView post1;
        public RadioGroup radiogp;
        public ImageView likeimg;
        private FontTextViewRegular commenttext;
        public FontTextViewRegular like2;
        private EditText edit1;
        public FontTextViewRegular like;
        private RadioButton like1, dislike1;
        public FontTextViewMedium mid, mname, mstart_date, mend_date;
        public TextView tv1;
        Context context;

        public RadioGroup rg;
        JustifiedTextView mdesc;


//        public RadioGroup radiogroupp;
//        public TextView label;

//        public FontTextViewRegular mcomment;
//        public RadioButton mlike, mdislike;
//        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);

            mid = (FontTextViewMedium) view.findViewById(R.id.id1);
            mname = (FontTextViewMedium) view.findViewById(R.id.subject);
            tv1 = (TextView) view.findViewById(R.id.tv);
            mdesc = view.findViewById(R.id.description1);
            mstart_date = (FontTextViewMedium) view.findViewById(R.id.date1);
            mend_date = (FontTextViewMedium) view.findViewById(R.id.date1);
            like1 = (RadioButton) view.findViewById(R.id.like1);
            dislike1 = (RadioButton) view.findViewById(R.id.dislike1);

            //   img=(ImageView)view.findViewById(R.id.loveimg);
            //  like=(FontTextViewRegular)view.findViewById(R.id.liketext);
            //  like2=(FontTextViewRegular)view.findViewById(R.id.liketext2);
            pol = (LinearLayout) view.findViewById(R.id.polls);
            commenttext = (FontTextViewRegular) view.findViewById(R.id.commenttext);
            commentimg = (ImageView) view.findViewById(R.id.commentimg);
            //  likeimg=(ImageView)view.findViewById(R.id.loveimg2);
            //   mClass1 = (FontTextViewRegular) view.findViewById(R.id.class1);
            radiogp = (RadioGroup) view.findViewById(R.id.rg);
            //   like = (RadioButton) view.findViewById(R.id.like);
            // dislike = (RadioButton) view.findViewById(R.id.dislikelike1);
            // cmt=(TextView)view.findViewById(R.id.comment);
            post1 = (ImageView) view.findViewById(R.id.post);
            mline = (RelativeLayout) view.findViewById(R.id.linear);
            edit1 = (EditText) view.findViewById(R.id.edit);
            //  btnclick();
            //  display();
            //   mcomment = (FontTextViewRegular) view.findViewById(R.id.comment);

//            mTime = (FontTextViewLight) view.findViewById(R.id.time);
//            linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        }

    }

    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
        //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        public void onTouchClick(int position, String like, String comment, RadioGroup rg, RelativeLayout layout, EditText edit);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    private void btnclick() {


    }


    public PollsAdapter(Context context, List<PollsList> moviesList) {
        this.moviesList = moviesList;
        this.ctx = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_polls_item_view, parent, false);


        return new MyViewHolder(itemView);

    }

    private ArrayList<String> arrayList;
    //    private Context context;
    private int selectedPosition = -1;

    //    public PollsAdapter(Context context, ArrayList<String> arrayList) {
//        this.arrayList = arrayList;
//        this.context = context;
//    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        PollsList movie = moviesList.get(position);
        // holder.mid.setText(movie.getID());
        holder.mid.setText(movie.getID());
        holder.mname.setText(movie.getName());
        holder.mdesc.setText(movie.getDescription());
        //   holder.like.set(movie.getLike()+"-"+movie.getDislike()+"-"+movie.getComment());
        //  holder.commenttext.setText(movie.getComment());
        //holder.mstart_date.setText(movie.getStart_date()+"-"+movie.getEnd_date());

        // holder.mstart_date.setText(dateFormatForMonth.format("YYYY-MM-DD"));

        String input = movie.getStart_date();
        String input1 = movie.getEnd_date();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mstart_date = null;
        Date mend_date = null;

        try {
            mstart_date = inputFormatter.parse(input);
            mend_date = inputFormatter.parse(input1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        String output = outputFormatter.format(mstart_date); // Output : 01/20/2012
        String output2 = outputFormatter.format(mend_date); // Output : 01/20/2012
        Log.d("date", "onBindViewHolder: " + output + " " + output2);

        holder.mstart_date.setText(output + " to " + output2);
        holder.post1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.edit1.getText().toString().isEmpty()) {
                    holder.edit1.setError(ctx.getResources().getString(R.string.fill));
                } else {
                    addTouchListen.onTouchClick(position, "", holder.edit1.getText().toString(), holder.radiogp, holder.mline, holder.edit1);
                    holder.edit1.setText("");
                    holder.mline.setVisibility(View.GONE);

                }
            }
        });

        //   holder.mClass1.setText(movie.getClass1());


//        like.setChecked(false);
//        dislike.setChecked(false);

//       pol.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               if(liketext.equalsIgnoreCase("0"))
//               {
//
//                   holder.likeimg.setVisibility(View.VISIBLE);
//                   holder.img.setVisibility(View.GONE);
//                   holder.like2.setVisibility(View.VISIBLE);
//                   holder.like.setVisibility(View.GONE);
//                  liketext="1";
//                  likegray="0";
//
//               }
//               else if(liketext.equalsIgnoreCase("1"))
//               {
//                   holder.img.setVisibility(View.VISIBLE);
//                   holder.likeimg.setVisibility(View.GONE);
//                   holder.like.setVisibility(View.VISIBLE);
//                   holder.like2.setVisibility(View.GONE);
//                   liketext="0";
//                   likegray="1";
//               }
//
//
//
//
//           }
//       });

        holder.radiogp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.e("group", "" + checkedId);
                if (holder.like1.isChecked()) {
                    addTouchListen.onTouchClick(position, "1", " ", holder.radiogp, holder.mline, holder.edit1);
                } else if (holder.dislike1.isChecked()) {
                    addTouchListen.onTouchClick(position, "0", " ", holder.radiogp, holder.mline, holder.edit1);
                }
                // checkedId is the RadioButton selected
            }
        });
        holder.commenttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.equalsIgnoreCase("0")) {
                    holder.mline.setVisibility(View.GONE);
                    hideKeyboardFrom(ctx, holder.edit1);
                    comment = "1";
                } else {
                    holder.mline.setVisibility(View.VISIBLE);
                    holder.edit1.setFocusable(true);
                    showSoftKeyboard(holder.edit1);

                    comment = "0";
                }

            }
        });


//        holder.radiogp.setTag(position==selectedPosition);
//        holder.radiogp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
//                if (checkedRadioButtonId == -1) {
//                    // No item selected
//                } else {
//                    if (checkedRadioButtonId == R.id.like1) {
//                        // Do something with the button
//                    }
//                }
//            }  });


     /*   holder.label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }


        });*/
//        holder.mcomment.setText(movie.getComment());
//        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
//                if (lastCheckedRB != null) {
//                    lastCheckedRB.setChecked(false);
//                }
//                //store the clicked radiobutton
//                lastCheckedRB = checked_rb;
//            }
//        });
       /* holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });*/
    }

   /* private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }*/

    @Override
    public int getItemCount() {
        return moviesList.size();
        // return 3;
    }

    //    public void onRadioButtonClicked(View view) {
//        if (like.isChecked()) {
//
//        } else if (dislike.isChecked()) {
//
//        }
//    }
//public void Onclick(View v) {
//    int radioId = radioGroup.getCheckedRadioButtonId();
//
//    like = (RadioButton) v.findViewById(R.id.like1);
//    dislike = (RadioButton) v.findViewById(R.id.dislikelike1);
//
////    Toast.makeText(this, "Selected Radio Button: " + RadioButton.getText(),
////            Toast.LENGTH_SHORT).show();
//}
    public static void hideSoftKeyboard(Activity activity) {
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Shows the soft keyboard
     */
    public static void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

}

// Is the button now checked?
//        boolean checked = ((RadioButton) view).isChecked();
//
//        // Check which radio button was clicked
//        switch(view.getId()) {
//            case R.id.like1:
//                if (checked)
//                    // Pirates are the best
//                    break;
//            case R.id.dislikelike1:
//                if (checked)
//                    // Ninjas rule
//                    break;
//        }


