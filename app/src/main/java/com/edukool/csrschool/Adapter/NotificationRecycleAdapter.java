package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.FontTextViewLight;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewRegular;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.NotificationModels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */


public class NotificationRecycleAdapter extends RecyclerView.Adapter<NotificationRecycleAdapter.MyViewHolder> {


    private Context mCtx;
    private List<NotificationModels> notificationList;
    AddTouchListener addTouchListen;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateformat = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm a");


    public NotificationRecycleAdapter(Context mCtx, List<NotificationModels> notificationList) {
        this.mCtx = mCtx;
        this.notificationList = notificationList;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.noti_recycle_view_item, null);
        return new MyViewHolder(view);
    }

    public interface AddTouchListener {
        public void onTouchClick(int position, String type);
    }


    public void setAddTouchListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        //getting the product of the specified position
        final NotificationModels movie = notificationList.get(position);
        if (movie.getType().equals("Circulars")) {
            //binding the data with the viewholder views
            holder.Title.setText(movie.getContent());

            try {
                Date date2 = formatter.parse(movie.getCreatedDate());
                String a = dateformat.format(date2);
                String b = timeformat.format(date2);
                Log.e("date2", "" + a);
                holder.date.setText(a);
                holder.time.setText(b);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.type_name.setText("C");
            holder.type.setText(movie.getType());
        } else if (movie.getType().equals("Assignments")) {
            holder.Title.setText(movie.getName());
            holder.type.setText(movie.getType());
            holder.date.setText(movie.getDate());
            try {
                Date date2 = formatter1.parse(movie.getDate());
                String a = dateformat.format(date2);
                Log.e("date2", "" + a);
                holder.date.setText(a);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.type_name.setText("A");
            holder.time.setVisibility(View.GONE);
        } else if (movie.getType().equals("Events")) {
            holder.type.setText(movie.getType());
            holder.Title.setText(movie.getName());
            try {
                Date date2 = formatter.parse(movie.getDate());
                String a = dateformat.format(date2);
                String b = timeformat.format(date2);
                Log.e("date2", "" + a);
                holder.date.setText(a);
                holder.time.setText(b);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.type_name.setText("E");
            holder.time.setVisibility(View.VISIBLE);
        } else if (movie.getType().equals("RecentActivity")) {
            holder.type.setText(movie.getType());
            holder.Title.setText(movie.getName());
            holder.date.setText(movie.getDate());
            holder.type_name.setText("R");
            try {
                Date date2 = formatter.parse(movie.getDate());
                String a = dateformat.format(date2);
                String b = timeformat.format(date2);
                Log.e("date2", "" + a);
                holder.date.setText(a);
                holder.time.setText(b);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.time.setVisibility(View.VISIBLE);
        }


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position, movie.getType());
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        FontTextViewRegular Title;
        FontTextViewMedium type;
        FontTextViewSemibold type_name;
        FontTextViewLight date, time;
        ImageView document;
        LinearLayout linearLayout;
        LinearLayout circularlayout, assignmentlayout;


        public MyViewHolder(View itemView) {
            super(itemView);

            Title = itemView.findViewById(R.id.subject);
            type = itemView.findViewById(R.id.type);
            type_name = itemView.findViewById(R.id.type_name);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            document = itemView.findViewById(R.id.document);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            circularlayout = itemView.findViewById(R.id.circular_layout);
            assignmentlayout = itemView.findViewById(R.id.assignment_layout);
        }
    }
}


