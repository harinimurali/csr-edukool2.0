package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edukool.csrschool.R;

/**
 * Created by keerthana on 8/24/2018.
 */

public class GridViewadapter extends BaseAdapter {

    private String[] gridstring;
    private Context mContext;

    private final int[] gridViewImageId;
    AddTouchListen addTouchListen;
    String gd[];


    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
        //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        public void onRemoveClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    public GridViewadapter(Context context, String[] gridstring, int[] gridViewImageId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gd = gridstring;
    }

    @Override
    public int getCount() {
        return gd.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridViewAndroid = inflater.inflate(R.layout.grid_view_item, null);

        } else {
            gridViewAndroid = (View) convertView;
        }
        TextView textViewAndroid = (TextView) gridViewAndroid.findViewById(R.id.grid_texts);
        ImageView imageViewAndroid = (ImageView) gridViewAndroid.findViewById(R.id.grid_image);
        ImageView comingsoon = (ImageView) gridViewAndroid.findViewById(R.id.comingsoon);
        RelativeLayout layout =  gridViewAndroid.findViewById(R.id.layout);
        textViewAndroid.setText(gd[i]);
        imageViewAndroid.setImageResource(gridViewImageId[i]);

        if (i == 20) {
            comingsoon.setVisibility(View.VISIBLE);
        } else {
            comingsoon.setVisibility(View.GONE);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onRemoveClick(i);
                }
            }
        });

        return gridViewAndroid;
    }


}














































