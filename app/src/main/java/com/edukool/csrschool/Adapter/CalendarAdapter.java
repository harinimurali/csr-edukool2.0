package com.edukool.csrschool.Adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.edukool.csrschool.models.EventResultItem;

import java.util.List;

/**
 * Created by harini on 9/18/2018.
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {


    private List<EventResultItem> eventlist;
    private Context context;
    List<String> listView;
    CalendarImageAdapter mAdapter;

    public CalendarAdapter(List<EventResultItem> eventlist, Context context) {
        this.eventlist = eventlist;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView desrp, name;
        public RecyclerView recyclerView;

        public MyViewHolder(View view) {
            super(view);
            desrp = (TextView) view.findViewById(R.id.subtitle);
            name = (TextView) view.findViewById(R.id.title);
            recyclerView = (RecyclerView) view.findViewById(R.id.event_item);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_grid_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        EventResultItem movie = eventlist.get(position);
        holder.desrp.setText(movie.getTitleDescription());
        holder.name.setText(movie.getModuleType());

        mAdapter = new CalendarImageAdapter(movie.getAttachment(), context);
        holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.recyclerView.setAdapter(mAdapter);

       /* String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        Date date1 = null;
        String str = null;
        String str1 = null;*/

      /*  try {
            date = inputFormat.parse(movie.getStartDate());
            date1 = inputFormat.parse(movie.getEndDate());
            str = outputFormat.format(date);
            str1 = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.date.setText(str + " to " + str1);
        //  holder.date.setText(movie.getStartDate()+" - "+movie.getEndDate());
        holder.submissiontype.setText(movie.getSubmissionType());
*/
    }

    @Override
    public int getItemCount() {
        return eventlist.size();
    }
}
