package com.edukool.csrschool.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.FontTextViewLight;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewRegular;
import com.edukool.csrschool.models.RecentActivititesModels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by keerthana on 8/24/2018.
 */

public class RecentActyAdapter extends RecyclerView.Adapter<RecentActyAdapter.MyViewHolder> {

    private List<RecentActivititesModels> moviesList;
    //  private final OnItemClickListener listener;

    AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewMedium mtitle;
        public FontTextViewRegular mSubject;
        public FontTextViewLight mTime, mDate;
        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            mtitle = (FontTextViewMedium) view.findViewById(R.id.title);
            mSubject = (FontTextViewRegular) view.findViewById(R.id.subject);
            mDate = (FontTextViewLight) view.findViewById(R.id.date);
            mTime = (FontTextViewLight) view.findViewById(R.id.time);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        }
    }


    public RecentActyAdapter(List<RecentActivititesModels> moviesList) {
        this.moviesList = moviesList;
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_vew_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        RecentActivititesModels movie = moviesList.get(position);
        holder.mtitle.setText(movie.getTitle());
        holder.mSubject.setText(movie.getSubject());
        String input = movie.getDate();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mstart_date = null;

        try {
            mstart_date = inputFormatter.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        DateFormat outputFormatter1 = new SimpleDateFormat("hh:mm a");
        String output = outputFormatter.format(mstart_date); // Output : 01/20/2012
        String output1 = outputFormatter1.format(mstart_date); // Output : 01/20/2012
        Log.d("date", "onBindViewHolder: " + output + output1);
        holder.mDate.setText(output);
        holder.mTime.setText(output1);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
        // return 3;
    }
}