package com.edukool.csrschool.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by keerthana on 8/24/2018.
 */

public class FirebaseMessaging extends FirebaseMessagingService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String TAG = "FirebaseMessagings";
    Bitmap bitmap;
    String message;
    String type = "";
    String imageUri = "";
    int notificationID;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        Log.d("FirebaseMessagings", "onMessageReceived: " + remoteMessage.getNotification());
        //sendNotification(remoteMessage.getNotification());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //The message which i send will have keys named [message, image, AnotherActivity] and corresponding values.
        //You can change as per the requirement.

        //message will contain the Push Message
        if (remoteMessage.getData().get("message") != null) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("message"));
                if (jsonObject.has("content")) {
                    message = jsonObject.getString("content");
                }/*else if(jsonObject.has("activityName")){
                    message = jsonObject.getString("activityName");
                }*/

                type = jsonObject.getString("notifType");
                Log.e("type", "" + type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //message = remoteMessage.getData().get("message");
        }
        if (remoteMessage.getData().get("image") != null) {
            imageUri = remoteMessage.getData().get("image");
        }
//        if (remoteMessage.getData().get("message") != null) {
//
//        }

        //imageUri will contain URL of the image to be displayed with Notification

        //If the key AnotherActivity has  value as True then when the user taps on notification, in the app AnotherActivity will be opened.
        //If the key AnotherActivity has  value as False then when the user taps on notification, in the app MainActivity will be opened.
        // String TrueOrFlase = remoteMessage.getData().get("AnotherActivity");
        bitmap = getBitmapfromUrl(imageUri);

        sendNotification(message, bitmap, type);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    //This method is only generating push icon_notification
    //It is same as we did in earlier posts
    private void sendNotification(String msg, Bitmap bitmap, String type) {


        String channelId = getString(R.string.default_notification_channel_id);
        String channelName = msg;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //  Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.icon_notify);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("msg", msg);

       /* TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(intent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);*/

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.csr_round);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.nott)
                .setContentTitle(type)
                .setContentText(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        //  Notification n = notificationBuilder.build();
        //  n.flags = Notification.FLAG_NO_CLEAR;
        // notificationManager.notify(importance++, n);
        notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());

        Intent intentNo = new Intent("notificationListener");
        intentNo.putExtra("notify", true);
        sendBroadcast(intentNo);
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }
}
