package com.edukool.csrschool.firebase;

import android.content.SharedPreferences;
import android.util.Log;

import com.edukool.csrschool.Utils.AppPreferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by keerthana on 8/24/2018.
 */
public class FirebaseInstanceID extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("fcmid", "onTokenRefresh: " + refreshedToken);
        //editor.putString(DmkConstants.FCMTOKEN,refreshedToken).commit();
//        fcmDeviceKey=refreshedToken;
        System.out.println("==========" + refreshedToken + "=======");
        AppPreferences.setFCMToken(getApplicationContext(), refreshedToken);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //  sharedPreferences=getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES,MODE_PRIVATE);
        //editor=sharedPreferences.edit();
    }
}
