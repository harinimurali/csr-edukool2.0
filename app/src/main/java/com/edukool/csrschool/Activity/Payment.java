package com.edukool.csrschool.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by keerthana on 8/24/2018.
 */

public class Payment extends AppCompatActivity implements PaymentResultListener {

    String ParentNumber, schoolid, versioncode;
    WS_CallService service_Login;
    String amount, idfees, name, studentid;
    String in;
    Button back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
        Intent i = getIntent();
        amount = i.getStringExtra("amount");
        idfees = i.getStringExtra("feesid");
        name = i.getStringExtra("name");
        studentid = i.getStringExtra("studentid");

        if (amount.contains(".")) {
            float fot = Float.parseFloat(amount);
            float ina = fot * 100;
            in = String.valueOf(ina);

            Log.e("float", "" + in);
        } else {
            in = String.valueOf(Integer.parseInt(amount) * 100);
        }
        back = findViewById(R.id.back_pay);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ParentNumber = AppPreferences.getParentNumber(Payment.this);
        schoolid = AppPreferences.getSchool_Name(Payment.this);
        versioncode = AppPreferences.getversioncode(Payment.this);
        startPayment();
    }

    private void startPayment() {
        final Activity activity = Payment.this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", name);
            options.put("description", "Fees Payment");
            //You can omit the image option to fetch the image from dashboard
            //  options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", in);

            JSONObject preFill = new JSONObject();
            preFill.put("email", "");
            preFill.put("contact", ParentNumber);

            options.put("prefill", preFill);

            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }

    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            callAPIFeePaid(razorpayPaymentID, idfees);
          // Toast.makeText(Payment.this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("paymentId", "razorpayPaymentID " + razorpayPaymentID);
        } catch (Exception e) {
            Log.e("payment ", "Exception in onPaymentSuccess", e);
        }
    }


    @Override
    public void onPaymentError(int i, String s) {
        Log.e("error", "payment " + s);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // finish();
    }

    private void callAPIFeePaid(String razorpayPaymentID, String feesid) {
        try {
            //   progressBar.setVisibility(View.VISIBLE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            // String schoolid = "SC-001-LS";
            // studentid = "1";
            byte[] data;
            String login_str = "UserName:" + schoolid + "|TransactionId:" + razorpayPaymentID + "|FeesId:" + feesid + "|StudentId:" + studentid + "|Function:FeesPaymentSuccess|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.BASE_OS + "";
            //String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:IsFeesPaid|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.BASE_OS + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_SuccessPayment_WS load_plan_list = new Load_SuccessPayment_WS(Payment.this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Load_SuccessPayment_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_SuccessPayment_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
           /* progressBar.setVisibility(View.GONE);
            profiles = new ArrayList<>();*/
            Log.e("jsonResponse", "Fee list" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equals("Success")) {
                   /* paymentbtn.setVisibility(View.GONE);
                    nofee.setVisibility(View.VISIBLE);*/

                    Intent i = new Intent(Payment.this, MainActivity.class);
                    i.putExtra("type", "payment");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    Toast.makeText(Payment.this, jObj.getString("response"), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
