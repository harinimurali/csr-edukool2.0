package com.edukool.csrschool.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by harini on 9/28/2018.
 */

public class RecentDetailActivity extends AppCompatActivity {
    String title, subject, date, description, activitydes;
    TextView mtitle, msubject, mdate, mactivity, mtime;
    ImageView back;
    JustifiedTextView mdescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_detail);


        Intent i = getIntent();

        Bundle b = i.getBundleExtra("recent");
        if (b != null) {
            title = b.getString("title");
            subject = b.getString("subject");
            date = b.getString("date");
            activitydes = b.getString("description");
        }

        mtitle = findViewById(R.id.title);
        msubject = findViewById(R.id.subject);
        mdate = findViewById(R.id.subdate);
        mdescription = findViewById(R.id.desc_text);
        mtime = findViewById(R.id.time);

        back = findViewById(R.id.back);

        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mstart_date1 = null;

        try {
            mstart_date1 = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        DateFormat outputFormatter1 = new SimpleDateFormat("hh:mm a");
        String dat = outputFormatter.format(mstart_date1); // Output : 01/20/2012
        String time = outputFormatter1.format(mstart_date1); // Output : 12:00 AM
        Log.d("date", "onBindViewHolder: " + dat + time);

        mtitle.setText(title);
        msubject.setText(subject);
        mdate.setText(dat);
        mdescription.setText(activitydes);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mdescription.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
        mtime.setText(time);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
