package com.edukool.csrschool.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import io.salyangoz.updateme.UpdateMe;
import io.salyangoz.updateme.listener.OnNegativeButtonClickListener;
import io.salyangoz.updateme.listener.OnPositiveButtonClickListener;

/**
 * Created by keerthana on 8/24/2018.
 */

public class SplashScreenActivity extends AppCompatActivity implements Animation.AnimationListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.splash_screen);

        FrameLayout mainFrame = ((FrameLayout) findViewById(R.id.splash_frame));
        Animation zoom_out = AnimationUtils.loadAnimation(this,
                R.anim.zoom_out);
        zoom_out.setAnimationListener(this);
        mainFrame.startAnimation(zoom_out);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            AppPreferences.setversioncode(getApplicationContext(), String.valueOf(verCode));

            Log.e("payment", "version" + version + "-" + verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        UpdateMe.with(this, 30).setDialogVisibility(true)
                .continueButtonVisibility(true)
                .setPositiveButtonText("Update")
                //.setNegativeButtonText("Oh No...")
                .setDialogIcon(R.drawable.logo_copy)
                .onNegativeButtonClick(new OnNegativeButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Later Button Clicked");
                        dialog.dismiss();
                    }
                })
                .onPositiveButtonClick(new OnPositiveButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Update Button Clicked");
                        dialog.dismiss();
                    }
                })
                .check();

    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        Intent i;
        if (AppPreferences.isLoggedIn(SplashScreenActivity.this)) {
            i = new Intent(SplashScreenActivity.this, MainActivity.class);

        } else {
            i = new Intent(SplashScreenActivity.this, LoginActivity.class);
        }
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        startActivity(i);
        finish();
/*
        if (Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase("")) {
            new UserRepository().accessTokenValidation(this, iUserDataHandler);
            new UserRepository().requestAppSettings(mCurrentActivity, iUserDataHandler);
        } else if (!Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase(""))
            iUserDataHandler.onTokenValidated();
        else
            iUserDataHandler.onError("");*/
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
