package com.edukool.csrschool.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by harini on 9/28/2018.
 */

public class AssignmentDetailActivity extends AppCompatActivity {
    TextView title, subject, date, submissiontype;
    String mtitle, msubject, mstartdate, menddate, mdescription, mtype;
    //JustifyTextView description;
    JustifiedTextView description;

    ImageView back;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_detail);

        Intent i = getIntent();

        Bundle b = i.getBundleExtra("assignment");
        if (b != null) {
            mtitle = b.getString("title");
            msubject = b.getString("subject");
            mstartdate = b.getString("startdate");
            menddate = b.getString("enddate");
            mdescription = b.getString("description");
            mtype = b.getString("submission");
        }

        title = findViewById(R.id.title);
        subject = findViewById(R.id.subject);
        date = findViewById(R.id.subdate);
        description = findViewById(R.id.desc_text);
        submissiontype = findViewById(R.id.type);
        back = findViewById(R.id.back);

        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date mstart_date1 = null;
        Date mstart_date2 = null;

        try {
            mstart_date1 = inputFormatter.parse(mstartdate);
            mstart_date2 = inputFormatter.parse(menddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        String start = outputFormatter.format(mstart_date1); // Output : 01/20/2012
        String end = outputFormatter.format(mstart_date2); // Output : 01/20/2012
        Log.d("date", "onBindViewHolder: " + start + end);

        title.setText(mtitle);
        subject.setText(msubject);
        date.setText(start + " to " + end);
        description.setText(mdescription);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            description.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
        submissiontype.setText(mtype);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
