package com.edukool.csrschool.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.NotificationRecycleAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.edukool.csrschool.models.NotificationModels;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationActivity extends Activity {
    TextView text;
    ImageView back;
    // private Toolbar toolbarr;
    private Spinner spinner;
    private RecyclerView notirecycle;
    private NotificationRecycleAdapter mAdapter;
    private List<NotificationModels> notificationList;
    List<ChildrenProfile> childrenProfiles;
    String studentid;
    ProgressBar progressBar;
    WS_CallService service_Login;
    String ParentNumber, versioncode;
    String schoolid;
    FontTextViewMedium norecords;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    AlertDialog.Builder alertDialog;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        spinner = (Spinner) findViewById(R.id.spinner);
        back = (ImageView) findViewById(R.id.back);
        progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
        norecords = (FontTextViewMedium) findViewById(R.id.norecords);
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(this);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        alertDialog = new AlertDialog.Builder(NotificationActivity.this);
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this,
                R.layout.spinner_layout, childrenProfiles);

        ParentNumber = AppPreferences.getParentNumber(NotificationActivity.this);
        schoolid = AppPreferences.getSchool_Name(NotificationActivity.this);
        versioncode = AppPreferences.getversioncode(NotificationActivity.this);
        notirecycle = (RecyclerView) findViewById(R.id.noti_recycle);
       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);
        stuname = findViewById(R.id.student_name);
        stuclass = findViewById(R.id.student_class);
        stuimage = findViewById(R.id.student_image);
        drpDown = findViewById(R.id.drpDown);

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(this)).getID();
        if (Permission.checknetwork(NotificationActivity.this)) {
            callAPI();
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(NotificationActivity.this)) {
                    callAPI();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       /* if (Permission.checknetwork(NotificationActivity.this)) {
            callAPI();
        }*/





      /*  notificationList.add(
                new NotificationList("Ramzon", "Tomorrow Holiday\n 20-06-18"));
        notificationList.add(
                new NotificationList("Tamil", "All of you Study Tamil on next day"));
        notificationList.add(
                new NotificationList("Hindi", "Bring Your Hindi Assignment"));
        notificationList.add(
                new NotificationList("Science", "Bring Your Record Note"));
        notificationList.add(
                new NotificationList("Social", "Bring Your Atlas Book"));
        notificationList.add(
                new NotificationList("Student", "Please practice well for Upcoming Exams"));
        notificationList.add(
                new NotificationList("Computer science", "Please go through the topics"));*/

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(NotificationActivity.this);
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(NotificationActivity.this, position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(NotificationActivity.this)).getID();
                callAPI();
                dialog.dismiss();
            }


        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAPI() {
        if (Permission.checknetwork(NotificationActivity.this)) {
            try {
                notirecycle.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                //  String schoolid = "SC-005-LS";
                // studentid = "1";
                byte[] data;
                //String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:"+studentid+"|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                String login_str = "UserName:" + schoolid + "|Parent_ID:" + ParentNumber + "|studentId:" + studentid + "|Function:Notifications|Device Type:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Notification_WS load_plan_list = new Load_Notification_WS(this, login);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public class Load_Notification_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Notification_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            notificationList = new ArrayList<>();
            Log.e("jsonResponse", "notificationlist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    /*if(jObj.getString("StatusCode").equals("201")){
                        norecords.setVisibility(View.VISIBLE);
                    }else{*/
                    norecords.setVisibility(View.GONE);
                    notirecycle.setVisibility(View.VISIBLE);

                    JSONArray array = jObj.getJSONArray("CircularNotifications");
                    if (array.length() != 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            NotificationModels models = new NotificationModels();
                            models.setID(jsonObject.getString("ID"));
                            models.setContent(jsonObject.getString("Content").trim());
                            models.setType(jsonObject.getString("Type").trim());
                            models.setAttachment(jsonObject.getString("Attachment"));
                            models.setCreatedDate(jsonObject.getString("CreatedDate"));
                            notificationList.add(models);
                            //    recent.setClass(jsonObject.getString("Class"));
                            // recent.setClass(jsonObject.getString("Date"));
                            //   Recentlist.add(recent);
                        }
                    }

                    JSONObject ob = jObj.getJSONObject("AssignmentsNotifications");
                    if (ob.has("Assignments")) {
                        JSONObject ob1 = ob.getJSONObject("Assignments");
                        JSONArray array1 = ob1.getJSONArray("Previous");
                        for (int i = 0; i < array1.length(); i++) {
                            JSONObject jsonObject = array1.getJSONObject(i);
                            NotificationModels models = new NotificationModels();
                            models.setName(jsonObject.getString("Name"));
                            models.setDescription(jsonObject.getString("Description"));
                            models.setSubject(jsonObject.getString("Subject"));
                            models.setDate(jsonObject.getString("StartDate"));
                            models.setType(jsonObject.getString("Type").trim());
                            notificationList.add(models);
                        }
                    }
                    JSONObject ob2 = jObj.getJSONObject("EventsNotifications");
                    if (ob2.has("Videos")) {
                        JSONArray array2 = ob2.getJSONArray("Videos");
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject jsonObject = array2.getJSONObject(i);
                            NotificationModels models = new NotificationModels();
                            models.setName(jsonObject.getString("Name"));
                            models.setDescription(jsonObject.getString("Description"));
                            models.setDate(jsonObject.getString("EventStartDate"));
                            models.setType(jsonObject.getString("Type").trim());
                            notificationList.add(models);
                        }
                    }

                    JSONObject ob3 = jObj.getJSONObject("StudActivityNotifications");
                    if (ob3.has("RecentActivity")) {
                        JSONObject ob4 = ob3.getJSONObject("RecentActivity");
                        JSONArray array3 = ob4.getJSONArray("Previous");
                        for (int i = 0; i < array3.length(); i++) {
                            JSONObject jsonObject = array3.getJSONObject(i);
                            NotificationModels models = new NotificationModels();
                            models.setName(jsonObject.getString("ActivityTitle"));
                            models.setName(jsonObject.getString("Subject"));
                            models.setDate(jsonObject.getString("Date"));
                            models.setType(jsonObject.getString("Type").trim());
                            notificationList.add(models);
                        }
                    }

                    mAdapter = new NotificationRecycleAdapter(NotificationActivity.this, notificationList);
                    mLayoutManager = new LinearLayoutManager(NotificationActivity.this);
                    notirecycle.setLayoutManager(mLayoutManager);
                    notirecycle.setItemAnimator(new DefaultItemAnimator());
                    notirecycle.setAdapter(mAdapter);

                    mAdapter.setAddTouchListen(new NotificationRecycleAdapter.AddTouchListener() {
                        @Override
                        public void onTouchClick(int position, String type) {
                            if (type.equals("Circulars")) {
                               /* Fragment fragmentc = new CircularsFragment();
                                MainActivity.replaceFragment(fragmentc);
                                MainActivity.title.setText(getResources().getString(R.string.circulars));*/
                                startActivity(new Intent(NotificationActivity.this, NotificationDetailActivity.class).putExtra("Notification", type));

                            } else if (type.equals("Assignments")) {
                                startActivity(new Intent(NotificationActivity.this, NotificationDetailActivity.class).putExtra("Notification", type));

                            } else if (type.equals("Events")) {
                                startActivity(new Intent(NotificationActivity.this, NotificationDetailActivity.class).putExtra("Notification", type));

                            } else if (type.equals("RecentActivity")) {
                                startActivity(new Intent(NotificationActivity.this, NotificationDetailActivity.class).putExtra("Notification", type));
                            }
                        }

                    });


                    //  }

                    System.out.println("success");
                    Intent intentNo = new Intent("notificationListener");
                    intentNo.putExtra("notify", false);
                    sendBroadcast(intentNo);
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    //Toast.makeText(NotificationActivity.this, Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

}
