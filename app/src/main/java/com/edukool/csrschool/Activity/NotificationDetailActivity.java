package com.edukool.csrschool.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukool.csrschool.Fragment.Assignments;
import com.edukool.csrschool.Fragment.CircularsFragment;
import com.edukool.csrschool.Fragment.HomePageFragment;
import com.edukool.csrschool.Fragment.PayFees;
import com.edukool.csrschool.Fragment.Polls;
import com.edukool.csrschool.Fragment.RecentActivities;
import com.edukool.csrschool.R;

/**
 * Created by harini on 11/7/2018.
 */

public class NotificationDetailActivity extends AppCompatActivity {
    ImageView back;
    FrameLayout frameLayout;
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);
        back = findViewById(R.id.back);
        title = findViewById(R.id.title_noti);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("noti", "noti");
        Intent i = getIntent();
        if (i.hasExtra("Notification")) {
            String type = i.getStringExtra("Notification");
            switch (type) {
                case "Circulars":
                    Fragment fragmentc = new CircularsFragment();
                    fragmentc.setArguments(bundle);
                    replaceFragment(fragmentc);
                    title.setText(getResources().getString(R.string.circulars));
                    break;
                case "Assignments":

                    Fragment fragmenta = new Assignments();
                    fragmenta.setArguments(bundle);
                    replaceFragment(fragmenta);
                    title.setText(getResources().getString(R.string.assignments));
                    break;
                case "Events": {
                    Fragment fragmente = new Polls();
                    fragmente.setArguments(bundle);
                    replaceFragment(fragmente);
                    title.setText(getResources().getString(R.string.polls));
                    break;
                }
                case "RecentActivity": {
                    Fragment fragmentt = new RecentActivities();
                    fragmentt.setArguments(bundle);
                    replaceFragment(fragmentt);
                    title.setText(getResources().getString(R.string.recentactivities));
                    break;
                }
            }
        }
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_noti, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
