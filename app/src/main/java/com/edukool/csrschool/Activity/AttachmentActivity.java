package com.edukool.csrschool.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edukool.csrschool.Utils.AppPreferences;

/**
 * Created by keerthana on 8/24/2018.
 */

public class AttachmentActivity extends AppCompatActivity {
    WebView webView;
    String schoolid, url;
    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.web_attachment);
        WebView webView = new WebView(AttachmentActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        progressDialog = new ProgressDialog(this);

        progressDialog.create();
        progressDialog.show();
        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
        webView.setWebViewClient(new Callback());

        schoolid = AppPreferences.getSchool_Name(this);
        Intent i = getIntent();
        if (i.hasExtra("circular_attachment")) {
            url = i.getStringExtra("circular_attachment");

        } else if (i.hasExtra("polls_attachment")) {
            url = i.getStringExtra("polls_attachment");
        }
        // webView = findViewById(R.id.webview);
        String baseurl = null;
        switch (schoolid) {
            case "SC-001-LS":
                baseurl = "www.csrschool.edukool.com";
                break;
            case "SC-002-LS":
                baseurl = "www.csrprimaryschool.edukool.com";
                break;
            case "SC-003-LS":
                baseurl = "www.sitalakshmischool.edukool.com";
                break;
            case "SC-004-LS":
                baseurl = "www.demo.edukool.com";
                break;
            case "SC-005-LS":
                baseurl = "www.kiddiecastleschool.edukool.com";
                break;
        }
        String attach_url = baseurl + url;
        Log.e("url", "attchment " + attach_url);
      /*  webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

        });*/
        // webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+attach_url);


        //  String pdfURL = "http://dl.dropboxusercontent.com/u/37098169/Course%20Brochures/AND101.pdf";
        if (attach_url.contains(".pdf")) {
            webView.loadUrl(
                    "http://docs.google.com/gview?embedded=true&url=" + attach_url);
            progressDialog.dismiss();
        } else {
            webView.loadUrl("http://" + attach_url);
            progressDialog.dismiss();
        }

        setContentView(webView);
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

}
