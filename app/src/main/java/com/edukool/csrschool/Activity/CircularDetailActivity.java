package com.edukool.csrschool.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edukool.csrschool.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by harini on 10/4/2018.
 */

public class CircularDetailActivity extends AppCompatActivity {

    TextView title, date;
    String mtitle, mdate, mattach, menddate, mdescription, mtype;
    //JustifyTextView description;
    JustifiedTextView description;
    ImageView back;
    LinearLayout attach_layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circulardetail);

        Intent i = getIntent();

        Bundle b = i.getBundleExtra("circular");
        if (b != null) {
            mtitle = b.getString("content");
            mdate = b.getString("date");
            mattach = b.getString("attachment");

        }

        date = findViewById(R.id.subdate);
        description = findViewById(R.id.desc_text);
        back = findViewById(R.id.back);
        attach_layout = findViewById(R.id.attach_layout);
        try {
            if (mattach.equals("") || mattach.isEmpty()||mattach.equals(null)) {
                attach_layout.setVisibility(View.GONE);
            } else {
                attach_layout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            attach_layout.setVisibility(View.GONE);
        }
        description.setText(mtitle);
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mstart_date1 = null;

        try {
            mstart_date1 = inputFormatter.parse(mdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        DateFormat outputFormatter1 = new SimpleDateFormat("hh:mm a");
        String dat = outputFormatter.format(mstart_date1); // Output : 01/20/2012
        String time = outputFormatter1.format(mstart_date1); // Output : 12:00 AM
        Log.d("date", "onBindViewHolder: " + dat + time);

        date.setText(dat + " " + time);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        attach_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CircularDetailActivity.this, AttachmentActivity.class).putExtra("circular_attachment", mattach));
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
