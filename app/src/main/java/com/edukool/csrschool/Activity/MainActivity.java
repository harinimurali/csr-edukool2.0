package com.edukool.csrschool.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.edukool.csrschool.Adapter.DrawerAdapter;
import com.edukool.csrschool.Fragment.AboutFragment;
import com.edukool.csrschool.Fragment.Assignments;
import com.edukool.csrschool.Fragment.CalendarFragment;
import com.edukool.csrschool.Fragment.CircularsFragment;
import com.edukool.csrschool.Fragment.ContactFragment;
import com.edukool.csrschool.Fragment.ExamScheduleFragment;
import com.edukool.csrschool.Fragment.GalleryFragment;
import com.edukool.csrschool.Fragment.HomePageFragment;
import com.edukool.csrschool.Fragment.LanguageFragment;
import com.edukool.csrschool.Fragment.LeaveRequestFragment;
import com.edukool.csrschool.Fragment.LiveStreamFragment;
import com.edukool.csrschool.Fragment.MessageFragment;
import com.edukool.csrschool.Fragment.NewsFragment;
import com.edukool.csrschool.Fragment.PayFees;
import com.edukool.csrschool.Fragment.Polls;
import com.edukool.csrschool.Fragment.ProfileFragment;
import com.edukool.csrschool.Fragment.RecentActivities;
import com.edukool.csrschool.Fragment.ResultFragment;
import com.edukool.csrschool.Fragment.StudentAttendance;
import com.edukool.csrschool.Fragment.TimeTableFragment;
import com.edukool.csrschool.Fragment.VoiceFragment;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawer;
    ListView listView;
    GridView gridview;
    ImageView notification;
    SharedPreferences sharedPreferences;
    private ActionBarDrawerToggle drawerToggle;
    public FrameLayout fragment_container;
    public static FontTextViewSemibold title;
    FontTextViewSemibold parentname;
    FontTextViewMedium phonenumber;
    Toolbar toolbar;
    String sidemenu[];
    String fcm_key;
    WS_CallService service_Login;
    String numb, schoolid, fathername, profileimage, versioncode;
    List<ChildrenProfile> childrenProfiles = new ArrayList<>();
    boolean doubleBackToExitPressedOnce = false;

    private NotificationReceiver notificationReceiver;
    public static TextView textCartItemCount;
    public MainActivity mainActivity;
    public static int mCartItemCount = 0;
//    String[] gridViewString = {
//            "ATTENDANCE", "RECENT ACTIVITIES",
//            "ASSIGNMENTS", "RESULTS",
//            "VIDEOS", "PAY FEES",
//101338655133
//    } ;
//    int[] gridViewImageId = {
//            R.drawable.att,  R.drawable.att,
//            R.drawable.att, R.drawable.att,
//            R.drawable.att, R.drawable.att
//
//    };
    // private ListView mDrawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_drawer);
        sidemenu = getResources().getStringArray(R.array.system);
        sharedPreferences = getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE);
        Log.e("shared pref ", "value" + sharedPreferences.getInt(Constants.LANGUAGE_CHANGE, 0));
        drawer = findViewById(R.id.drawer_layout);

        mainActivity = new MainActivity();
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        gridview = (GridView) findViewById(R.id.grid_view);
        listView = (ListView) findViewById(R.id.navdrawer);
        title = (FontTextViewSemibold) findViewById(R.id.title);
        parentname = (FontTextViewSemibold) findViewById(R.id.username);
        phonenumber = (FontTextViewMedium) findViewById(R.id.mobilenumber);
        notification = (ImageView) findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));

            }
        });
        String fathername = AppPreferences.getParentName(MainActivity.this);
        String phone = AppPreferences.getParentNumber(MainActivity.this);
        Log.e("set", "" + fathername);
        parentname.setText(fathername);
        phonenumber.setText(phone);
        numb = AppPreferences.getParentNumber(getApplicationContext());
        schoolid = AppPreferences.getSchool_Name(getApplicationContext());
        fcm_key = AppPreferences.getFcmToken(getApplicationContext());
        versioncode = AppPreferences.getversioncode(MainActivity.this);
        // Log.e("Notification count", ">>> " + AppPreferences.getNotificationCount(MainActivity.this));
        if (schoolid.equals("SC-001-LS")) {
            setTitleText(getResources().getString(R.string.app_name));
        } else if (schoolid.equals("SC-002-LS")) {
            setTitleText(getResources().getString(R.string.app_name_primary));
        } else {
            setTitleText(getResources().getString(R.string.app_name));
        }
        notificationReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter("notificationListener");
        registerReceiver(notificationReceiver, intentFilter);

        //  try {
        Intent i = getIntent();
        if (i.hasExtra("type")) {
            String pushtype = i.getStringExtra("type");
            String msg = i.getStringExtra("msg");
            switch (pushtype) {
                case "Circulars":
                    Fragment fragmentc = new CircularsFragment();
                    replaceFragment(fragmentc);
                    title.setText(getResources().getString(R.string.circulars));
                    break;
                case "Assignments":
                    Fragment fragmenta = new Assignments();
                    replaceFragment(fragmenta);
                    title.setText(getResources().getString(R.string.assignments));
                    break;
                case "Events":
                    Fragment fragmente = new Polls();
                    replaceFragment(fragmente);
                    title.setText(getResources().getString(R.string.polls));
                    break;

                case "StudentActivity":
                    Fragment fragmentrecent = new RecentActivities();
                    replaceFragment(fragmentrecent);
                    title.setText(getResources().getString(R.string.recentactivities));
                    break;

                case "payment":
                    Fragment fragmentb = new PayFees();
                    replaceFragment(fragmentb);
                    title.setText(getResources().getString(R.string.payfees));
                    break;
                case "BirthdayGreetings":
                    Bundle bundle = new Bundle();
                    bundle.putString("birthday", msg);
                    HomePageFragment myObj = new HomePageFragment();
                    myObj.setArguments(bundle);
                    replaceFragment(myObj);
                    title.setText(getResources().getString(R.string.app_name));
                    break;
            }
        } else if (i.hasExtra("Notification")) {
            String type = i.getStringExtra("Notification");
            switch (type) {
                case "Circulars":
                    Fragment fragmentc = new CircularsFragment();
                    replaceFragment(fragmentc);
                    title.setText(getResources().getString(R.string.circulars));
                    break;
                case "Assignments":
                    Fragment fragmenta = new Assignments();
                    replaceFragment(fragmenta);
                    title.setText(getResources().getString(R.string.assignments));
                    break;
                case "Events": {
                    Fragment fragmente = new Polls();
                    replaceFragment(fragmente);
                    title.setText(getResources().getString(R.string.polls));
                    break;
                }
                case "RecentActivity": {
                    Fragment fragmentt = new RecentActivities();
                    replaceFragment(fragmentt);
                    title.setText(getResources().getString(R.string.recentactivities));
                    break;
                }
            }
        } else {
            Fragment fragment = new HomePageFragment();
            replaceFragment(fragment);
            if (schoolid.equals("SC-001-LS")) {
                setTitleText(String.valueOf(R.string.app_name));
            } else {
                setTitleText(String.valueOf(R.string.app_name_primary));
            }
        }
        // } catch (Exception e) {
        //   e.printStackTrace();
        // }
/*
        if (AppPreferences.getNotificationCount(this).equals("0")) {
            setupBadge(0);
        } else {
            setupBadge(Integer.parseInt(AppPreferences.getNotificationCount(this)));
        }*/
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentManager fragmentManager = getFragmentManager();
                switch (position) {
                    case 0:
                        drawer.closeDrawers();
                        Fragment fragment = new HomePageFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        if (schoolid.equals("SC-001-LS")) {
                            title.setText(getResources().getString(R.string.app_name));
                        } else {
                            title.setText(getResources().getString(R.string.app_name_primary));
                        }

                        break;

                    case 1:
                        drawer.closeDrawers();
                        Fragment fragment1 = new StudentAttendance();
                        replaceFragment(fragment1);
                        //setTitle("Attendance");
                        title.setText(getResources().getString(R.string.attendance));

                        break;
                    case 2:
                        drawer.closeDrawers();
                        Fragment fragment2 = new Assignments();
                        replaceFragment(fragment2);
                        // setTitle("Assignment");
                        title.setText(getResources().getString(R.string.assignments));
                        break;
                    case 3:
                        drawer.closeDrawers();
                        Fragment result = new ResultFragment();
                        replaceFragment(result);
                        //toolbar.setTitle("Results");
                        title.setText(getResources().getString(R.string.results));
                        break;
                    case 4:
                        drawer.closeDrawers();
                        Fragment pay = new PayFees();
                        replaceFragment(pay);
                        title.setText(getResources().getString(R.string.payfees));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 5:
                        drawer.closeDrawers();
                        Fragment leave = new LeaveRequestFragment();
                        replaceFragment(leave);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.leaverequest));
                        break;
                    case 6:
                        drawer.closeDrawers();
                        Fragment tt = new TimeTableFragment();
                        replaceFragment(tt);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.timetable));
                        break;
                    case 7:
                        drawer.closeDrawers();
                        Fragment exam = new ExamScheduleFragment();
                        replaceFragment(exam);
                        title.setText(getResources().getString(R.string.ecamschedule));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 8:
                        drawer.closeDrawers();
                        Fragment fragmentt = new RecentActivities();
                        replaceFragment(fragmentt);
                        // setTitle("Recent Activities");
                        title.setText(getResources().getString(R.string.recentactivities));
                        break;

                    case 9:
                        drawer.closeDrawers();
                        Fragment news = new NewsFragment();
                        replaceFragment(news);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.news));
                        break;
                    case 10:
                        drawer.closeDrawers();
                        Fragment pol = new Polls();
                        replaceFragment(pol);
                        title.setText(getResources().getString(R.string.polls));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 11:
                        drawer.closeDrawers();
                        Fragment circle = new CircularsFragment();
                        replaceFragment(circle);
                        // toolbar.setTitle("Circulars");
                        title.setText(getResources().getString(R.string.circulars));
                        break;

                    case 12:
                        drawer.closeDrawers();
                        Fragment cont = new ContactFragment();
                        replaceFragment(cont);
                        title.setText(getResources().getString(R.string.contact));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 13:
                        drawer.closeDrawers();
                        Fragment msg = new MessageFragment();
                        replaceFragment(msg);
                        title.setText(getResources().getString(R.string.message));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 14:
                        drawer.closeDrawers();
                        Fragment voice = new VoiceFragment();
                        replaceFragment(voice);
                        title.setText(getResources().getString(R.string.voicemsg));
                        // title.setText("Language");
                        break;
                    case 15:
                        drawer.closeDrawers();
                        Fragment gal = new GalleryFragment();
                        replaceFragment(gal);
                        title.setText(getResources().getString(R.string.gallery));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 16:
                        drawer.closeDrawers();
                        Intent i = new Intent(MainActivity.this, VideoActivity.class);
                        startActivity(i);

                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 17:
                        drawer.closeDrawers();
                        Fragment live = new LiveStreamFragment();
                        replaceFragment(live);
                        title.setText(getResources().getString(R.string.livestream));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 18:
                        drawer.closeDrawers();
                        Fragment cal = new CalendarFragment();
                        replaceFragment(cal);
                        title.setText(getResources().getString(R.string.calendar));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 19:
                        drawer.closeDrawers();
                        Fragment prof = new ProfileFragment();
                        replaceFragment(prof);
                        title.setText(getResources().getString(R.string.profile));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;


                    case 20:
                        drawer.closeDrawers();
                        Fragment about = new AboutFragment();
                        replaceFragment(about);
                        title.setText(getResources().getString(R.string.about));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 21:
                        drawer.closeDrawers();
                        Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
                       /* Fragment lang = new LanguageFragment();
                        replaceFragment(lang);
                        title.setText(getResources().getString(R.string.language));*/
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 22:
                        drawer.closeDrawers();
                        Fragment lang = new LanguageFragment();
                        replaceFragment(lang);
                        title.setText(getResources().getString(R.string.language));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 23:
                        drawer.closeDrawers();
                        logoutDialog();
                        break;

                }
            }
        });

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.info_icon);
            toolbar.setTitleTextColor(getResources().getColor(R.color.abc_primary_text_material_dark));
            toolbar.setTitle("Edukool Parent");
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);

        //  mDrawerList = (ListView) findViewById(R.id.navdrawer);
        DrawerAdapter adapter1 = new DrawerAdapter(MainActivity.this, sidemenu);
        listView.setAdapter(adapter1);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        GridViewadapter adapterViewAndroid = new GridViewadapter(MainActivity.this, gridViewString, gridViewImageId);
//        gridview=(GridView)findViewById(R.id.grid_view);
//
//        gridview.setAdapter(adapterViewAndroid);
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int i, long id) {
//             //   Toast.makeText(GridViewImageTextActivity.this, "GridView Item: " + gridViewString[+i], Toast.LENGTH_LONG).show();
//            }
//        });
    }


    public void setTitleText(String text) {
        title.setText(text);
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
    public void replaceFragment1(Fragment fragment,int flag) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, flag);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {


            Log.e("back_arrow pressed", "back_arrow pressed");

            FragmentManager manager = getSupportFragmentManager();

            Log.e("manager", "entrycount" + manager.getBackStackEntryCount());

            if (manager.getBackStackEntryCount() == 1) {

           /* // Toast.makeText(MainActivity.this, "skjfghgsdfhjdsf", Toast.LENGTH_SHORT).show();
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getBaseContext(), "Tap back_arrow button in order to exit", Toast.LENGTH_SHORT).show();
            }*/

                //  mBackPressed = System.currentTimeMillis();
                String backStateName;
                backStateName = ((Object) new HomePageFragment()).getClass().getName();
                String fragmentTag = backStateName;

                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.main, new HomePageFragment(), fragmentTag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(backStateName);
                ft.commit();

                if (doubleBackToExitPressedOnce) {
                    finish();
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
            super.onBackPressed();
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    public void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.confrim);
        builder.setMessage(R.string.logout_string)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        CallApiLogout();
                       /* List<ChildrenProfile> ch = new ArrayList<>();
                        ChildrenProfile p = new ChildrenProfile();
                        AppPreferences.setParentNumber(MainActivity.this, "");
                        AppPreferences.setLanguage(MainActivity.this, 0);
                        AppPreferences.setParentName(MainActivity.this, "");
                        AppPreferences.saveLoginData(MainActivity.this, false, "", "", "", "", "", "", "", "");
                        AppPreferences.setchildrenProfile(MainActivity.this, ch);
                        AppPreferences.setParentImgae(MainActivity.this, "");
                        startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));*/

                        // FIRE ZE MISSILES!
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void callAPI() {
        try {
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            String fcm_key = AppPreferences.getFcmToken(MainActivity.this);

            byte[] data;
            String fcmtoken = AppPreferences.getFcmToken(MainActivity.this);
            String login_str = "UserName:" + schoolid + "|parentId:" + numb + "|password:par|Function:SplashScreen|DeviceType:android|GCMKey:''|fcm_userkey:" + fcm_key + "|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            //  String login_str = "UserName:'" + schoolid + "'|parentId:'9994922081'|password:par|Function:ParentLogin|DeviceType:android|GCMKey:'"+fcmtoken+"'|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Login_WS load_plan_list = new Load_Login_WS(MainActivity.this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            childrenProfiles = new ArrayList<>();

            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONArray array = jObj.getJSONArray("Childrens");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        ChildrenProfile profile = new ChildrenProfile();
                        profile.setID(jsonObject.getString("ID"));
                        profile.setFatherName(jsonObject.getString("FatherName"));
                        fathername = jsonObject.getString("FatherName");
                        profileimage = jsonObject.getString("ProfileImage");
                        profile.setName(jsonObject.getString("Name"));
                        profile.setProfileImage(jsonObject.getString("ProfileImage"));
                        profile.setClassId(jsonObject.getString("ClassId"));
                        profile.setClasses(jsonObject.getString("Class"));
                        profile.setSection(jsonObject.getString("Section"));
                        profile.setFees(jsonObject.getString("Fees"));
                        childrenProfiles.add(profile);
                    }
                    AppPreferences.setchildrenProfile(MainActivity.this, childrenProfiles);
                    AppPreferences.saveLoginData(MainActivity.this, true, fathername, profileimage,
                            childrenProfiles.get(0).getID(), childrenProfiles.get(0).getClassId(),
                            childrenProfiles.get(0).getClasses(), childrenProfiles.get(0).getSection(), childrenProfiles.get(0).getFees(), "+91 " + numb, childrenProfiles.get(0).getEncryptedStudID());
                    AppPreferences.setParentName(MainActivity.this, fathername);
                    AppPreferences.setParentImgae(MainActivity.this, profileimage);
                    AppPreferences.setParentNumber(MainActivity.this, numb);
                    AppPreferences.setSchool_Name(MainActivity.this, schoolid);
                    AppPreferences.setStudentPosition(MainActivity.this, 0);
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);
                    Toast.makeText(MainActivity.this, "Please Register your number", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


   /* public String getVersionInstalled() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return null;
    }*/

    private void CallApiLogout() {
        try {
            ArrayList<NameValuePair> logout = new ArrayList<NameValuePair>();

            byte[] data;
            String login_str = "UserName:" + schoolid + "|parentId:" + numb + "|Function:ParentLogout|DeviceType:android|GCMKey:''|fcm_userkey:" + fcm_key + "DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            logout.add(new BasicNameValuePair("WS", base64_register));
            Load_Logout_WS load_plan_list = new Load_Logout_WS(getApplicationContext(), logout);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class Load_Logout_WS extends AsyncTask<String, String, String> {
        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Logout_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            //  hideProgressBar();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    List<ChildrenProfile> ch = new ArrayList<>();
                    ChildrenProfile p = new ChildrenProfile();
                    AppPreferences.setParentNumber(MainActivity.this, "");
                    AppPreferences.setLanguage(MainActivity.this, 0);
                    AppPreferences.setParentName(MainActivity.this, "");
                    AppPreferences.saveLoginData(MainActivity.this, false, "", "", "", "", "", "", "", "", "");
                    AppPreferences.setchildrenProfile(MainActivity.this, ch);
                    AppPreferences.setParentImgae(MainActivity.this, "");
                    AppPreferences.setLanguage(MainActivity.this, 3);
                    startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                }
            } catch (Exception e) {

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.item1);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        textCartItemCount.setVisibility(View.GONE);

        // setupBadge();
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.item1:
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void setupBadge(int count) {

        if (textCartItemCount != null) {
            if (count == 0) {
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                // textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            if (intent.getBooleanExtra("notify", false)) {
                if (textCartItemCount.getVisibility() != View.VISIBLE)
                    textCartItemCount.setVisibility(View.VISIBLE);
                AppPreferences.setNotificationCount(MainActivity.this, "1");

                /*int notificationCount;
                if (!textCartItemCount.getText().toString().equalsIgnoreCase("9+"))
                    notificationCount = Integer.parseInt(textcart_badge.getText().toString()) + 1;
                else
                    notificationCount = 10;

                if (notificationCount > 9)
                    textcart_badge.setText("9+");
                else
                    textcart_badge.setText("" + notificationCount);*/

            } else {
                textCartItemCount.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (schoolid.equals("SC-001-LS")) {
            setTitleText(getResources().getString(R.string.app_name));
        } else if (schoolid.equals("SC-002-LS")) {
            setTitleText(getResources().getString(R.string.app_name_primary));
        }
        if (AppPreferences.getNotificationCount(MainActivity.this).equals("0")) {
            // textCartItemCount.setVisibility(View.GONE);
        }
    }
}


