package com.edukool.csrschool.Activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.edukool.csrschool.Adapter.VideoAdapter;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.models.YoutubeVideos;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoActivity extends Activity {

    private RecyclerView recyclerView;
    // private Vector<YoutubeVideos> YoutubeVideos = new Vector<YoutubeVideos>();
    ImageView back;
    List<YoutubeVideos> youtubeVideosList;
    ProgressBar progressBar;
    WS_CallService service_Login;
    VideoAdapter videoAdapter;
    String ParentNumber, schoolid, versioncode;
    FontTextViewMedium norecords;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        norecords = (FontTextViewMedium) findViewById(R.id.norecords);
        back = (ImageView) findViewById(R.id.back);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ParentNumber = AppPreferences.getParentNumber(VideoActivity.this);
        schoolid = AppPreferences.getSchool_Name(VideoActivity.this);
        versioncode = AppPreferences.getversioncode(VideoActivity.this);

      /*  youtubeVideosList = new ArrayList<>();

        youtubeVideosList.add(
                new YoutubeVideos(
                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/V3auP0XRp8g\" frameborder=\"0\" allowfullscreen></iframe>",

                        "Maths",
                        "All Units are covered in this video",
                        "www.mathstutorial.com"


                ));
        youtubeVideosList.add(
                new YoutubeVideos(
                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/p6qVJ1KhHek\" frameborder=\"0\" allowfullscreen></iframe>",

                        "English",
                        "All Units are covered in this video",
                        "www.englishtutorial.com"


                ));

        youtubeVideosList.add(
                new YoutubeVideos(
                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/RPVe3R3STv4\" frameborder=\"0\" allowfullscreen></iframe>",

                        "Tamil",
                        "All Units are covered in this video",
                        "www.tamiltutorial.com"


                ));

        youtubeVideosList.add(
                new YoutubeVideos(
                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/TEYN7S81e88\" frameborder=\"0\" allowfullscreen></iframe>",

                        "Science",
                        "All Units are covered in this video",
                        "www.sciencetutorial.com"


                ));

        youtubeVideosList.add(
                new YoutubeVideos(
                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe>",

                        "Social",
                        "All Units are covered in this video",
                        "www.socialtutorial.com"


                ));
*/

//        youtubeVideosList = new ArrayList<>();
//
//        youtubeVideosList.add(
//                new YoutubeVideos(
//                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/V3auP0XRp8g\" frameborder=\"0\" allowfullscreen></iframe>",
//
//                        "Maths",
//                        "All Units are covered in this video",
//                        "www.mathstutorial.com"
//
//
//                ));
//        youtubeVideosList.add(
//                new YoutubeVideos(
//                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/p6qVJ1KhHek\" frameborder=\"0\" allowfullscreen></iframe>",
//
//                        "English",
//                        "All Units are covered in this video",
//                        "www.englishtutorial.com"
//
//
//                ));
//
//        youtubeVideosList.add(
//                new YoutubeVideos(
//                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/RPVe3R3STv4\" frameborder=\"0\" allowfullscreen></iframe>",
//
//                        "Tamil",
//                        "All Units are covered in this video",
//                        "www.tamiltutorial.com"
//
//
//                ));
//
//        youtubeVideosList.add(
//                new YoutubeVideos(
//                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/TEYN7S81e88\" frameborder=\"0\" allowfullscreen></iframe>",
//
//                        "Science",
//                        "All Units are covered in this video",
//                        "www.sciencetutorial.com"
//
//
//                ));
//
//        youtubeVideosList.add(
//                new YoutubeVideos(
//                        "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe>",
//
//                        "Social",
//                        "All Units are covered in this video",
//                        "www.socialtutorial.com"
//
//
//                ));


//       YoutubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/V3auP0XRp8g\" frameborder=\"0\" allowfullscreen></iframe>") );
//        YoutubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/p6qVJ1KhHek\" frameborder=\"0\" allowfullscreen></iframe>") );
//        YoutubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/RPVe3R3STv4\" frameborder=\"0\" allowfullscreen></iframe>") );
//        YoutubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/TEYN7S81e88\" frameborder=\"0\" allowfullscreen></iframe>") );
//        YoutubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe>") );

      /*  VideoAdapter videoAdapter = new VideoAdapter(getApplication(), youtubeVideosList);
        recyclerView.setAdapter(videoAdapter);*/

//        VideoAdapter videoAdapter = new VideoAdapter(getApplication(), youtubeVideosList);
//        recyclerView.setAdapter(videoAdapter);

        RecyclerView.ItemDecoration divider = new RecyclerView.ItemDecoration() {
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.setEmpty();
            }
        };
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               onBackPressed();
            }
        });
        if (Permission.checknetwork(VideoActivity.this)) {
            callAPI();
        }
    }


    private void callAPI() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            //  String schoolid = "SC-005-LS";
            byte[] data;
            //  String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:" + studentid + "|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            String login_str = "UserName:" + schoolid + "|parentId:'" + ParentNumber + "'|Function:Videos|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Vidoes_WS load_plan_list = new Load_Vidoes_WS(VideoActivity.this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    public class Load_Vidoes_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Vidoes_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);

            youtubeVideosList = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    /*if (jObj.getString("StatusCode").equals("201")) {
                        norecords.setVisibility(View.VISIBLE);
                    } else {*/
                    norecords.setVisibility(View.GONE);
                    JSONArray array = jObj.getJSONArray("Videos");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        YoutubeVideos recent = new YoutubeVideos();
                        recent.setId(jsonObject.getString("ID"));
                        recent.setVideotitle(jsonObject.getString("Title"));
                        String video = jsonObject.getString("URL");
                        recent.setVideoUrl(video);

                        // recent.setVideoUrl("<iframe width=\"100%\" height=\"100%\" src= \""+video+"\" frameborder=\"0\" allowfullscreen></iframe>");

                        //   recent.setVideoUrl("<iframe width=100%height=100%src="+jsonObject.getString("URL")+" frameborder=\"0\" allowfullscreen></iframe>");

                        recent.setImage(jsonObject.getString("Image"));
                        recent.setDate(jsonObject.getString("Date"));
                        youtubeVideosList.add(recent);
                    }
                    videoAdapter = new VideoAdapter(VideoActivity.this, youtubeVideosList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VideoActivity.this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(videoAdapter);

                    // }
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    //Toast.makeText(getApplication(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    /* @Override
    public void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
    }*/

}



