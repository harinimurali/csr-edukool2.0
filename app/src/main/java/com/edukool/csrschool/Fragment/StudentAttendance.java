package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.CalanderModels;
import com.edukool.csrschool.models.ChildrenProfile;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentAttendance extends Fragment {

    List<ChildrenProfile> childrenProfiles;
    String studentid, current_date, student_name;
    WS_CallService service_Login;
    Long timeInMilliseconds, timeInMilliseconds1;
    CompactCalendarView calendarView;
    List<CalanderModels> calanderModels;
    FontTextViewSemibold monthtext;
    FontTextViewMedium present_text;
    List<Event> events;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    String ParentNumber, schoolid, versioncode, stuClassid;
    private MaterialCalendarView cv;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy");
    ProgressBar progressBar;
    LinearLayout linearlayout;
    ProgressDialog progDailog;
    FontTextViewSemibold precent_percentage, absent_percentage, holiday_percentage, leave_percentage;

    public StudentAttendance() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_student_attendance, container, false);

        String[] values =
                {"GOWTHAMI", "JANANI", "PREETHI", "KUMARAN", "PRIYA", "DHEENADAYALAN", "PRAVEENA", "ASHWINI",};
        TextView textview = (TextView) v.findViewById(R.id.text);
        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
      /*  ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(LTRadapter);*/

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        calendarView = (CompactCalendarView) v.findViewById(R.id.compactcalendarview);
        monthtext = (FontTextViewSemibold) v.findViewById(R.id.caltext);
        present_text = (FontTextViewMedium) v.findViewById(R.id.present_sign);
        progressBar = (ProgressBar) v.findViewById(R.id.progressIndicator);
        linearlayout = v.findViewById(R.id.linearLayout);

        precent_percentage = v.findViewById(R.id.precent_percentage);
        absent_percentage = v.findViewById(R.id.absent_percentage);
        holiday_percentage = v.findViewById(R.id.holiday_percentage);
        leave_percentage = v.findViewById(R.id.leave_percentage);

        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());

        present_text.setVisibility(View.GONE);

        stuname = v.findViewById(R.id.student_name);
        stuclass = v.findViewById(R.id.student_class);
        stuimage = v.findViewById(R.id.student_image);
        drpDown = v.findViewById(R.id.drpDown);

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }
        progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
        progDailog.setCancelable(false);

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        stuClassid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClassId();

        student_name = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName();
        if (Permission.checknetwork(getActivity())) {
            callAPI();
        }
        cv = v.findViewById(R.id.material_calender);
        cv.clearSelection();
        new ArrayList().add(new CalendarDay());
        this.cv.addDecorator(new DayDecorator());
        this.cv.addDecorator(new AbsentDecorators());
        //  this.cv.addDecorator(new CurrentDateDecorator());
        cv.state().edit().setFirstDayOfWeek(1).setMinimumDate(CalendarDay.from(2018, 4, 1)).setMaximumDate(CalendarDay.from(2050, 12, 12)).setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        cv.setCurrentDate(Calendar.getInstance());
        cv.getCurrentDate();
        cv.addDecorator(new EventDecorator(getActivity().getResources().getColor(R.color.gray), CalendarDay.today()));

        cv.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                Log.e("selection", "day>> " + getdateConvert(String.valueOf(calendarDay.getDate())));
                current_date = getdateConvert(String.valueOf(calendarDay.getDate()));
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
                // getdateConvert(String.valueOf(calendarDay.getDate()));

            }
        });
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), R.layout.spinner_layout, childrenProfiles);

        calendarView.shouldDrawIndicatorsBelowSelectedDays(false);
        calendarView.setUseThreeLetterAbbreviation(true);
        calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = calendarView.getEvents(dateClicked);
                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                monthtext.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "set of child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                stuClassid = childrenProfiles.get(pos).getClassId();
                student_name = childrenProfiles.get(pos).getName();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println(formatter.format(date));
        current_date = formatter.format(date);

        SimpleDateFormat formate = new SimpleDateFormat("MMM yyyy");

        Log.e("date", "mDate" + formate.format(date));
        monthtext.setText(formate.format(date));

      /*  callAPI();*/
        SimpleDateFormat formatterr = new SimpleDateFormat("dd/MM/yyyy");
        Date datee = new Date();
        System.out.println(formatterr.format(datee));

        String date1 = formatterr.format(datee);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date mDate = sdf.parse(date1);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // calendarView.setMaxDate(timeInMilliseconds);


        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy");
        Date dates = new Date();
        System.out.println(formatter1.format(dates));
        String dates1 = formatter1.format(dates);
        Log.e("dates1", "" + dates1);


        String date2 = "01/06/" + dates1;
        Log.e("date2", "" + date2);


        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date mDate = sdf1.parse(date2);
            String a = String.valueOf(mDate.getTime());
            timeInMilliseconds1 = Long.parseLong(a);
            // timeInMilliseconds1 = mDate.getTime();
            System.out.println("Date in milli1test :: " + timeInMilliseconds1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // calendarView(timeInMilliseconds1);
      /*  Event ev1 = new Event(Color.GREEN, 1529844651000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev1);
        Event ev2 = new Event(Color.GREEN, 1530017451000L);
        calendarView.addEvent(ev2);
        Event ev3 = new Event(Color.CYAN, 1529758251000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev3);
        Event ev4 = new Event(Color.RED, 1529671851000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev4);*/
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return v;


        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_student_attendance, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.attendance));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        stuClassid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClassId();
        student_name = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName();
        callAPI();
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                stuClassid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClassId();
                student_name = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName();
                callAPI();
                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAPI() {
        if (Permission.checknetwork(getActivity())) {
            try {
                progDailog.show();
                // progressBar.setVisibility(View.VISIBLE);
                // linearlayout.setVisibility(View.GONE);
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                //   String schoolid = "SC-001-LS";
                // studentid = "1";
                byte[] data;
                //   String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:"+studentid+"|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                String login_str = "UserName:" + schoolid + "|studentId:" + studentid + "|ClassID:" + stuClassid + "|date:" + current_date + "|Parent_ID:" + ParentNumber + "|Function:StudentAttendance|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Attendance_WS load_plan_list = new Load_Attendance_WS(getContext(), login);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            progDailog.dismiss();
        }

    }


    public class Load_Attendance_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Attendance_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            calendarView.removeAllEvents();
            calanderModels = new ArrayList<>();
            events = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                String total = null;
                if (status.equalsIgnoreCase("Success")) {
                    calanderModels.clear();
                    ArrayList eventspresent = new ArrayList<>();
                    ArrayList eventsabs = new ArrayList<>();
                    ArrayList eventholiday = new ArrayList<>();
                    ArrayList eventleave = new ArrayList<>();
                    JSONArray array = jObj.getJSONArray("Attendance");
                    if (array.length() != 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            CalanderModels recent = new CalanderModels();
                            recent.setDate(jsonObject.getString("date"));
                            recent.setPresent(jsonObject.getString("Present"));

                            recent.setTotalworkingdays(jsonObject.getString("TotalWorkingDays"));
                            total = jsonObject.getString("TotalWorkingDays");
                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

                            try {
                                Date mDate = sdf1.parse(jsonObject.getString("date"));
                                //  timeInMilliseconds1 = mDate.getTime();
                                //  String a = String.valueOf(timeInMilliseconds1);


                                // Long l = Long.parseLong(a);
                                //  System.out.println("Date in milli1 :: " + timeInMilliseconds1);
                                //  System.out.println("long in milli1 :: " + l);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            CalendarDay calendarDay;
                            ArrayList arrayList;
                            if (jsonObject.getString("Present").equals("0")) {
                                Event ev = new Event(getActivity().getResources().getColor(R.color.holiday), timeInMilliseconds1);
                                if (current_date.equals(jsonObject.getString("date"))) {

                                    //    calendarView.setCurrentDayBackgroundColor(getActivity().getResources().getColor(R.color.clr1));
                                    //  calendarView.setSelected(false);
                                    present_text.setText(student_name + " is Absent Today");
                                    //  present_text.setVisibility(View.VISIBLE);
                                    // calendarView.setCurrentDayIndicatorStyle();
                                    // calendarView.setCurrentSelectedDayBackgroundColor(getActivity().getResources().getColor(R.color.clr1));

                                }/*else {
                                present_text.setVisibility(View.GONE);
                            }*/
                                calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.getString("date")));
                                arrayList = new ArrayList();
                                arrayList.add(calendarDay);
                                eventsabs.add(calendarDay);
                                cv.addDecorator(new AbsentDecorator(arrayList));
                                //  calendarView.addEvent(ev);
                                //  events.add(ev);
                            } else if (jsonObject.getString("Present").equals("2")) {
                                calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.getString("date")));
                                arrayList = new ArrayList();
                                arrayList.add(calendarDay);
                                eventleave.add(calendarDay);
                                cv.addDecorator(new LeaveDecorator(arrayList));
                            } else {
                                Event ev = new Event(getActivity().getResources().getColor(R.color.present), timeInMilliseconds1);
                                if (current_date.equals(jsonObject.getString("date"))) {
                                    // calendarView.setCurrentSelectedDayBackgroundColor(getActivity().getResources().getColor(R.color.clr2));
                                    //calendarView.setSelected(false);
                                    // present_text.setVisibility(View.VISIBLE);
                                    //calendarView.setCurrentDayBackgroundColor(getActivity().getResources().getColor(R.color.clr2));
                                    present_text.setText(student_name + " is Present Today");
                                }/*else {
                                present_text.setVisibility(View.GONE);
                            }*/
                                calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.getString("date")));
                                arrayList = new ArrayList();
                                arrayList.add(calendarDay);
                                eventspresent.add(calendarDay);
                                cv.addDecorator(new PresentDecorator(arrayList));
                                // Event ev = new Event(getActivity().getResources().getColor(R.color.clr2), Long.parseLong(String.valueOf(timeInMilliseconds1)));
                                // calendarView.addEvent(ev);
                                // events.add(ev);
                            }
                            // calendarView.addEvents(events);
                            // calendarView.addEvents(eventsabs);

                            //  calanderModels.add(recent);
                        }
                    }
                    JSONObject object = jObj.getJSONObject("MonthlyHolidaysList");
                    if (object.has("MonthlyHolidays")) {
                        JSONArray array1 = object.getJSONArray("MonthlyHolidays");
                        for (int i = 0; i < array1.length(); i++) {
                            JSONObject object1 = array1.getJSONObject(i);
                            CalendarDay calendarDay;
                            ArrayList arrayListholoday;
                            CalanderModels.MonthlyHolidays monthlyHolidays = new CalanderModels.MonthlyHolidays();
                            monthlyHolidays.setId(object1.getString("ID"));
                            monthlyHolidays.setEventname(object1.getString("EventName"));
                            monthlyHolidays.setEventstartdate(object1.getString("EventStartDate"));
                            calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(object1.getString("EventStartDate")));
                            arrayListholoday = new ArrayList();
                            arrayListholoday.add(calendarDay);
                            eventholiday.add(calendarDay);
                            cv.addDecorator(new HolidayDecorator(arrayListholoday));

                        }
                    }
                    // progressBar.setVisibility(View.GONE);
                    //  linearlayout.setVisibility(View.VISIBLE);
                    progDailog.dismiss();
                    Log.e("list", "populate" + calanderModels.toString());
                    System.out.println("success");

                    int hol = eventholiday.size();
                    int tot = Integer.parseInt(total);
                    int t = tot - hol;

                    precent_percentage.setText(String.valueOf(eventspresent.size()) + " / " + t);
                    absent_percentage.setText(String.valueOf(eventsabs.size()));
                    leave_percentage.setText(String.valueOf(eventleave.size()));
                    holiday_percentage.setText(String.valueOf(eventholiday.size()));
                } else {
                    System.out.println("failed");
                    ArrayList eventholiday = new ArrayList<>();
                    JSONObject object = jObj.getJSONObject("MonthlyHolidaysList");
                    if (object.has("MonthlyHolidays")) {
                        JSONArray array1 = object.getJSONArray("MonthlyHolidays");
                        for (int i = 0; i < array1.length(); i++) {
                            JSONObject object1 = array1.getJSONObject(i);
                            CalendarDay calendarDay;
                            ArrayList arrayListholoday;
                            CalanderModels.MonthlyHolidays monthlyHolidays = new CalanderModels.MonthlyHolidays();
                            monthlyHolidays.setId(object1.getString("ID"));
                            monthlyHolidays.setEventname(object1.getString("EventName"));
                            monthlyHolidays.setEventstartdate(object1.getString("EventStartDate"));
                            calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(object1.getString("EventStartDate")));
                            arrayListholoday = new ArrayList();
                            arrayListholoday.add(calendarDay);
                            eventholiday.add(calendarDay);
                            cv.addDecorator(new HolidayDecorator(arrayListholoday));

                        }
                    }
                    precent_percentage.setText("0");
                    absent_percentage.setText("0");
                    leave_percentage.setText("0");
                    holiday_percentage.setText(String.valueOf(eventholiday.size()));
                    //  String Message = jObj.getString("Message");
                    progDailog.dismiss();
                    // progressBar.setVisibility(View.GONE);
                    // linearlayout.setVisibility(View.VISIBLE);

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    //  Toast.makeText(getActivity(), "No Records found", Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {
                progDailog.dismiss();
                System.out.println(e.toString() + "zcx");
                Log.e("zcx", "" + e.toString());
            }

        }

    }

    public class AbsentDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public AbsentDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.holiday)));
        }
    }

    public class AbsentDecorators implements DayViewDecorator {
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return Boolean.parseBoolean(null);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.holiday)));
        }
    }

    public class PresentDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public PresentDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.present)));
        }
    }

    public class HolidayDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public HolidayDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.clr2)));
        }
    }

    public class LeaveDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public LeaveDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.leave)));
        }
    }

    /*
        public class CurrentDateDecorator implements DayViewDecorator {
            private CalendarDay date = CalendarDay.today();

            public boolean shouldDecorate(CalendarDay calendarDay) {
                return this.date == null ? null : true;
            }

            public void decorate(DayViewFacade dayViewFacade) {
                dayViewFacade.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.absent_circle));
                dayViewFacade.addSpan(new ForegroundColorSpan(-1));
            }

            public void setDate(Date date) {
                this.date = CalendarDay.from(date);
            }
        }
    */
    public class DayDecorator implements DayViewDecorator {
        private final Calendar calendar = Calendar.getInstance();

        public boolean shouldDecorate(CalendarDay calendarDay) {
            calendarDay.copyTo(this.calendar);
            return this.calendar.get(Calendar.DAY_OF_WEEK) == 1;
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.gray)));
        }
    }

    public class EventDecorator implements DayViewDecorator {

        private final int color;
        private final CalendarDay today;

        public EventDecorator(int color, CalendarDay today) {
            this.color = color;
            this.today = today;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return day.equals(today);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.absent_circle));
            view.addSpan(new ForegroundColorSpan(-1));
        }
    }

    public String getdateConvert(String currentda) {
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = (Date) formatter.parse(currentda);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String formatedDate = outputFormat.format(date);
        System.out.println("formatedDate : " + formatedDate);
        return formatedDate;
    }

}

