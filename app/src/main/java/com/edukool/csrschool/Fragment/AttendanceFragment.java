package com.edukool.csrschool.Fragment;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.CalanderModels;
import com.edukool.csrschool.models.ChildrenProfile;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {

    List<ChildrenProfile> childrenProfiles;
    String studentid, current_date;
    WS_CallService service_Login;
    Long timeInMilliseconds, timeInMilliseconds1;
    CompactCalendarView calendarView;
    List<CalanderModels> calanderModels;
    FontTextViewSemibold monthtext;
    List<Event> events;
    String ParentNumber, schoolid;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy");

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_student_attendance, container, false);

        TextView textview = (TextView) v.findViewById(R.id.text);
        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);


      /*  ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(LTRadapter);*/

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        calendarView = (CompactCalendarView) v.findViewById(R.id.compactcalendarview);
        monthtext = (FontTextViewSemibold) v.findViewById(R.id.caltext);
        stuname = v.findViewById(R.id.student_name);
        stuclass = v.findViewById(R.id.student_class);
        stuimage = v.findViewById(R.id.student_image);
        drpDown = v.findViewById(R.id.drpDown);


        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), R.layout.spinner_layout, childrenProfiles);
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        calendarView.shouldDrawIndicatorsBelowSelectedDays(false);
        calendarView.setUseThreeLetterAbbreviation(true);
        calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = calendarView.getEvents(dateClicked);
                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                monthtext.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);
        callAPI();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "set of child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println(formatter.format(date));
        current_date = formatter.format(date);

        SimpleDateFormat formate = new SimpleDateFormat("MMM yyyy");

        Log.e("date", "mDate" + formate.format(date));
        monthtext.setText(formate.format(date));

     /*   if (Permission.checknetwork(getActivity())) {
            callAPI();
        }*/
        SimpleDateFormat formatterr = new SimpleDateFormat("dd/MM/yyyy");
        Date datee = new Date();
        System.out.println(formatterr.format(datee));

        String date1 = formatterr.format(datee);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date mDate = sdf.parse(date1);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // calendarView.setMaxDate(timeInMilliseconds);


        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy");
        Date dates = new Date();
        System.out.println(formatter1.format(dates));
        String dates1 = formatter1.format(dates);
        Log.e("dates1", "" + dates1);


        String date2 = "01/06/" + dates1;
        Log.e("date2", "" + date2);


        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date mDate = sdf1.parse(date2);
            String a = String.valueOf(mDate.getTime());
            timeInMilliseconds1 = Long.parseLong(a);
            // timeInMilliseconds1 = mDate.getTime();
            System.out.println("Date in milli1test :: " + timeInMilliseconds1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // calendarView(timeInMilliseconds1);
      /*  Event ev1 = new Event(Color.GREEN, 1529844651000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev1);
        Event ev2 = new Event(Color.GREEN, 1530017451000L);
        calendarView.addEvent(ev2);
        Event ev3 = new Event(Color.CYAN, 1529758251000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev3);
        Event ev4 = new Event(Color.RED, 1529671851000L, "Some extra data that I want to store.");
        calendarView.addEvent(ev4);*/
        return v;


        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_student_attendance, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.attendance));
    }


    private void callAPI() {
        try {
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            //  String schoolid = "SC-001-LS";
            // studentid = "1";
            byte[] data;
            //   String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:"+studentid+"|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            String login_str = "UserName:" + schoolid + "|studentId:" + studentid + "|date:" + current_date + "|Parent_ID:" + ParentNumber + "|Function:StudentAttendance|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Attendance_WS load_plan_list = new Load_Attendance_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class Load_Attendance_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Attendance_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            calendarView.removeAllEvents();
            calanderModels = new ArrayList<>();
            events = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    calanderModels.clear();
                    List<Event> eventspresent = new ArrayList<>();
                    List<Event> eventsabs = new ArrayList<>();
                    JSONArray array = jObj.getJSONArray("Attendance");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        CalanderModels recent = new CalanderModels();
                        recent.setDate(jsonObject.getString("date"));
                        recent.setPresent(jsonObject.getString("Present"));
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            Date mDate = sdf1.parse(jsonObject.getString("date"));
                            timeInMilliseconds1 = mDate.getTime();
                            String a = String.valueOf(timeInMilliseconds1);


                            Long l = Long.parseLong(a);
                            System.out.println("Date in milli1 :: " + timeInMilliseconds1);
                            System.out.println("long in milli1 :: " + l);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (jsonObject.getString("Present").equals("0")) {
                            Event ev = new Event(getActivity().getResources().getColor(R.color.clr1), timeInMilliseconds1);
                            if (current_date.equals(jsonObject.getString("date"))) {

                                calendarView.setCurrentDayBackgroundColor(getActivity().getResources().getColor(R.color.clr1));
                                calendarView.setSelected(false);
                                // calendarView.setCurrentDayIndicatorStyle();
                                // calendarView.setCurrentSelectedDayBackgroundColor(getActivity().getResources().getColor(R.color.clr1));

                            }

                            calendarView.addEvent(ev);
                            //  events.add(ev);
                        } else {
                            Event ev = new Event(getActivity().getResources().getColor(R.color.clr2), timeInMilliseconds1);
                            if (current_date.equals(jsonObject.getString("date"))) {
                                // calendarView.setCurrentSelectedDayBackgroundColor(getActivity().getResources().getColor(R.color.clr2));
                                calendarView.setSelected(false);
                                calendarView.setCurrentDayBackgroundColor(getActivity().getResources().getColor(R.color.clr2));
                            }
                            // Event ev = new Event(getActivity().getResources().getColor(R.color.clr2), Long.parseLong(String.valueOf(timeInMilliseconds1)));
                            calendarView.addEvent(ev);
                            // events.add(ev);
                        }
                        // calendarView.addEvents(events);
                        // calendarView.addEvents(eventsabs);

                        //  calanderModels.add(recent);
                    }

                    Log.e("list", "populate" + calanderModels.toString());
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
                Log.e("zcx", "" + e.toString());
            }

        }

    }


}

