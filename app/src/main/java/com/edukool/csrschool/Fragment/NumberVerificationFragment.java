package com.edukool.csrschool.Fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.edukool.csrschool.Activity.LoginActivity;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 6/12/2018.
 */

public class NumberVerificationFragment extends Fragment {
    Spinner selectschool;
    EditText phonenumber;
    Button next;
    public static final String INTENT_PHONENUMBER = "phonenumber";
    public static final String INTENT_COUNTRY_CODE = "code";
    WS_CallService service_Login;
    List<String> schoolid;
    List<String> school;
    String school_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.number_login_fragment, parentViewGroup, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Permission.verifySmsRead(getActivity());
        selectschool = (Spinner) view.findViewById(R.id.select_school);
        phonenumber = (EditText) view.findViewById(R.id.mobile_number);

        Log.i("TAG", "user: " + Build.USER);
        Log.i("TAG", "BASE: " + Build.VERSION_CODES.BASE);
        Log.i("TAG", "INCREMENTAL " + Build.VERSION.INCREMENTAL);
        Log.i("TAG", "SDK  " + Build.VERSION.SDK);
        Log.i("TAG", "MODEL: " + Build.MODEL);
        Log.i("TAG", "ID: " + Build.ID);
        Log.i("TAG", "type: " + Build.TYPE);

        try {
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();

            byte[] data;
            String login_str =
                    "Function:SchoolLists|DeviceType:'android'|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));

            Load_School_WS load_plan_list = new Load_School_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        next = (Button) view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    LoginActivity baseActivity = (LoginActivity) getActivity();
                    OtpVerifyFragment codeVerificationFragment = new OtpVerifyFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.NUMBER, phonenumber.getText().toString());
                    bundle.putString(Constants.SCHOOLID, school_id);
                    //  bundle.putString(Constants.INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
                    codeVerificationFragment.setArguments(bundle);
                    baseActivity.replaceFragment(codeVerificationFragment,1);
                }
            }
        });

    }

    public boolean validate() {
        boolean validate = false;
        if (phonenumber.getText().toString().length() == 0 || phonenumber.getText().toString().length()< 10) {
            phonenumber.setError("Please Enter Valid Phone Number");
            validate = false;
        } else if (school_id.equals("0")) {
            Toast.makeText(getActivity(), "Please Select School", Toast.LENGTH_SHORT).show();
            validate = false;
        } else {
            validate = true;
        }
        return validate;
    }

    public class Load_School_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_School_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            school = new ArrayList<>();
            schoolid = new ArrayList<>();
            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    school.add("Select School");
                    schoolid.add("0");
                    JSONArray jsonArray = jObj.getJSONArray("Schools");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        schoolid.add(jsonObject.getString("Id"));
                        school.add(jsonObject.getString("Name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.spinner_row,
                            school
                    );
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    selectschool.setAdapter(adapter);

                    selectschool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                            school_id = schoolid.get(position);
                            //  Toast.makeText(getContext(), school_id, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {

                        }
                    });

                  /*  AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY,user_wallet_amount);
                    System.out.println("Login response-->" + Message);

                    userID = jObj.getJSONArray("Response").getJSONObject(0).getString("UserID");
                    UserName = jObj.getJSONArray("Response").getJSONObject(0).getString("UserName");
                    Email = jObj.getJSONArray("Response").getJSONObject(0).getString("Email");
                    Mobile = jObj.getJSONArray("Response").getJSONObject(0).getString("Mobile");

                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, userID);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, UserName);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, Email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, Mobile);
                    AppPreferences.putBooleanIntoStore(Preference_Constants.PREY_KEY_USER_HOWIT_FLAG, true);


                    Intent LoginIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(LoginIntent);
                    finish();*/

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

}
