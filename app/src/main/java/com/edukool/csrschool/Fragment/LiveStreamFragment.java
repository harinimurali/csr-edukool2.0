package com.edukool.csrschool.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;

/**
 * Created by harini on 9/17/2018.
 */

public class LiveStreamFragment extends Fragment {
    String schoolId;

    public LiveStreamFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);
        schoolId = AppPreferences.getSchool_Name(getActivity());
        if (Permission.checknetwork(getActivity())) {
            final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
            progDailog.setCancelable(false);
//terms_of_service_webview.getSettings().setJavaScriptEnabled(true);
            WebView webView = (WebView) view.findViewById(R.id.webview);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setSupportMultipleWindows(true);
            webView.setWebChromeClient(new WebChromeClient());
            webView.setHorizontalScrollBarEnabled(false);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progDailog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progDailog.dismiss();
                }
            });
            // webView.loadUrl("http://demo.edukool.com/edukool/web/index.php?r=admin/news/getlivestreaming");
            if (schoolId.equals("SC-001-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.LiveStream_english_url);
                } else {
                    webView.loadUrl(Constants.LiveStream_tamil_url);
                }
            } else if (schoolId.equals("SC-002-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.LiveStream_english_url_prm);
                } else {
                    webView.loadUrl(Constants.LiveStream_tamil_url_prm);
                }
            }
            /*if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                webView.loadUrl(Constants.LiveStream_english_url);
            } else {
                webView.loadUrl(Constants.LiveStream_tamil_url);
            }*/
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.livestream));
    }
}
