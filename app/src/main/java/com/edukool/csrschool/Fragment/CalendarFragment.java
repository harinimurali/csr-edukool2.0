package com.edukool.csrschool.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.CalendarAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.edukool.csrschool.models.EventResultItem;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by harini on 9/17/2018.
 */

public class CalendarFragment extends Fragment {

    List<ChildrenProfile> childrenProfiles;
    List<EventResultItem> eventResultItems;
    List<EventResultItem> eventResultItemsList;
    String studentId;
    SimpleDateFormat simpleDateFormat, simpleDateFormat1;
    CompactCalendarView compactCalendarView;
    Date currentDate, startDate, endDate;
    private Date selDate;
    private Boolean isVisible = false;
    private Boolean isStarted = false;
    private boolean isFromCalender = false;
    String ParentNumber, schoolid, versioncode;
    List<EventResultItem> eventsResultsItemList;
    ProgressDialog progDailog;
    WS_CallService service_Login;
    CalendarAdapter mAdapter;
    RecyclerView recycler_event;
    JSONArray jsonArray;
    TextView textEventDate;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM yyyy");
    TextView monthText;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;

    public CalendarFragment() {

    }

    /* @Override
     public void onStart() {
         super.onStart();
         if (selDate != null)
             currentDate = selDate;
         else
             currentDate = Calendar.getInstance().getTime();

         simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
         //  textEventDate.setText("Events on: " + simpleDateFormat.format(currentDate));
         compactCalendarView.setCurrentDate(currentDate);
         isStarted = true;
         if (isVisible) {
             isFromCalender = false;
 //            showProgress();
             getEventListInCalendar();
             callAPI(String.valueOf(currentDate));
         }

     }
 */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());

        progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
        progDailog.setCancelable(false);
        compactCalendarView = view.findViewById(R.id.compact_calendar_view);
        recycler_event = view.findViewById(R.id.recycler_event);
        textEventDate = view.findViewById(R.id.textEventDate);
        monthText = view.findViewById(R.id.caltext);

        stuname = view.findViewById(R.id.student_name);
        stuclass = view.findViewById(R.id.student_class);
        stuimage = view.findViewById(R.id.student_image);
        drpDown = view.findViewById(R.id.drpDown);

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses()+ "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }
        //////////////////////////////
        if (selDate != null)
            currentDate = selDate;
        else
            currentDate = Calendar.getInstance().getTime();

        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);
        simpleDateFormat1 = new SimpleDateFormat("MMM yyyy",Locale.ENGLISH);
        textEventDate.setText("Events on: " + simpleDateFormat.format(currentDate));
        monthText.setText(simpleDateFormat1.format(currentDate));
        compactCalendarView.setCurrentDate(currentDate);
        isStarted = true;

        isFromCalender = false;
//            showProgress();
        if (Permission.checknetwork(getActivity())) {
            getEventListInCalendar();
        }
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });

        ////////////////////////
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {

            @Override
            public void onDayClick(Date dateClicked) {
                isFromCalender = true;
                // eventsResultsItemList.clear();
                textEventDate.setText(simpleDateFormat.format(dateClicked));

                if (Permission.checknetwork(getActivity())) {
                    getRecycleList(getdateConvert(String.valueOf(dateClicked)));
                }
                selDate = dateClicked;
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                monthText.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });

        return view;
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses()+ "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                if (Permission.checknetwork(getActivity())) {
                    getEventListInCalendar();
                }
                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public String getdateConvert(String currentda) {
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = (Date) formatter.parse(currentda);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        String formatedDate = outputFormat.format(date);
        System.out.println("formatedDate : " + formatedDate);
        return formatedDate;
    }


/*
    public void getEventList(final Date currentDate) {
        // textEventDate.setText("Events on : " + simpleDateFormat.format(currentDate));
        if (Permission.checknetwork(getActivity())) {
            if (isFromCalender)
                showProgress();
            // eventsResultsItemList.clear();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            timeLineDelete.setLimit(30);
            timeLineDelete.setOffset(currentPage);
            isLoading = true;
            if (currentPage == 0) {
                eventsResultsItemList.clear();
            }
            dmkAPI.getEventList(timeLineDelete).enqueue(new Callback<EventResponse>() {
                @Override
                public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                    isLoading = false;
                    hideProgress();
                    eventResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (eventResponse.getResults().size() > 0) {
                            if (isAdded()) {
                                imageNoEvents.setVisibility(View.GONE);
                                textNoEvents.setVisibility(View.GONE);
                                recycler_event.setVisibility(View.VISIBLE);
                            }

                            for (int i = 0; i < eventResponse.getResults().size(); i++) {
                                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                                try {
                                    startDate = format1.parse(eventResponse.getResults().get(i).getStartDate() != null ?
                                            eventResponse.getResults().get(i).getStartDate() : "");
                                    endDate = format1.parse(eventResponse.getResults().get(i).getEndDate() != null ?
                                            eventResponse.getResults().get(i).getEndDate() : "");
                                    eventStartDate = simpleDateFormat.format(startDate);
                                    eventEndDate = simpleDateFormat.format(endDate);
                                    selectedDate = simpleDateFormat.format(currentDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (selectedDate.equalsIgnoreCase(eventStartDate) || selectedDate.equalsIgnoreCase(eventEndDate)
                                        || (currentDate.before(endDate) && currentDate.after(startDate))) {
                                    eventsResultsItemList.add(new EventsResultsItem(
                                            eventResponse.getResults().get(i).getId(),
                                            eventResponse.getResults().get(i).getName() != null ?
                                                    eventResponse.getResults().get(i).getName() : "",
                                            eventResponse.getResults().get(i).getDescription() != null ?
                                                    eventResponse.getResults().get(i).getDescription() : "",
                                            eventResponse.getResults().get(i).getMediafiles(),
                                            eventResponse.getResults().get(i).getTitleimage() != null ?
                                                    eventResponse.getResults().get(i).getTitleimage() : "",
                                            eventResponse.getResults().get(i).getStartDate() != null ?
                                                    getDateFormat(eventResponse.getResults().get(i).getStartDate()) : "",
                                            eventResponse.getResults().get(i).getEndDate() != null ?
                                                    getDateFormat(eventResponse.getResults().get(i).getEndDate()) : "",
                                            eventResponse.getResults().get(i).getCreatedBy(), eventResponse.getResults().get(i).getDistrictID()));


                                }
//
                            }
                            if (eventsResultsItemList.size() > 0) {
                                //Collections.reverse(eventsResultsItemList);
                                isLoading = false;
                                eventListAdapter.notifyDataSetChanged();
                            } else {
                                if (eventsResultsItemList.size() == 0) {
                                    if (isAdded()) {
                                        imageNoEvents.setVisibility(View.VISIBLE);
                                        textNoEvents.setVisibility(View.VISIBLE);
                                        textNoEvents.setText(R.string.no_events);
                                        imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                                        recycler_event.setVisibility(View.GONE);
                                    }
                                } else {

                                    lastEnd = true;

                                }
                            }

                        } else {
//                            eventResponse.getResults().size() == 0
                            if (eventsResultsItemList.size() == 0) {
                                if (isAdded()) {
                                    imageNoEvents.setVisibility(View.VISIBLE);
                                    textNoEvents.setVisibility(View.VISIBLE);
                                    recycler_event.setVisibility(View.GONE);
                                    textNoEvents.setText(R.string.no_events);
                                    imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                                }
                            } else {

                                lastEnd = true;

                            }
                        }
                    } else {
                        // response.body()!=200
                        if (eventsResultsItemList.size() == 0) {
                            if (isAdded()) {
                                imageNoEvents.setVisibility(View.VISIBLE);
                                textNoEvents.setVisibility(View.VISIBLE);
                                recycler_event.setVisibility(View.GONE);
                                textNoEvents.setText(R.string.no_events);
                                imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                            }
                        } else {

                            lastEnd = true;

                        }
                    }

                }

                @Override
                public void onFailure(Call<EventResponse> call, Throwable t) {
                    hideProgress();
                    if (eventsResultsItemList.size() == 0) {
                        if (isAdded()) {
                            imageNoEvents.setVisibility(View.VISIBLE);
                            textNoEvents.setVisibility(View.VISIBLE);
                            recycler_event.setVisibility(View.GONE);
                            textNoEvents.setText(R.string.no_events);
                            imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                        }

                    }
                }
            });
        } else {
            hideProgress();
            if (isAdded()) {
                imageNoEvents.setVisibility(View.VISIBLE);
                textNoEvents.setVisibility(View.VISIBLE);
                recycler_event.setVisibility(View.GONE);
                imageNoEvents.setImageResource(R.mipmap.icon_no_network);
                textNoEvents.setText(R.string.no_network);
            }
        }

    }
*/

    public void getEventListInCalendar() {
        if (Permission.checknetwork(getActivity())) {

            try {
                progDailog.show();
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                String current = getdateConvert(String.valueOf(currentDate));
                byte[] data;
                String fcmtoken = AppPreferences.getFcmToken(getActivity());
                String login_str = "UserName:" + schoolid + "|studentId:" + studentId + "|date:" + current + "|Parent_ID:" + ParentNumber + "|Function:EventsCalender|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
                //  String login_str = "UserName:'" + schoolid + "'|parentId:'9994922081'|password:par|Function:ParentLogin|DeviceType:android|GCMKey:'"+fcmtoken+"'|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Events_WS load_plan_list = new Load_Events_WS(getContext(), login);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public class Load_Events_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Events_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            //  progressBar.setVisibility(View.GONE);
            eventResultItemsList = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONArray array = jObj.getJSONArray("Events");

                    if (array.length() != 0)
                        jsonArray = array;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);

                        List<Date> betweenDates = getDates(
                                jsonObject.getString("EventDate") != null ?
                                        jsonObject.getString("EventDate") : "");
                        List<Event> eventList = new ArrayList<Event>();
                        eventList.clear();
                        for (int j = 0; j < betweenDates.size(); j++) {
                            eventList.add(new Event(
                                    getActivity().getResources().getColor(R.color.clr1), betweenDates.get(j).getTime(), "Events"));
                        }
                        if (isAdded()) {
                            compactCalendarView.addEvents(eventList);
                        }
                           /* EventResultItem event = new EventResultItem();
                            event.setID(jsonObject.getString("ID"));
                            event.setTitleDescription(jsonObject.getString("TitleDescription"));
                            event.setModuleType(jsonObject.getString("ModuleType"));
                            event.setEventDate(jsonObject.getString("EventDate"));
                            event.setCreatedDate(jsonObject.getString("CreatedDate"));
                            JSONArray array1 = jsonObject.getJSONArray("Attachment");
                            List<String> attachment = new ArrayList<>();
                            if (array1.length() != 0) {
                                for (int j = 0; j < array1.length(); j++) {
                                    attachment.add(array1.getString(j));
                                    event.setAttachment(attachment);
                                }
                            }
                            eventResultItems.add(event);*/
                    }
                    progDailog.dismiss();
                    getRecycleList(getdateConvert(String.valueOf(currentDate)));
                    progDailog.dismiss();
                    System.out.println("success");
                } else {
                    progDailog.dismiss();
                    System.out.println("failed");
                   // String Message = jObj.getString("response");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);
                    Toast.makeText(getActivity(), "No Events", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "no events cache");
            }
        }

    }

    private void getRecycleList(String date) {
        Log.e("date", "date" + date);
        try {
            eventResultItems = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.getString("EventDate").equals(date)) {
                    EventResultItem event = new EventResultItem();
                    event.setID(jsonObject.getString("ID"));
                    event.setTitleDescription(jsonObject.getString("TitleDescription"));
                    event.setModuleType(jsonObject.getString("ModuleType"));
                    event.setEventDate(jsonObject.getString("EventDate"));
                    event.setCreatedDate(jsonObject.getString("CreatedDate"));
                    JSONArray array1 = jsonObject.getJSONArray("Attachment");
                    List<String> attachment = new ArrayList<>();
                    if (array1.length() != 0) {
                        for (int j = 0; j < array1.length(); j++) {
                            attachment.add(array1.getString(j));
                            event.setAttachment(attachment);
                        }
                    }
                    eventResultItems.add(event);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAdapter = new CalendarAdapter(eventResultItems, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_event.setLayoutManager(mLayoutManager);
        recycler_event.setItemAnimator(new DefaultItemAnimator());
        recycler_event.setAdapter(mAdapter);
    }

    private static List<Date> getDates(String dateString1) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        // Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            // date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        dates.add(cal1.getTime());


           /* Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);*/

        /*while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }*/
        return dates;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.calendar));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).error(R.drawable.student_default).into(stuimage);
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
    }
}
