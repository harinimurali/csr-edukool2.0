package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;


import com.edukool.csrschool.Activity.AssignmentDetailActivity;
import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.AssignmentRecycleAdapter;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.Assignment;
import com.edukool.csrschool.models.ChildrenProfile;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Assignments extends Fragment {


    private RecyclerView recyclerView;
    private View view;

    private List<Assignment> assignmentLists;
    private AssignmentRecycleAdapter mAdapter;
    WS_CallService service_Login;
    List<ChildrenProfile> childrenProfiles;
    String studentid;
    ProgressBar progressBar;
    String ParentNumber, versioncode;
    String schoolid, noti;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    FontTextViewMedium norecords;


    public Assignments() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_assignments, container, false);

        String[] values =
                {"GOWTHAMI", "JANANI", "PREETHI", "KUMARAN", "PRIYA", "DHEENADAYALAN", "PRAVEENA", "ASHWINI",};
        TextView textview = (TextView) view.findViewById(R.id.text);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        progressBar = (ProgressBar) view.findViewById(R.id.progressIndicator);
        norecords = (FontTextViewMedium) view.findViewById(R.id.norecords);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleassign);
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());

        stuname = view.findViewById(R.id.student_name);
        stuclass = view.findViewById(R.id.student_class);
        stuimage = view.findViewById(R.id.student_image);
        drpDown = view.findViewById(R.id.drpDown);
        try {
            Bundle b = getArguments();
            noti = b.getString("noti");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        if (Permission.checknetwork(getActivity())) {
            callAPI();
        }
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),
                R.layout.spinner_layout, childrenProfiles);


       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (noti.equals("noti")) {

            } else {
                ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.assignments));

                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.assignments));
            stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
            stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
            Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
            studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
            if (Permission.checknetwork(getActivity())) {
                callAPI();
            }
        }
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAPI() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            //  String schoolid = "SC-001-LS";
            // studentid = "1";
            byte[] data;
            //String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:"+studentid+"|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:StudentAssignments|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Assignments_WS load_plan_list = new Load_Assignments_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class Load_Assignments_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Assignments_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            assignmentLists = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                   /* if (jObj.getString("StatusCode").equals("201")) {
                        norecords.setVisibility(View.VISIBLE);
                    } else {*/
                    norecords.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    JSONObject object = jObj.getJSONObject("Assignments");

                    JSONArray array = object.getJSONArray("Previous");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        Assignment recent = new Assignment();
                        recent.setID(jsonObject.getString("ID"));
                        recent.setName(jsonObject.getString("Name"));
                        recent.setSubject(jsonObject.getString("Subject").trim());
                        recent.setDescription(jsonObject.getString("Description").trim());
                        recent.setStartDate(jsonObject.getString("StartDate"));
                        recent.setEndDate(jsonObject.getString("EndDate"));
                        recent.setSubmissionType(jsonObject.getString("SubmissionType"));
                        assignmentLists.add(recent);
                        //    recent.setClass(jsonObject.getString("Class"));
                        // recent.setClass(jsonObject.getString("Date"));
                        //   Recentlist.add(recent);
                    }

                    mAdapter = new AssignmentRecycleAdapter(assignmentLists);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.setOnClickListen(new AddTouchListen() {
                        @Override
                        public void onTouchClick(int position) {
                            Bundle b = new Bundle();
                            b.putString("title", assignmentLists.get(position).getName());
                            b.putString("subject", assignmentLists.get(position).getSubject());
                            b.putString("startdate", assignmentLists.get(position).getStartDate());
                            b.putString("enddate", assignmentLists.get(position).getEndDate());
                            b.putString("description", assignmentLists.get(position).getDescription());
                            b.putString("submission", assignmentLists.get(position).getSubmissionType());
                            //  b.putSerializable("data", assignmentLists.get(position));
                            startActivity(new Intent(getActivity(), AssignmentDetailActivity.class).putExtra("assignment", b));
                        }
                    });

                    //   }
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    //   Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


  /*  private void prepareMovieData() {


        RecentActivititesModels movie = new RecentActivititesModels("English", "assignments_icon on Grammer class ","");
        assignmentLists.add(movie);

        movie = new RecentActivititesModels("Tamil", "assignments_icon on Lessons","");
        assignmentLists.add(movie);

    }*/

}
