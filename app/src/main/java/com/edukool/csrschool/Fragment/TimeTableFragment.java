package com.edukool.csrschool.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;
import com.edukool.csrschool.models.ChildrenProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/17/2018.
 */

public class TimeTableFragment extends Fragment {

    List<ChildrenProfile> childrenProfiles;
    String studentId, schoolId;

    public TimeTableFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getEncryptedStudID();
        schoolId = AppPreferences.getSchool_Name(getActivity());
        if (Permission.checknetwork(getActivity())) {
            final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
            progDailog.setCancelable(false);
//terms_of_service_webview.getSettings().setJavaScriptEnabled(true);
            WebView webView = (WebView) view.findViewById(R.id.webview);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progDailog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progDailog.dismiss();
                }
            });
            if (schoolId.equals("SC-001-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.Timetable_english_url + studentId);
                } else {
                    webView.loadUrl(Constants.Timetable_tamil_url + studentId);
                }
            } else if (schoolId.equals("SC-002-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.Timetable_english_url_prm + studentId);
                } else {
                    webView.loadUrl(Constants.Timetable_tamil_url_prm + studentId);

                }
            }
            /*if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                webView.loadUrl(Constants.Timetable_english_url+ studentId);
            }else{
                webView.loadUrl(Constants.Timetable_tamil_url+studentId);
            }*/
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.timetable));
    }
}
