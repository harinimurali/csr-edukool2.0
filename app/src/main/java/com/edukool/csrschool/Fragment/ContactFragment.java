package com.edukool.csrschool.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;
import com.edukool.csrschool.models.ChildrenProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/17/2018.
 */

public class ContactFragment extends Fragment {
    List<ChildrenProfile> childrenProfiles;
    String studentId, schoolId;
    WebView webView;


    public ContactFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getEncryptedStudID();
        schoolId = AppPreferences.getSchool_Name(getActivity());
        if (Permission.checknetwork(getActivity())) {
            final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
            progDailog.setCancelable(false);
//terms_of_service_webview.getSettings().setJavaScriptEnabled(true);
            webView = (WebView) view.findViewById(R.id.webview);
            webView.getSettings().setUseWideViewPort(true);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progDailog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progDailog.dismiss();
                }
            });
            webView.getSettings().setSupportMultipleWindows(true);
            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    //Required functionality here
                    return super.onJsAlert(view, url, message, result);
                }
            });
            webView.setWebChromeClient(new WebChromeClient() {


                @Override
                public boolean onCreateWindow(WebView view, boolean isDialog,
                                              boolean isUserGesture, Message resultMsg) {


                    WebView newWebView = new WebView(getActivity());
                    newWebView.getSettings().setJavaScriptEnabled(true);
                    newWebView.getSettings().setSupportZoom(true);
                    newWebView.getSettings().setBuiltInZoomControls(true);
                    newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
                    newWebView.getSettings().setSupportMultipleWindows(true);
                    view.addView(newWebView);
                    WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                    transport.setWebView(newWebView);
                    resultMsg.sendToTarget();

                    newWebView.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });

                    return true;
                }

            });
            webView.setOnKeyListener(new View.OnKeyListener() {

                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    handler.sendEmptyMessage(1);
                   /* if (keyCode == KeyEvent.KEYCODE_BACK
                            && event.getAction() == MotionEvent.ACTION_UP
                            && webView.canGoBack()) {
                        handler.sendEmptyMessage(1);
                        return true;
                    }*/

                    return false;
                }

            });

            if (schoolId.equals("SC-001-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.Contact_english_url + studentId);
                } else {
                    webView.loadUrl(Constants.Contact_tamil_url + studentId);
                }
            } else if (schoolId.equals("SC-002-LS")) {
                if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                    webView.loadUrl(Constants.Contact_english_url_prm + studentId);
                } else {
                    webView.loadUrl(Constants.Contact_tamil_url_prm + studentId);
                }
            }
           /* if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                if (schoolId.equals("SC-001-LS")) {
                    webView.loadUrl(Constants.Contact_english_url + studentId);
                } else {
                    webView.loadUrl(Constants.Contact_english_url_prm + studentId);
                }
            } else {
                if (schoolId.equals("SC-001-LS")) {
                    webView.loadUrl(Constants.Contact_tamil_url + studentId);
                } else {
                    webView.loadUrl(Constants.Contact_tamil_url_prm + studentId);
                }
            }*/
            Log.e("language", ">>" + LanguageHelper.getLanguage(getActivity()));
        }
        return view;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1: {
                    webViewGoBack();
                }
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.contact));
    }

    private void webViewGoBack() {
        webView.goBack();
    }
   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        getActivity().finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }*/
   /*@Override
   public void onBackPressed() {
       if (webView.canGoBack()) {
           webView.goBack();
       } else {
           super.onBackPressed();
       }
   }*/
}
