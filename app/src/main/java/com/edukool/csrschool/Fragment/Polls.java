package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.PollsAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewMedium;
import com.edukool.csrschool.models.PollsList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Polls extends Fragment {

    WS_CallService service_Login;
    private RecyclerView recyclerView;
    private PollsAdapter mAdapter;
    private List<PollsList> polllist;
    ProgressBar progressbar;
    String ParentNumber, schoolid, versioncode, noti;
    private int listposition;
    FontTextViewMedium norecords;
    RadioGroup radioGroup;
    RelativeLayout commentlayout;
    EditText edit_comment;



    public Polls() {
        //required

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_polls, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.pollsrecycle);
        progressbar = (ProgressBar) view.findViewById(R.id.progressIndicator);

        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());
        // recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        norecords = (FontTextViewMedium) view.findViewById(R.id.norecords);
        try {
            Bundle b = getArguments();
            noti = b.getString("noti");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CallAPI();

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (noti.equals("noti")) {

            } else {
                ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.polls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void APIintegration(String eventid, String like, String comment) {

        try {
            {
                progressbar.setVisibility(View.GONE);
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                //  String schoolid = "SC-001-LS";


                byte[] data;
                String login_str1 = "UserName:" + schoolid + "|Comments:" + comment + "|Like:" + like + "|Parent_ID:" + ParentNumber + "|Event_ID:" + eventid +
                        "|Function:EventPolls|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";


                data = login_str1.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                //  base64_register="VXNlck5hbWU6U0MtMDAxLUxTfFBhcmVudF9JRDo5OTUyNDgyNjQxfEZ1bmN0aW9uOkV2ZW50c3xEZXZpY2VUeXBlOmFuZHJvaWR8R0NNS2V5OicnfERldmljZUlEOicnfEFwcElEOjEuMHxJTUVJTnVtYmVyOicnfEFwcFZlcnNpb246dGV8TUFDQWRkcmVzczonJ3xPU1ZlcnNpb246MTEuMg0K";
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Polls_WS1 load_plan_list = new Load_Polls_WS1(getContext(), login);
                load_plan_list.execute();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void CallAPI() {
        if (Permission.checknetwork(getActivity())) {
            try {
                {
                    progressbar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                    //  String schoolid = "SC-001-LS";

                    byte[] data;
                    String login_str = "UserName:" + schoolid + "|Parent_ID:" + ParentNumber + "|Function:Events|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    Log.e("basecode", "" + base64_register);
                    //  base64_register="VXNlck5hbWU6U0MtMDAxLUxTfFBhcmVudF9JRDo5OTUyNDgyNjQxfEZ1bmN0aW9uOkV2ZW50c3xEZXZpY2VUeXBlOmFuZHJvaWR8R0NNS2V5OicnfERldmljZUlEOicnfEFwcElEOjEuMHxJTUVJTnVtYmVyOicnfEFwcFZlcnNpb246dGV8TUFDQWRkcmVzczonJ3xPU1ZlcnNpb246MTEuMg0K";
                    login.add(new BasicNameValuePair("WS", base64_register));
                    Load_Polls_WS load_plan_list = new Load_Polls_WS(getContext(), login);
                    load_plan_list.execute();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public class Load_Polls_WS1 extends AsyncTask<String, String, String> {
        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Polls_WS1(Context context_ws, ArrayList<NameValuePair> loginws) {
            this.loginact = loginws;
            this.context_aact = context_ws;


        }

        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressbar.setVisibility(View.GONE);


            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    //  Toast.makeText(getActivity(), jObj.getString("response"), Toast.LENGTH_SHORT).show();
                   /* polllist.remove(listposition);
                    //  radioGroup.setSelected(false);
                    radioGroup.clearCheck();
                    commentlayout.setVisibility(View.GONE);
                    edit_comment.setText("");

                    mAdapter.notifyDataSetChanged();

                    if (polllist.size() == 0) {
                        norecords.setVisibility(View.VISIBLE);
                    } else {
                        norecords.setVisibility(View.GONE);
                    }*/
                    System.out.println("success");
                    showDialog(jObj.getString("response"));
                }
            } catch (JSONException e2) {
                System.out.println(e2.toString() + "zcx");
            }
        }
    }

    public void showDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Thanks for your feedback");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                polllist.remove(listposition);
                //  radioGroup.setSelected(false);
                radioGroup.clearCheck();
                commentlayout.setVisibility(View.GONE);
                edit_comment.setText("");

                mAdapter.notifyDataSetChanged();

                // startActivity(new Intent(getActivity(), LeaveRequestListActivity.class));
                if (polllist.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    norecords.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    norecords.setVisibility(View.GONE);
                }
                dialog.dismiss();


            }
        });
        // Create the AlertDialog object and return it

        AlertDialog dialog = builder.create();
        dialog.show();

    }


//Toast.makeText(getActivity(),jObj.getString("response"),Toast.LENGTH_SHORT).show();


    public class Load_Polls_WS extends AsyncTask<String, String, String> {


        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;


        public Load_Polls_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            this.loginact = loginws;
            this.context_aact = context_ws;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressbar.setVisibility(View.GONE);
            polllist = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    norecords.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    JSONArray array = jObj.getJSONArray("Videos");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        PollsList recent = new PollsList();
                        recent.setID(jsonObject.getString("ID"));
                        recent.setName(jsonObject.getString("Name").trim());
                        recent.setDescription(jsonObject.getString("Description").trim());
                        recent.setStart_date(jsonObject.getString("EventStartDate"));
                        recent.setEnd_date(jsonObject.getString("EventEndDate"));
                        polllist.add(recent);
                    }
                    mAdapter = new PollsAdapter(getActivity(), polllist);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);

                    mAdapter.setOnClickListen(new PollsAdapter.AddTouchListen() {

                        @Override
                        public void onTouchClick(int position, String like, String comment, RadioGroup rg, RelativeLayout layout, EditText editText) {
                            String eventid = polllist.get(position).getID();

                            APIintegration(eventid, like, comment);

                            listposition = position;
                            radioGroup = rg;
                            commentlayout = layout;
                            edit_comment = editText;
                        }
                    });
                    System.out.println("success");

                } else {
                    Log.e("share", "failed");
                    System.out.println("failed");
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    // norecords.setText(Message);


                    //  Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e1) {
                System.out.println(e1.toString() + "zcx");
            }
        }

    }
}