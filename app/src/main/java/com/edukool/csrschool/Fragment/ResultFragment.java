package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.ResultAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.edukool.csrschool.models.ExamsModel;
import com.edukool.csrschool.models.ResultModel;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment {

    List<ChildrenProfile> childrenProfiles;
    List<ExamsModel> exams;
    String studentid;
    ProgressBar progressBar;
    Spinner examsSpinner;
    WS_CallService service_Login;
    List<ResultModel> resultModelList = new ArrayList<>();
    RecyclerView recycle_class;
    String tag = "result";
    TextView norecord;
    LinearLayout result_lay;
    String ParentNumber, schoolid, versioncode;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    RelativeLayout exam_spinner_layout;
    LinearLayout gradelayout;


    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_result, container, false);

        String[] values =
                {"GOWTHAMI", "JANANI", "PREETHI", "KUMARAN", "PRIYA", "DHEENADAYALAN", "PRAVEENA", "ASHWINI",};
        TextView textview = (TextView) view.findViewById(R.id.text);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner1);
        examsSpinner = (Spinner) view.findViewById(R.id.examsSpinner);
        recycle_class = view.findViewById(R.id.recycle_class);
        norecord = view.findViewById(R.id.norecord);
        result_lay = view.findViewById(R.id.result_lay);
        gradelayout = view.findViewById(R.id.gradelayout);
        exam_spinner_layout = view.findViewById(R.id.exam_spinner_layout);
        exams = new ArrayList<>();
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        progressBar = (ProgressBar) view.findViewById(R.id.progressIndicator);
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());


        stuname = view.findViewById(R.id.student_name);
        stuclass = view.findViewById(R.id.student_class);
        stuimage = view.findViewById(R.id.student_image);
        drpDown = view.findViewById(R.id.drpDown);


        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses()+ "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        if (Permission.checknetwork(getActivity())) {
            callAPI(studentid);
        }


        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),
                R.layout.spinner_layout, childrenProfiles);

       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI(studentid);
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        examsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String examname = exams.get(i).getName();
                if (Permission.checknetwork(getActivity())) {
                    getResultAPI(examname);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


//        CustomSpinnerAdapter children = new CustomSpinnerAdapter(getActivity(),
//                R.layout.spinner_layout, childrenProfiles);
//
//
//       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
//        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
//        examsSpinner.setAdapter(children);

//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
//                studentid = childrenProfiles.get(pos).getID();
//            }
//
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

      /*  adapter.setOnClickListen(new CustomSpinnerAdapter.AddTouchListen() {

            @Override
            public void onSelectClick(int position) {
                Log.e("id", "" + childrenProfiles.get(position).getID());
            }
        });
*/
//        recyclerView = (RecyclerView) view.findViewById(R.id.recycle);
//        but = (Button) view.findViewById(R.id.button);
//
//        callAPI();

        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.results));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses()+ "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        callAPI(studentid);
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses()+ "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                callAPI(studentid);
                dialog.dismiss();
            }
        });

/*Grade for Marks:
| 100-91  | A1
| 90-81  | A2
| 80-71  | B1
| 70-61  | B2
| 60-51  | C1
| 50-41  | C2
| 40-31  | D
| 30-21  | E1
| 20-0  | E2*/

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAPI(String studentid) {
        if (Permission.checknetwork(getActivity())) {
            try {

                Log.d(tag, "callAPI: " + studentid);
                //  progressBar=new ProgressBar(getActivity());
                progressBar.setVisibility(View.VISIBLE);
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                // String schoolid = "SC-001-LS";
                byte[] data;
//            UserName:SC-001-LS|parentId:7402516995|studentId:1|Function:StudentReports|Device Type:android
//                    | GCMKey:''|DeviceID:''|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:11.2
                //  String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:" + studentid + "|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:StudentExamDesc|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Vidoes_WS load_plan_list = new Load_Vidoes_WS(getActivity(), login, 0);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getResultAPI(String exam) {
        try {
            Log.d(tag, "getResultAPI: " + exam);
            //  progressBar=new ProgressBar(getActivity());
            progressBar.setVisibility(View.VISIBLE);
            result_lay.setVisibility(View.GONE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            // String schoolid = "SC-001-LS";
            byte[] data;
//            UserName:SC-001-LS|parentId:7402516995|studentId:1|Function:StudentReports|Device Type:android
//                    | GCMKey:''|DeviceID:''|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:11.2
//
//            UserName:SC-001-LS|studentId:1|examDesc:''|Function:StudentBasedMarks|DeviceType:android
//                    |GCMKey:''|DeviceID:''|AppID:1.0|IMEINumber:''|AppVersion:te
//                    |MACAddress:''|OSVersion:11.2
            //  String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:" + studentid + "|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|" +
                    "studentId:" + studentid + "|examDesc:" + exam + "|Function:StudentBasedMarks|DeviceType:android|GCMKey:''" +
                    "|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Vidoes_WS load_plan_list = new Load_Vidoes_WS(getActivity(), login, 1);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class Load_Vidoes_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;
        int type;

        public Load_Vidoes_WS(Context context_ws, ArrayList<NameValuePair> loginws, int type) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;
            this.type = type;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            //  youtubeVideosList = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");

                if (status.equalsIgnoreCase("Success")) {
                    if (type == 0) {
                        parseExams(jObj);
                    } else if (type == 1) {
                        parseResult(jObj);

                    }


                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);
//
                    //   Toast.makeText(getApplication(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    public void parseExams(JSONObject json) {
        exams.clear();
        try {
            JSONArray array = json.getJSONArray("ExamDescription");
            if (array.length() == 0) {

                exam_spinner_layout.setVisibility(View.GONE);
                norecord.setVisibility(View.VISIBLE);
            } else {
                exam_spinner_layout.setVisibility(View.VISIBLE);
                norecord.setVisibility(View.GONE);
                for (int i = 0; i < array.length(); i++) {

                    JSONObject jsonObject = array.getJSONObject(i);

                    ExamsModel model = new ExamsModel();
                    //  model.setCode(jsonObject.getString("Code"));
                    //  model.setId(jsonObject.getString("ID"));
                    model.setName(jsonObject.getString("ExamDescription"));
                    //  model.setToatal(String.valueOf(jsonObject.getInt("TotalMark")));
                    //  model.setDate(jsonObject.getString("Date"));
                    exams.add(model);
//                        YoutubeVideos recent = new YoutubeVideos();
//                        recent.setId(jsonObject.getString("ID"));
//                        String video = jsonObject.getString("URL");
//                        recent.setVideoUrl(video);
//
//                        // recent.setVideoUrl("<iframe width=\"100%\" height=\"100%\" src= \""+video+"\" frameborder=\"0\" allowfullscreen></iframe>");
//
//                        //   recent.setVideoUrl("<iframe width=100%height=100%src="+jsonObject.getString("URL")+" frameborder=\"0\" allowfullscreen></iframe>");
//                        recent.setVideotitle(jsonObject.getString("Title"));
//                        recent.setImage(jsonObject.getString("Image"));
//                        recent.setDate(jsonObject.getString("Date"));
                    //  youtubeVideosList.add(recent);
                }
            }

            Log.d("data", "onPostExecute: " + exams.size());

            // adapter;
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_layout, R.id.spinner_text, exams);
            examsSpinner.setAdapter(adapter);

            //  videoAdapter = new VideoAdapter(VideoActivity.this, youtubeVideosList);
//                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VideoActivity.this);
//                    recyclerView.setLayoutManager(mLayoutManager);
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setAdapter(videoAdapter);
            System.out.println("success");
        } catch (JSONException ex) {

        }


    }

    public void parseResult(JSONObject jsonObj) {
        resultModelList.clear();
        try {
            JSONArray array = jsonObj.getJSONArray("MarksDetails");
            for (int i = 0; i < array.length(); i++) {

                JSONObject jsonObject = array.getJSONObject(i);

                ResultModel model = new ResultModel();
                //  model.setCode(jsonObject.getString("Code"));
                //  model.setId(jsonObject.getString("ID"));
                model.setSubject(jsonObject.getString("SubjectName"));
                model.setClassname(jsonObject.getString("ClassName"));
                model.setSection(jsonObject.getString("Section"));
                model.setGrade(jsonObject.getString("Marks"));
                model.setDate(jsonObject.getString("ExamDate"));
                //  model.setToatal(String.valueOf(jsonObject.getInt("TotalMark")));
                //  model.setDate(jsonObject.getString("Date"));
                resultModelList.add(model);
//                        YoutubeVideos recent = new YoutubeVideos();
//                        recent.setId(jsonObject.getString("ID"));
//                        String video = jsonObject.getString("URL");
//                        recent.setVideoUrl(video);
//
//                        // recent.setVideoUrl("<iframe width=\"100%\" height=\"100%\" src= \""+video+"\" frameborder=\"0\" allowfullscreen></iframe>");
//
//                        //   recent.setVideoUrl("<iframe width=100%height=100%src="+jsonObject.getString("URL")+" frameborder=\"0\" allowfullscreen></iframe>");
//                        recent.setVideotitle(jsonObject.getString("Title"));
//                        recent.setImage(jsonObject.getString("Image"));
//                        recent.setDate(jsonObject.getString("Date"));
                //  youtubeVideosList.add(recent);
            }

            Log.d("data", "onPostExecute: " + exams.size());

            if (resultModelList.size() == 0) {
                norecord.setVisibility(View.VISIBLE);
                result_lay.setVisibility(View.GONE);
                gradelayout.setVisibility(View.GONE);
            } else {
                norecord.setVisibility(View.GONE);
                result_lay.setVisibility(View.VISIBLE);
                gradelayout.setVisibility(View.VISIBLE);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recycle_class.setLayoutManager(mLayoutManager);
                ResultAdapter adapter = new ResultAdapter(resultModelList, getActivity());
                recycle_class.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            // adapter;

//            ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_layout, R.id.spinner_text, exams);
//            examsSpinner.setAdapter(adapter);

            //  videoAdapter = new VideoAdapter(VideoActivity.this, youtubeVideosList);
//                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VideoActivity.this);
//                    recyclerView.setLayoutManager(mLayoutManager);
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setAdapter(videoAdapter);
            System.out.println("success");
        } catch (JSONException ex) {

        }
    }
}

