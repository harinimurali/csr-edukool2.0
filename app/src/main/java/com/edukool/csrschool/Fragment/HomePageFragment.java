package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Activity.VideoActivity;
import com.edukool.csrschool.Adapter.CircularHomeAdapter;
import com.edukool.csrschool.Adapter.GridViewadapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.edukool.csrschool.models.CircularsModels;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomePageFragment extends Fragment {
    RecyclerView lv;
    //    static String[] values = {"Attendance", "Recent Activities",
//            "Assignment", "Results", "Videos", "Pay Fees",};
    String gd[];
    GridView gridview;
    private CircularHomeAdapter mAdapter;
    private List<CircularsModels> circularsModels;
    WS_CallService service_Login;
    List<ChildrenProfile> profiles = new ArrayList<>();
    String studentid;
    ProgressBar progressBar;
    RelativeLayout layout;
    String ParentNumber, schoolid, versioncode, fathername, profileimage;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    List<ChildrenProfile> childrenProfiles = new ArrayList<>();
    ImageView drpDown;


    /*   int[] gridViewImageId = {
               R.drawable.attendance, R.drawable.recent_activities,
               R.drawable.assignments, R.drawable.results,
               R.drawable.videos, R.drawable.payfees,
               R.drawable.news, R.drawable.polls_icon,
               R.drawable.circular_icon, R.drawable.message,
               R.drawable.leave_icon,
               R.drawable.calendar, R.drawable.time_table,
               R.drawable.voice_message, R.drawable.gallery,
               R.drawable.exam_schedule, R.drawable.profile,
               R.drawable.live_stream, R.drawable.calendar,
               R.drawable.bus_tracking, R.drawable.about}; */
    int[] gridViewImageId = {
            R.drawable.home_attendance_icon,
            R.drawable.home_assignment_icon,
            R.drawable.home_results_icon,
            R.drawable.home_payfees_icon,
            R.drawable.home_leave_icon,
            R.drawable.home_timetable_icon,
            R.drawable.home_examschd_icon,
            R.drawable.home_recent_icon,
            R.drawable.home_news_icon,
            R.drawable.home_polls_icon,
            R.drawable.home_circulars_icon,
            R.drawable.home_contact_icon,
            R.drawable.home_message_icon,
            R.drawable.home_voice_icon,
            R.drawable.home_gallery_icon,
            R.drawable.home_videos_icon,
            R.drawable.home_livestrm_icon,
            R.drawable.home_date_calendar_icon,
            R.drawable.home_profile_icon,
            R.drawable.home_about_icon,
            R.drawable.home_bus_tracking_icon,};

    private String[] gridstring;


    public static HomePageFragment newInstance() {
        HomePageFragment homePageFragment = new HomePageFragment();
        return homePageFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (schoolid.equals("SC-001-LS")) {
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name));
        } else if(schoolid.equals("SC-002-LS")) {
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name_primary));
        }else{
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name));
        }
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        //  callAPI();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_home_page, null);
        lv = (RecyclerView) rootview.findViewById(R.id.list_view1);
        gridview = (GridView) rootview.findViewById(R.id.grid_view);
        stuname = rootview.findViewById(R.id.student_name);
        stuclass = rootview.findViewById(R.id.student_class);
        stuimage = rootview.findViewById(R.id.student_image);
        drpDown = rootview.findViewById(R.id.drpDown);
        gridstring = getResources().getStringArray(R.array.gridview);
        layout = rootview.findViewById(R.id.layout);
        progressBar = (ProgressBar) rootview.findViewById(R.id.progressIndicator);
        profiles = new ArrayList<>();
        profiles = AppPreferences.getchildrenProfile(getActivity());
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());
        if (schoolid.equals("SC-001-LS")) {
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name));
        } else if(schoolid.equals("SC-002-LS")) {
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name_primary));
        }else{
            ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name));
        }
        callAPI();
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        GsonBuilder gsonBuilder = new GsonBuilder();

        // This is the main class for using Gson. Gson is typically used by first constructing a Gson instance and then invoking toJson(Object) or fromJson(String, Class) methods on it.
        // Gson instances are Thread-safe so you can reuse them freely across multiple threads.

        Gson gson = new Gson();
        gson.toJson(childrenProfiles);

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
        studentid = profiles.get(AppPreferences.getSudentPosition(getActivity())).getID();


        if (getArguments() != null) {
            String mParam1 = getArguments().getString("birthday");
            // if (mParam1.equals("Happy Birthday")) {
            showBirthdayDialog(mParam1);
            //    }
        }
       /* for (int i = 0; i < jsonArray.length(); i++) {
            try {

                JSONObject object = jsonArray.getJSONObject(i);
                stuname.setText(object.getString("Name"));
                stuclass.setText(object.getString("Class"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }*/


        GridViewadapter adapter = new GridViewadapter(getActivity(), gridstring, gridViewImageId);
        gridview.setAdapter(adapter);
        /*GradientDrawable gdd = new GradientDrawable(
                GradientDrawable.Orientation.RIGHT_LEFT,
                new int[] {0x9c60d3,0xfd84ff});
        gdd.setCornerRadius(0f);
        layout.setBackgroundDrawable(gdd);*/


       /* if (Permission.checknetwork(getActivity())) {
            callAPI();
        }*/
        gd = getResources().getStringArray(R.array.gridview);
        adapter.setOnClickListen(new GridViewadapter.AddTouchListen() {


            public void onRemoveClick(int position) {
                switch (position) {
                    case 0:
                        //  startActivity(new Intent(getActivity(), Webview2.class));

                        Fragment fragment = new StudentAttendance();
                        replaceFragment(fragment);
                        MainActivity.title.setText(getResources().getString(R.string.attendance));

                        break;
                    case 1:
                        Fragment fragment2 = new Assignments();
                        replaceFragment(fragment2);
                        MainActivity.title.setText(getResources().getString(R.string.assignments));


                        break;

                    case 2:
                        Fragment fragment3 = new ResultFragment();
                        replaceFragment(fragment3);
                        MainActivity.title.setText(getResources().getString(R.string.results));


                        break;
                    case 3:
                        Fragment fragment4 = new PayFees();
                        replaceFragment(fragment4);
                        MainActivity.title.setText(getResources().getString(R.string.payfees));


                        break;
                    case 4:
                        Fragment fragment9 = new LeaveRequestFragment();
                        replaceFragment(fragment9);
                        MainActivity.title.setText(getResources().getString(R.string.leave));


                        break;
                    case 5:
                        Fragment fragment11 = new TimeTableFragment();
                        replaceFragment(fragment11);
                        MainActivity.title.setText(getResources().getString(R.string.timetable));


                        break;
                    case 6:
                        Fragment fragment14 = new ExamScheduleFragment();
                        replaceFragment(fragment14);
                        MainActivity.title.setText(getResources().getString(R.string.ecamschedule));


                        break;
                    case 7:

                        Fragment fragment1 = new RecentActivities();
                        replaceFragment(fragment1);
                        MainActivity.title.setText(getResources().getString(R.string.recentactivities));


                        break;
                    case 8:
                        Fragment fragment5 = new NewsFragment();
                        replaceFragment(fragment5);
                        MainActivity.title.setText(getResources().getString(R.string.news));

                        break;
                    case 9:
                        Fragment fragment6 = new Polls();
                        replaceFragment(fragment6);
                        MainActivity.title.setText(getResources().getString(R.string.polls));


                        break;
                    case 10:
                        Fragment fragment7 = new CircularsFragment();
                        replaceFragment(fragment7);
                        MainActivity.title.setText(getResources().getString(R.string.circulars));

                        break;
                    case 11:
                        Fragment fragment10 = new ContactFragment();
                        replaceFragment(fragment10);
                        MainActivity.title.setText(getResources().getString(R.string.contact));

                        break;


                    case 12:
                        Fragment fragment8 = new MessageFragment();
                        replaceFragment(fragment8);
                        MainActivity.title.setText(getResources().getString(R.string.message));


                        break;

                    case 13:
                        Fragment fragment12 = new VoiceFragment();
                        replaceFragment(fragment12);
                        MainActivity.title.setText(getResources().getString(R.string.voicemsg));


                        break;

                    case 14:
                        Fragment fragment13 = new GalleryFragment();
                        replaceFragment(fragment13);
                        MainActivity.title.setText(getResources().getString(R.string.gallery));


                        break;


                    case 15:
                        Intent i = new Intent(getActivity(), VideoActivity.class);
                        startActivity(i);

                        break;

                    case 16:
                        Fragment fragment16 = new LiveStreamFragment();
                        replaceFragment(fragment16);
                        MainActivity.title.setText(getResources().getString(R.string.livestream));


                        break;

                    case 17:
                        Fragment fragment17 = new CalendarFragment();
                        replaceFragment(fragment17);
                        MainActivity.title.setText(getResources().getString(R.string.calendar));


                        break;


                    case 18:
                        Fragment fragment15 = new ProfileFragment();
                        replaceFragment(fragment15);
                        MainActivity.title.setText(getResources().getString(R.string.profile));


                        break;


                    case 19:
                        Fragment fragment18 = new AboutFragment();
                        replaceFragment(fragment18);
                        MainActivity.title.setText(getResources().getString(R.string.about));

                        break;
                    case 20:

                        Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
                       /* Fragment fragment15 = new BusTrackingFragment();
                        replaceFragment(fragment15);
                        MainActivity.title.setText(getResources().getString(R.string.bustrack));*/


                        break;

                }
            }

        });

        // gridview.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, values));
        //  prepareMovieData();

        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });


        return rootview;

    }


    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
                studentid = profiles.get(AppPreferences.getSudentPosition(getActivity())).getID();

                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void showBirthdayDialog(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setIcon(R.drawable.birthday);
        builder.setTitle(Html.fromHtml("<font color='#000000'>Happy Birthday</font>"));
        builder.setMessage(Html.fromHtml("<font color='#000000'>" + msg + "</font>"));
        //  builder.setMessage(Html.fromHtml("<font color='#000000'>Today is your Child Birthday. Happy Birthday" + AppPreferences.getchildrenProfile(getActivity()).get(AppPreferences.getSudentPosition(getActivity())) + "</font>"));

        builder.setPositiveButton(Html.fromHtml("<font color='#000000'>OK</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Log.e("", "Yes");
                dialog.dismiss();
            }
        });
        /*builder.setNegativeButton(Html.fromHtml("<font color='#ffffff'>CANCEL</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Log.e("", "No");
            }
        });*/
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();

        // dialog.getWindow().setBackgroundDrawableResource(getResources().getDrawable(R.drawable.background_image));
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

/*
    public class Load_Circulars_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Circulars_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            circularsModels = new ArrayList<>();
            //  circularsModels = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    circularsModels.clear();
                    JSONArray array = jObj.getJSONArray("Circulars");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        CircularsModels recent = new CircularsModels();
                        recent.setId(jsonObject.getString("ID"));
                        recent.setContent(jsonObject.getString("Content").trim());
                        recent.setDate(jsonObject.getString("CreatedDate"));
                        recent.setAttachment(jsonObject.getString("Attachment"));
                        circularsModels.add(recent);
                    }
                    mAdapter = new CircularHomeAdapter(circularsModels);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    lv.setLayoutManager(mLayoutManager);
                    lv.setItemAnimator(new DefaultItemAnimator());
                    lv.setAdapter(mAdapter);

                    mAdapter.setOnClickListen(new CircularHomeAdapter.AddTouchListen() {

                        @Override
                        public void onTouchClick(int position) {
                            Fragment frag = new CircularsFragment();
                            replaceFragment(frag);
                            MainActivity.title.setText(getActivity().getResources().getString(R.string.circulars));
                        }
                    });
                    System.out.println("success");

                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }
*/
  /*  private void prepareMovieData() {


        RecentActivititesModels movie = new RecentActivititesModels("Farewell Party", "2 days party in school ","");
        assignmentLists.add(movie);

        movie = new RecentActivititesModels("Book Fair", "assignments_icon on Lessons","");
        assignmentLists.add(movie);

    }*/

    private void callAPI() {
        if (Permission.checknetwork(getActivity())) {
            try {
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                byte[] data;
                String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|password:par|Function:SplashScreen|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
                //  String login_str = "UserName:'" + schoolid + "'|parentId:'9994922081'|password:par|Function:ParentLogin|DeviceType:android|GCMKey:'"+fcmtoken+"'|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_Login_WS load_plan_list = new Load_Login_WS(getActivity(), login);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            childrenProfiles = new ArrayList<>();

            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONArray array = jObj.getJSONArray("Childrens");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        ChildrenProfile profile = new ChildrenProfile();
                        profile.setID(jsonObject.getString("ID"));
                        profile.setFatherName(jsonObject.getString("FatherName"));
                        fathername = jsonObject.getString("FatherName");
                        profileimage = jsonObject.getString("ProfileImage");
                        profile.setName(jsonObject.getString("Name"));
                        profile.setProfileImage(jsonObject.getString("ProfileImage"));
                        profile.setClassId(jsonObject.getString("ClassId"));
                        profile.setEncryptedStudID(jsonObject.getString("EncryptedStudID"));
                        profile.setClasses(jsonObject.getString("Class"));
                        profile.setSection(jsonObject.getString("Section"));
                        profile.setFees(jsonObject.getString("Fees"));
                        childrenProfiles.add(profile);
                    }
                    AppPreferences.setchildrenProfile(getActivity(), childrenProfiles);
                    AppPreferences.saveLoginData(getActivity(), true, fathername, profileimage,
                            childrenProfiles.get(0).getID(), childrenProfiles.get(0).getClassId(),
                            childrenProfiles.get(0).getClasses(), childrenProfiles.get(0).getSection(),
                            childrenProfiles.get(0).getFees(), ParentNumber, childrenProfiles.get(0).getEncryptedStudID());
                    AppPreferences.setParentName(getActivity(), fathername);
                    AppPreferences.setParentImgae(getActivity(), profileimage);
                    AppPreferences.setParentNumber(getActivity(), ParentNumber);
                    AppPreferences.setSchool_Name(getActivity(), schoolid);
                    AppPreferences.setStudentPosition(getActivity(), 0);
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);
                    Toast.makeText(getActivity(), "Please Register your number", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

}
