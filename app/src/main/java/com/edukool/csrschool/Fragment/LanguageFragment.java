package com.edukool.csrschool.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanguageFragment extends Fragment {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    String language = "ta";
    private MenuItem change;
    private RadioButton radioButtonEnglish, radioButtonTamil;
    private RadioGroup radio;
    private RadioButton eng1, tam1;
    Context ctx;
    private LanguageHelper mLanguageCode;
    private int MODE_PRIVATE;
    Button confirm;
    String ParentNumber, clicked = "0";


    public LanguageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        radio = (RadioGroup) view.findViewById(R.id.radiolanguage);
        radioButtonEnglish = (RadioButton) view.findViewById(R.id.english);
        radioButtonTamil = (RadioButton) view.findViewById(R.id.tamil);
        confirm = (Button) view.findViewById(R.id.confirm);
        String a = AppPreferences.getLanguage(getActivity());
        Log.e("get languageid", ">>" + a);
        if (a.isEmpty() || a.equals("1") || a.equals("0")) {
            setLangauge(1);
        } else {
            setLangauge(2);
        }
        // setLangauge(Integer.parseInt(AppPreferences.getLanguage(getActivity())));
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        // schoolid = AppPreferences.getSchool_Name(getActivity());
       /* sharedPreferences = getActivity().getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE);
        editor = sharedPreferences.edit();*/
        //editor = getActivity().getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE).edit();
//        final SharedPreferences sharedPreferences = PreferenceManager
//                .getDefaultSharedPreferences(getActivity());
        //  editor = sharedPreferences.edit();
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked.equals("1")) {
                    if (AppPreferences.getLanguage(getActivity()).equals("1")) {
                        // if (sharedPreferences.getInt(Constants.LANGUAGE_CHANGE, 0) == 0) {
                        LanguageHelper.setLanguage(getContext(), language);
                        Intent intent_english = new Intent(getActivity(), MainActivity.class);
                        intent_english.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent_english);
                    } else {
                        LanguageHelper.setLanguage(getContext(), language);
                        Intent intent_english = new Intent(getActivity(), MainActivity.class);
                        intent_english.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent_english);
                    }
                } else {
                    Intent intent_english = new Intent(getActivity(), MainActivity.class);
                    intent_english.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_english);
                }
                //   LanguageHelper.setLanguage(getContext(), language_icon);

            }
        });
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                switch (checkedId) {
                    case R.id.english:
                        // editor.putInt(Constants.LANGUAGE_CHANGE, 0).commit();
                        AppPreferences.setLanguage(getActivity(), 1);
                        language = "en";
                        clicked = "1";
                        //  Toast.makeText(getActivity(), "Successfully changed", Toast.LENGTH_SHORT).show();
                       /* Intent intent_english = new Intent(getActivity(), MainActivity.class);
                        intent_english.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent_english);*/
                        //  LanguageHelper.setLanguage(getContext(), language_icon);
                        break;
                    case R.id.tamil:
                        //  editor.putInt(Constants.LANGUAGE_CHANGE, 1).commit();
                        AppPreferences.setLanguage(getActivity(), 2);
                        language = "ta";
                        clicked = "1";

                     /*   Toast.makeText(getActivity(), "Successfully changed1", Toast.LENGTH_SHORT).show();
                        Intent intent_english1 = new Intent(getActivity(), MainActivity.class);
                        intent_english1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent_english1);*/
                        // LanguageHelper.setLanguage(getContext(), language_icon);
                        break;

                }
                //  setLangauge(sharedPreferences.getInt(Constants.LANGUAGE_CHANGE, 1));
            }
        });
//        setLangauge(sharedPreferences.getInt(Constants.LANGUAGE, 1));


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.language));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setLocale(String lang) {

        Resources res = getContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(language));
        // API 17+ only.
        res.updateConfiguration(conf, dm);
        Intent intent_homeacvity = new Intent(getContext(), MainActivity.class);
        intent_homeacvity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent_homeacvity);

    }

    //    public void setLocale(String lang) {
//        Locale myLocale = new Locale(lang);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
//        Intent refresh = new Intent(getContext(), MainActivity.class);
//        startActivity(refresh);
//
//    }
    //
    private void setLangauge(int selectedLanguage) {
        //  Log.e("shared pref ", "value" + sharedPreferences.getInt(Constants.LANGUAGE_CHANGE, 0));

        switch (selectedLanguage) {
            case 1:
                radioButtonEnglish.setChecked(true);
                break;
            case 2:
                radioButtonTamil.setChecked(true);
                break;
        }


    }
}
