package com.edukool.csrschool.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;
import com.edukool.csrschool.Utils.FontTextViewLight;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtpVerifyFragment extends Fragment implements
        ActivityCompat.OnRequestPermissionsResultCallback, VerificationListener {

    private static final String TAG = Verification.class.getSimpleName();
    private Verification mVerification;

    EditText otp;
    Button verify;
    FontTextViewLight otpphonenumber;
    FontTextViewSemibold resend_otp;
    ProgressBar progressBar;
    LinearLayout otplayout;
    String numb, schoolid, fathername, profileimage;
    List<ChildrenProfile> childrenProfiles;
    WS_CallService service_Login;
    String versioncode;
    ProgressDialog progDailog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otp_verification_fragment, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        otp = (EditText) view.findViewById(R.id.otp);
        verify = (Button) view.findViewById(R.id.verify);
        resend_otp = (FontTextViewSemibold) view.findViewById(R.id.resend_otp);
        otpphonenumber = (FontTextViewLight) view.findViewById(R.id.otpphonenumber);
        progressBar = (ProgressBar) view.findViewById(R.id.progressIndicator);
        otplayout = (LinearLayout) view.findViewById(R.id.linearlayout);
        versioncode = AppPreferences.getversioncode(getActivity());
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otp.getText().toString().length() == 0) {
                    otp.setError("Please Enter OTP");
                } else {
                    onSubmitClicked();
                }

            }
        });

        initiateVerification();
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                resend_otp.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                resend_otp.setClickable(false);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                resend_otp.setText("Resend OTP");
                resend_otp.setClickable(true);
            }

        }.start();

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateVerification();
                Toast.makeText(getActivity(), "OTP sent to your number", Toast.LENGTH_SHORT).show();
                new CountDownTimer(30000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        resend_otp.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                        resend_otp.setClickable(false);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        resend_otp.setText("Resend OTP");
                        resend_otp.setClickable(true);
                    }

                }.start();
            }
        });

    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
       /* if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
            hideProgressBar();
        } else {*/
        mVerification = SendOtpVerification.createSmsVerification
                (SendOtpVerification
                        .config(countryCode + phoneNumber)
                        .context(getContext())
                        .autoVerification(true)
                        .build(), this);
        mVerification.initiate();// finall build will be submitted on 22
        //  }
    }

/*
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your SMS to automatically verify your "
                        + "phone, you may disable the permission once you have been verified.", Toast.LENGTH_LONG).show();
            }
            //  enableInputField(true);
        }
        initiateVerificationAndSuppressPermissionCheck();
    }
*/

    void initiateVerification(boolean skipPermissionCheck) {
        //set user number in textview
        if (getArguments() != null) {
            numb = getArguments().getString(Constants.NUMBER);
            schoolid = getArguments().getString(Constants.SCHOOLID);
            if (numb != null) {
                otpphonenumber.setText("We have sent the verification code to " + "+91" + numb + ". Please Verify.");
                createVerification(numb, skipPermissionCheck, "+91");
            }
        }
    }

    @Override
    public void onInitiated(String response) {
        Log.d(TAG, "Initialized!" + response);
    }

    @Override
    public void onInitiationFailed(Exception paramException) {
        Log.e(TAG, "Verification initialization failed: " + paramException.getMessage());
        hideProgressBarAndShowMessage("Verification Failed");
    }

    @Override
    public void onVerified(String response) {
        // progressBar.setVisibility(View.GONE);
        // hideProgressBar();
        hideKeypad();
        if (Permission.checknetwork(getActivity())) {
            callAPI();
        }
      /*  AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("OTP Verified");
        builder.setMessage("Your phone number is Verified.");
        builder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
                dialog.dismiss();
            }
        });
        // Create the AlertDialog object and return it

        AlertDialog dialog = builder.create();
        dialog.show();*/
    }

    private void callAPI() {
        try {
            // showProgress();
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            String fcm_key = AppPreferences.getFcmToken(getActivity());

            byte[] data;
            String fcmtoken = AppPreferences.getFcmToken(getActivity());
            String login_str = "UserName:" + schoolid + "|parentId:" + numb + "|password:par|Function:ParentLogin|DeviceType:android|GCMKey:''|fcm_userkey:" + fcm_key + "|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            //  String login_str = "UserName:'" + schoolid + "'|parentId:'9994922081'|password:par|Function:ParentLogin|DeviceType:android|GCMKey:'"+fcmtoken+"'|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Login_WS load_plan_list = new Load_Login_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVerificationFailed(Exception paramException) {
        Log.e(TAG, "Verification initialization failed: " + paramException.getMessage());
        hideProgressBarAndShowMessage("Verification Failed");

    }

    void initiateVerification() {
        initiateVerification(false);
    }

    void initiateVerificationAndSuppressPermissionCheck() {
        initiateVerification(true);
    }

    public void onSubmitClicked() {
        String code = otp.getText().toString();
        if (!code.isEmpty()) {
            hideKeypad();
            if (mVerification != null) {
                mVerification.verify(code);
                progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
                progDailog.setCancelable(false);
                progDailog.show();
                ///  showProgress();
               /* TextView messageText = (TextView) findViewById(R.id.textView);
                messageText.setText("Verification in progress");*/
                // enableInputField(false);
            }
        }
    }

    private void hideKeypad() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    void hideProgressBarAndShowMessage(String message) {
        hideProgressBar();
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

       /* TextView messageText = (TextView) findViewById(R.id.textView);
        messageText.setText(message);*/
    }

    void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        otplayout.setVisibility(View.VISIBLE);
        verify.setVisibility(View.VISIBLE);
    }

    void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        otplayout.setVisibility(View.GONE);
        verify.setVisibility(View.GONE);
    }

    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            childrenProfiles = new ArrayList<>();

            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONArray array = jObj.getJSONArray("Childrens");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        ChildrenProfile profile = new ChildrenProfile();
                        profile.setID(jsonObject.getString("ID"));
                        profile.setFatherName(jsonObject.getString("FatherName"));
                        fathername = jsonObject.getString("FatherName");
                        profileimage = jsonObject.getString("ProfileImage");
                        profile.setName(jsonObject.getString("Name"));
                        profile.setProfileImage(jsonObject.getString("ProfileImage"));
                        profile.setClassId(jsonObject.getString("ClassId"));
                        profile.setEncryptedStudID(jsonObject.getString("EncryptedStudID"));
                        profile.setClasses(jsonObject.getString("Class"));
                        profile.setSection(jsonObject.getString("Section"));
                        profile.setFees(jsonObject.getString("Fees"));
                        childrenProfiles.add(profile);
                    }
                    AppPreferences.setchildrenProfile(getActivity(), childrenProfiles);
                    AppPreferences.saveLoginData(getActivity(), true, fathername, profileimage,
                            childrenProfiles.get(0).getID(), childrenProfiles.get(0).getClassId(),
                            childrenProfiles.get(0).getClasses(), childrenProfiles.get(0).getSection(), childrenProfiles.get(0).getFees(), "+91 " + numb, childrenProfiles.get(0).getEncryptedStudID());
                    AppPreferences.setParentName(getActivity(), fathername);
                    AppPreferences.setParentImgae(getActivity(), profileimage);
                    AppPreferences.setParentNumber(getActivity(), numb);
                    AppPreferences.setSchool_Name(getActivity(), schoolid);
                    AppPreferences.setStudentPosition(getActivity(), 0);

                    Intent LoginIntent = new Intent(getActivity(), MainActivity.class);
                    LoginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(LoginIntent);
                    getActivity().finish();
                    progDailog.dismiss();
                    // hideProgressBar();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");

                    //hideProgressBar();
                    progDailog.dismiss();
//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);
                    // hideProgressBar();
                    Toast.makeText(getActivity(), "Please Register your number", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }
}

