package com.edukool.csrschool.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Helper.LanguageHelper;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.Constants;
import com.edukool.csrschool.models.ChildrenProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/19/2018.
 */

public class AboutFragment extends Fragment {
    List<ChildrenProfile> childrenProfiles;
    String studentId;

    public AboutFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        studentId = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        if (Permission.checknetwork(getActivity())) {
            final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
            progDailog.setCancelable(false);
//terms_of_service_webview.getSettings().setJavaScriptEnabled(true);
            WebView webView = (WebView) view.findViewById(R.id.webview);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progDailog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progDailog.dismiss();
                }
            });
            if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                webView.loadUrl(Constants.About_english_url);
            }else{
                webView.loadUrl(Constants.About_tamil_url);
            }
        }
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.about));
    }
}

