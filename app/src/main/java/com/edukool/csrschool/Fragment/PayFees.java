package com.edukool.csrschool.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Activity.Payment;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.PayFeesAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.Permission;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayFees extends Fragment implements PaymentResultListener {

    List<ChildrenProfile> childrenProfiles;
    String studentid, current_date;
    private RecyclerView recyclerView;
    private Button but;
    private PayFeesAdapter mAdapter;
    ProgressBar progressBar;
    WS_CallService service_Login;
    String ParentNumber, schoolid, versioncode;
    Button paymentbtn;
    List<ChildrenProfile> profiles;
    List<ChildrenProfile> profilesList;
    FontTextViewSemibold nofee;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;


    public PayFees() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay_fees, container, false);

        TextView textview = (TextView) view.findViewById(R.id.text);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        paymentbtn = (Button) view.findViewById(R.id.payment_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progressIndicator);
        nofee = (FontTextViewSemibold) view.findViewById(R.id.no_fees);
        stuname = view.findViewById(R.id.student_name);
        stuclass = view.findViewById(R.id.student_class);
        stuimage = view.findViewById(R.id.student_image);
        drpDown = view.findViewById(R.id.drpDown);

        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),
                R.layout.spinner_layout, childrenProfiles);
        spinner.setAdapter(adapter);

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).error(R.drawable.student_default_icon).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        if (Permission.checknetwork(getActivity())) {
            callAPI();
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "set of child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle);


       /* // prepareMovieData();
        mAdapter = new PayFeesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/
        /*if (Permission.checknetwork(getActivity())) {
            callAPI();
        }*/
           /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */
        Checkout.preload(getActivity());
        paymentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Payment.class).putExtra("feesid", profiles.get(0).getFeesId()).putExtra("amount",
                        profiles.get(0).getFeesAmt()).putExtra("name", profiles.get(0).getName()).putExtra("studentid", studentid));
                // startPayment();
            }
        });
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return view;
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).error(R.drawable.student_default_icon).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                if (Permission.checknetwork(getActivity())) {
                    callAPI();
                }
                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void startPayment() {
        final Activity activity = getActivity();

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", profiles.get(0).getName());
            options.put("description", "Fees Payment");
            //You can omit the image option to fetch the image from dashboard
            //  options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", Integer.parseInt(profiles.get(0).getFeesAmt()) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", "");
            preFill.put("contact", ParentNumber);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getActivity().getResources().getString(R.string.payfees));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).error(R.drawable.student_default_icon).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
       /* if (Permission.checknetwork(getActivity())) {
            callAPI();
        }*/
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            String feesid = profiles.get(0).getFeesId();
            callAPIFeePaid(razorpayPaymentID, feesid);
            Toast.makeText(getActivity(), "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("paymentId", "razorpayPaymentID " + razorpayPaymentID);
        } catch (Exception e) {
            Log.e("payment ", "Exception in onPaymentSuccess", e);
        }
    }

    private void callAPIFeePaid(String razorpayPaymentID, String feesid) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            //  String schoolid = "SC-005-LS";
            // studentid = "1";
            byte[] data;
            String login_str = "UserName:" + schoolid + "TransactionId:" + razorpayPaymentID + "|FeesId:" + feesid + "|StudentId:" + studentid + "|Function:FeesPaymentSuccess|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.BASE_OS + "'";
            //String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:IsFeesPaid|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.BASE_OS + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_SuccessPayment_WS load_plan_list = new Load_SuccessPayment_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getActivity(), "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("payment", "Exception in onPaymentError", e);
        }
    }


    private void callAPI() {
        if (Permission.checknetwork(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
                // String schoolid = "SC-005-LS";
                // studentid = "1";
                byte[] data;
                //String login_str = "UserName:SC-001-LS|parentId:9994922081|studentId:"+studentid+"|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:IsFeesPaid|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
                data = login_str.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                Log.e("basecode", "" + base64_register);
                login.add(new BasicNameValuePair("WS", base64_register));
                Load_IsFeePaid_WS load_plan_list = new Load_IsFeePaid_WS(getContext(), login);
                load_plan_list.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class Load_IsFeePaid_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_IsFeePaid_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            profiles = new ArrayList<>();
            profilesList = new ArrayList<>();
            Log.e("jsonResponse", "Fee list" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equals("Success")) {
                    JSONArray array = jObj.getJSONArray("Childrens");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        ChildrenProfile child1 = new ChildrenProfile();
                        child1.setID(object.getString("ID"));
                        child1.setName(object.getString("Name"));
                        child1.setFatherName(object.getString("FatherName"));
                        child1.setProfileImage(object.getString("ProfileImage"));
                        child1.setClassId(object.getString("ClassId"));
                        child1.setClasses(object.getString("Class"));
                        child1.setSection(object.getString("Section"));
                        JSONArray jsonArray = object.getJSONArray("FeesDetails");
                        if (jsonArray.length() != 0) {
                            nofee.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject object1 = jsonArray.getJSONObject(j);
                                ChildrenProfile child = new ChildrenProfile();
                                if (object1.has("FeesId") && object1.has("FeesAmount") &&
                                        object1.has("FeeShowFrom") && object1.has("FeeShowTo") && object1.has("Fees")) {
                                    child.setFeesId(object1.getString("FeesId"));
                                    child.setFeesAmt(object1.getString("FeesAmount"));
                                    child.setFeestremname(object1.getString("Term"));
                                    child.setFeesShowFrm(object1.getString("FeeShowFrom"));
                                    child.setFeesShowTo(object1.getString("FeeShowTo"));
                                    child.setFeesstatus(object1.getString("Fees"));

                                }
                                profiles.add(child);
                            }
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            nofee.setVisibility(View.VISIBLE);

                        }
                        profilesList.add(child1);
                    }
                    if (profiles.size() != 0) {
                        nofee.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        mAdapter = new PayFeesAdapter(getActivity(), profiles);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.setAddTouchListen(new PayFeesAdapter.AddTouchListener() {
                            @Override
                            public void onTouchClick(int position) {
                                startActivity(new Intent(getActivity(), Payment.class).putExtra("feesid", profiles.get(position).getFeesId()).putExtra("amount",
                                        profiles.get(position).getFeesAmt()).putExtra("name", profilesList.get(0).getName()).putExtra("studentid", studentid));
                            }
                        });
                    } else {
                        nofee.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }

                } else {
                    nofee.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    public class Load_SuccessPayment_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_SuccessPayment_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            profiles = new ArrayList<>();
            Log.e("jsonResponse", "Fee list" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equals("Success")) {
                    paymentbtn.setVisibility(View.GONE);
                    nofee.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), jObj.getString("response"), Toast.LENGTH_SHORT).show();

                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

}


