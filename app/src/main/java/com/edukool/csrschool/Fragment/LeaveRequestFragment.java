package com.edukool.csrschool.Fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.edukool.csrschool.Activity.LeaveRequestListActivity;
import com.edukool.csrschool.Activity.MainActivity;
import com.edukool.csrschool.Adapter.CustomSpinnerAdapter;
import com.edukool.csrschool.Adapter.StudentListAdapter;
import com.edukool.csrschool.Helper.WS_CallService;
import com.edukool.csrschool.Helper.WebserviceUrl;
import com.edukool.csrschool.R;
import com.edukool.csrschool.Utils.AddTouchListen;
import com.edukool.csrschool.Utils.AppPreferences;
import com.edukool.csrschool.Utils.FontTextViewSemibold;
import com.edukool.csrschool.models.ChildrenProfile;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveRequestFragment extends Fragment {

    EditText fromdate, todate, reason;
    Button submit;
    FontTextViewSemibold clickhere;
    WS_CallService service_Login;
    Spinner spinner;
    List<ChildrenProfile> childrenProfiles;
    String studentid, ParentNumber, schoolid, versioncode;
    private int mYear, mMonth, mDay, mHour, mMinute;
    LinearLayout layout, layoutspinner;
    ProgressBar progressBar;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;
    RelativeLayout layoutspinner1;
    long timeInMilliseconds, timeInMillisecondsMax;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leave_request, container, false);
        fromdate = view.findViewById(R.id.fromdate);
        todate = view.findViewById(R.id.todate);
        reason = view.findViewById(R.id.reason);
        submit = view.findViewById(R.id.submit);
        clickhere = view.findViewById(R.id.clickhere);
        spinner = view.findViewById(R.id.spinner);
        layout = view.findViewById(R.id.layout);
        layoutspinner = view.findViewById(R.id.layoutspinner);
        layoutspinner1 = view.findViewById(R.id.layoutspinner1);
        progressBar = view.findViewById(R.id.progressIndicator);
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());

        clickhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LeaveRequestListActivity.class));
            }
        });

        stuname = view.findViewById(R.id.student_name);
        stuclass = view.findViewById(R.id.student_class);
        stuimage = view.findViewById(R.id.student_image);
        drpDown = view.findViewById(R.id.drpDown);


        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reasonstr = reason.getText().toString().trim();
                String fromd = fromdate.getText().toString().trim();
                String todat = todate.getText().toString().trim();
                String inputPattern = "dd-MM-yyyy";
                String outputPattern = "yyyy-MM-dd";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
                Date date = null;
                Date date1 = null;
                String str = null;
                String str1 = null;

                try {
                    date = inputFormat.parse(fromd);
                    date1 = inputFormat.parse(todat);
                    str = outputFormat.format(date);
                    str1 = outputFormat.format(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if (validate()) {
                    layout.setVisibility(View.GONE);
                    layoutspinner.setVisibility(View.GONE);
                    layoutspinner1.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    CallApi(str, str1, reasonstr);
                }
            }
        });
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.e("currendate", "" + mDay + "-" + (mMonth + 1) + "-" + mYear);
        fromdate.setText(mDay + "-" + (mMonth + 1) + "-" + mYear);
        String givenDateString = mDay + "-" + (mMonth + 1) + "-" + mYear;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                fromdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                String givenDateString = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");
                                try {
                                    Date mDate = sdf.parse(givenDateString);
                                    timeInMilliseconds = mDate.getTime();
                                    System.out.println("Date in milli :: " + timeInMilliseconds);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                if (timeInMillisecondsMax == 0) {

                } else {
                    datePickerDialog.getDatePicker().setMaxDate(timeInMillisecondsMax);
                }

            }
        });

        todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                todate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                String givenDateString = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");
                                try {
                                    Date mDate = sdf.parse(givenDateString);
                                    timeInMillisecondsMax = mDate.getTime();
                                    System.out.println("Date in milli :: " + timeInMillisecondsMax);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);

            }
        });


        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),
                R.layout.spinner_layout, childrenProfiles);


       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return view;
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back_arrow button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
                dialog.dismiss();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.leaverequest));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses() + "  " + childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getSection());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default).error(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();
        // fromdate.setText(null);
        todate.setText(null);
        todate.setError(null);
        reason.setText(null);
        reason.setError(null);
    }

    public boolean validate() {
        boolean validate = false;
        String todat = todate.getText().toString();
        if (fromdate.getText().toString().length() == 0) {
            fromdate.setError(getActivity().getResources().getString(R.string.enterreasn));
            validate = false;
        } else if (todat.length() == 0 || todat.isEmpty()) {
            todate.setError(getActivity().getResources().getString(R.string.enterreasn));
            validate = false;
        } else if (reason.getText().toString().length() == 0) {
            reason.setError(getActivity().getResources().getString(R.string.enterreasn));
            validate = false;
        } else {
            reason.setError(null);
            todate.setError(null);
            fromdate.setError(null);
            validate = true;
        }

        return validate;
    }

    private void CallApi(String fromd, String todat, String reasonstr) {
        try {

            ArrayList<NameValuePair> logout = new ArrayList<NameValuePair>();

            byte[] data;
            String login_str = "UserName:" + schoolid + "|student_id:" + studentid + "|subject:leave_icon request|message:" + reasonstr + "|from:" + fromd + "|to:" + todat + "|Function:LeaveApply|Device Type:Android|GCMKey:|DeviceID:" + Build.ID + "|AppID:1|IMEINumber:|AppVersion:" + versioncode + "|MACAddress:|OSVersion:" + Build.VERSION.SDK + "";
            // String login_str = "UserName:" + schoolid + "|Function:ParentLogout|DeviceType:android|GCMKey:''|DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            logout.add(new BasicNameValuePair("WS", base64_register));
            Load_Leave_WS load_plan_list = new Load_Leave_WS(getActivity(), logout);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class Load_Leave_WS extends AsyncTask<String, String, String> {
        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Leave_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            //  hideProgressBar();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    layout.setVisibility(View.VISIBLE);
                    layoutspinner.setVisibility(View.GONE);
                    layoutspinner1.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    showDialog(jObj.getString("response"), 0);
                    //Toast.makeText(getActivity(), jObj.getString("response"), Toast.LENGTH_SHORT).show();
                    todate.setText("");
                    fromdate.setText("");
                    reason.setText("");
                } else {
                    layout.setVisibility(View.VISIBLE);
                    layoutspinner.setVisibility(View.GONE);
                    layoutspinner1.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    showDialog("Leave Request Failed", 1);
                    //  Toast.makeText(getActivity(), jObj.getString("response"), Toast.LENGTH_SHORT).show();
                  /*  todate.setText("");
                    fromdate.setText("");
                    reason.setText("");*/
                }
            } catch (Exception e) {

            }
        }
    }

    public void showDialog(String msg, final int flag) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (flag == 0) {
                    startActivity(new Intent(getActivity(), LeaveRequestListActivity.class));
                } else {
                    dialog.dismiss();
                }

            }
        });
        // Create the AlertDialog object and return it

        AlertDialog dialog = builder.create();
        dialog.show();

    }

}
