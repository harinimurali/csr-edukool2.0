package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class YoutubeVideos {
    String videoUrl;
    private String videotitle;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String image;
    private String date;
    // private static int videoimage;

    public YoutubeVideos() {
    }

    public YoutubeVideos(String videoUrl, String videotitle, String shortdesc1, String shortdesc2) {
        this.videoUrl = videoUrl;
        this.videotitle = videotitle;
        this.image = shortdesc1;
        this.date = shortdesc2;
        //   this.videoimage=videoimage;


    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideotitle() {
        return videotitle;
    }

    public void setVideotitle(String videotitle) {
        this.videotitle = videotitle;
    }


//    public static int getVideoimage() {
//        return videoimage;
//    }
//
//    public void setVideoimage(int videoimage) {
//        this.videoimage= videoimage;
//    }
//}
}

