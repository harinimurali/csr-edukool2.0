package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */
public class LeaveRequestList{
    private String fromDate;
    private String teacherRemarks;
    private String subject;
    private String staffId;
    private String toDate;
    private String studentId;
    private String id;
    private String message;
    private String approvedStatus;

    public void setFromDate(String fromDate){
        this.fromDate = fromDate;
    }

    public String getFromDate(){
        return fromDate;
    }

    public void setTeacherRemarks(String teacherRemarks){
        this.teacherRemarks = teacherRemarks;
    }

    public String getTeacherRemarks(){
        return teacherRemarks;
    }

    public void setSubject(String subject){
        this.subject = subject;
    }

    public String getSubject(){
        return subject;
    }

    public void setStaffId(String staffId){
        this.staffId = staffId;
    }

    public String getStaffId(){
        return staffId;
    }

    public void setToDate(String toDate){
        this.toDate = toDate;
    }

    public String getToDate(){
        return toDate;
    }

    public void setStudentId(String studentId){
        this.studentId = studentId;
    }

    public String getStudentId(){
        return studentId;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setApprovedStatus(String approvedStatus){
        this.approvedStatus = approvedStatus;
    }

    public String getApprovedStatus(){
        return approvedStatus;
    }

    @Override
    public String toString(){
        return
                "LeaveRequestList{" +
                        "fromDate = '" + fromDate + '\'' +
                        ",teacherRemarks = '" + teacherRemarks + '\'' +
                        ",subject = '" + subject + '\'' +
                        ",staff_id = '" + staffId + '\'' +
                        ",toDate = '" + toDate + '\'' +
                        ",student_id = '" + studentId + '\'' +
                        ",id = '" + id + '\'' +
                        ",message = '" + message + '\'' +
                        ",approvedStatus = '" + approvedStatus + '\'' +
                        "}";
    }
}
