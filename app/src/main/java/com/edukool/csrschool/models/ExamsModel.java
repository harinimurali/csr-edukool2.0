package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class ExamsModel {

    String date,code,toatal,id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getToatal() {
        return toatal;
    }

    public void setToatal(String toatal) {
        this.toatal = toatal;
    }

    @Override
    public String toString() {
        return name;
    }
}
