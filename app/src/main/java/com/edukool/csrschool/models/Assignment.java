package com.edukool.csrschool.models;

import java.io.Serializable;

/**
 * Created by keerthana on 8/24/2018.
 */

public class Assignment implements Serializable {
    private String startDate;
    private String description;
    private String submissionType;
    private String classname;
    private String iD;
    private String endDate;
    private String subject;
    private String name;

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public void setSubmissionType(String submissionType){
        this.submissionType = submissionType;
    }

    public String getSubmissionType(){
        return submissionType;
    }

    public void setClass(String classname){
        this.classname = classname;
    }

    public String getClassname(){
        return classname;
    }

    public void setID(String iD){
        this.iD = iD;
    }

    public String getID(){
        return iD;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }

    public String getEndDate(){
        return endDate;
    }

    public void setSubject(String subject){
        this.subject = subject;
    }

    public String getSubject(){
        return subject;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String toString(){
        return
                "Assignment{" +
                        "startDate = '" + startDate + '\'' +
                        ",description = '" + description + '\'' +
                        ",submissionType = '" + submissionType + '\'' +
                        ",class = '" + classname + '\'' +
                        ",iD = '" + iD + '\'' +
                        ",endDate = '" + endDate + '\'' +
                        ",subject = '" + subject + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}
