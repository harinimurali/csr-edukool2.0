package com.edukool.csrschool.models;

import java.util.List;

public class EventResultItem{
	private String titleDescription;
	private String moduleType;
	private String eventDate;
	private String createdDate;
	private List<String> attachment;
	private String iD;

	public void setTitleDescription(String titleDescription){
		this.titleDescription = titleDescription;
	}

	public String getTitleDescription(){
		return titleDescription;
	}

	public void setModuleType(String moduleType){
		this.moduleType = moduleType;
	}

	public String getModuleType(){
		return moduleType;
	}

	public void setEventDate(String eventDate){
		this.eventDate = eventDate;
	}

	public String getEventDate(){
		return eventDate;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setAttachment(List<String> attachment){
		this.attachment = attachment;
	}

	public List<String> getAttachment(){
		return attachment;
	}

	public void setID(String iD){
		this.iD = iD;
	}

	public String getID(){
		return iD;
	}

	@Override
 	public String toString(){
		return 
			"EventResultItem{" + 
			"titleDescription = '" + titleDescription + '\'' + 
			",moduleType = '" + moduleType + '\'' + 
			",eventDate = '" + eventDate + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",attachment = '" + attachment + '\'' + 
			",iD = '" + iD + '\'' + 
			"}";
		}
}