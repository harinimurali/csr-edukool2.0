package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */
public class SchoolList {
    private String id;
    private String name;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "SchoolList{" +
                        "id = '" + id + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}
