package com.edukool.csrschool.models;

/**
 * Created by harini on 9/18/2018.
 */

public class ImageString {
    public String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
