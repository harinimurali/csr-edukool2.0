package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class NotificationModels {
    private String content;
    private String createdDate;
    private String attachment;
    private String type;
    private String iD;
    private String name;
    private String description;
    private String subject;
    private String date;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getID() {
        return iD;
    }

   /* @Override
    public String toString(){
        return
                "NotificationModels{" +
                        "content = '" + content + '\'' +
                        ",createdDate = '" + createdDate + '\'' +
                        ",attachment = '" + attachment + '\'' +
                        ",iD = '" + iD + '\'' +
                        "}";
    }*/
}
