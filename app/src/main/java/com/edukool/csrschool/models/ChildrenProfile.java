package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */
public class ChildrenProfile {
    private String profileImage;
    private String classId;
    private String classes;
    private String iD;
    private String fees;
    private String section;
    private String name;
    private String fatherName;
    private String feesId;
    private String feesAmt;

    public String getFeestremname() {
        return feestremname;
    }

    public void setFeestremname(String feestremname) {
        this.feestremname = feestremname;
    }

    private String feestremname;

    public String getEncryptedStudID() {
        return EncryptedStudID;
    }

    public void setEncryptedStudID(String encryptedStudID) {
        EncryptedStudID = encryptedStudID;
    }

    private String EncryptedStudID;


    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getFeesId() {
        return feesId;
    }

    public void setFeesId(String feesId) {
        this.feesId = feesId;
    }

    public String getFeesAmt() {
        return feesAmt;
    }

    public void setFeesAmt(String feesAmt) {
        this.feesAmt = feesAmt;
    }

    public String getFeesShowFrm() {
        return feesShowFrm;
    }

    public void setFeesShowFrm(String feesShowFrm) {
        this.feesShowFrm = feesShowFrm;
    }

    public String getFeesShowTo() {
        return feesShowTo;
    }

    public void setFeesShowTo(String feesShowTo) {
        this.feesShowTo = feesShowTo;
    }

    private String feesShowFrm;
    private String feesShowTo;



    public String getFeesstatus() {
        return feesstatus;
    }

    public void setFeesstatus(String feesstatus) {
        this.feesstatus = feesstatus;
    }

    private String feesstatus;

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClasses(String Class) {
        this.classes = Class;
    }

    public String getClasses() {
        return classes;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getID() {
        return iD;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getFees() {
        return fees;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection() {
        return section;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getFatherName() {
        return fatherName;
    }

    @Override
    public String toString() {
        return
                "ChildrenProfile{" +
                        "profileImage = '" + profileImage + '\'' +
                        ",classId = '" + classId + '\'' +
                        ",class = '" + classes + '\'' +
                        ",iD = '" + iD + '\'' +
                        ",fees = '" + fees + '\'' +
                        ",section = '" + section + '\'' +
                        ",name = '" + name + '\'' +
                        ",fatherName = '" + fatherName + '\'' +
                        "}";
    }
}
