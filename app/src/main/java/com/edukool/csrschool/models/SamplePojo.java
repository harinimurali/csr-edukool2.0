package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */
public class SamplePojo {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
}