package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class PollsList {

    private String ID, name, description, start_date, end_date;
    //class1;
    // private RadioGroup radio;
    //   private RadioButton like,dislike;


    public PollsList() {
    }

    public PollsList(String ID, String name, String description, String start_date, String end_date) {
        this.ID = ID;
        this.name = name;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;


        //  this.class1 = class1;
        //  this.radio=radio;
        //this.comment = comment;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }


//        public String getClass1() {
//            return class1;
//        }
//
//        public void setClass1(String class1) {
//            this.class1 = class1;
//        }
//    public RadioGroup getRadio() {
//        return radio;
//    }
//
//    public void setRadio(RadioGroup radio) {
//        this.radio = radio;
//    }

//        public String getComment() {
//            return comment;
//        }
//
//        public void setComment(String comment) {
//            this.comment = comment;
//        }
//    public RadioButton getLike() {
//        return like;
//    }
//
//    public void setLike(RadioButton like) {
//        this.like = like;
//    }
//    public RadioButton getDislike() {
//        return dislike;
//    }
//
//    public void setDislike(RadioButton dislike) {
//        this.dislike = dislike;
//    }
}
