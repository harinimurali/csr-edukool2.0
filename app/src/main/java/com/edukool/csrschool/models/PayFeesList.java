package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */
public class PayFeesList {
    private String pay1, pay2, pay3;

    public PayFeesList() {
    }

    public PayFeesList(String pay1, String pay2, String pay3) {
        this.pay1 = pay1;
        this.pay2 = pay2;
        this.pay3 = pay3;
    }

    public String getPay1() {
        return pay1;
    }

    public void setPay1(String pay1) {
        this.pay1 = pay1;
    }

    public String getPay2() {
        return pay2;
    }

    public void setPay2(String pay2) {
        this.pay2 = pay2;
    }

    public String getPay3() {
        return pay3;
    }

    public void setPay3(String pay3) {
        this.pay3 = pay3;
    }
}
