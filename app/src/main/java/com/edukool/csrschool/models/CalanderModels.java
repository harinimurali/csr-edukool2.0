package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class CalanderModels {
    private String date;
    private String present;

    public String getTotalworkingdays() {
        return totalworkingdays;
    }

    public void setTotalworkingdays(String totalworkingdays) {
        this.totalworkingdays = totalworkingdays;
    }

    private String totalworkingdays;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

   public static class MonthlyHolidays {
        private String id, eventname, eventstartdate, eventenddate, createdate, modifieddate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEventname() {
            return eventname;
        }

        public void setEventname(String eventname) {
            this.eventname = eventname;
        }

        public String getEventstartdate() {
            return eventstartdate;
        }

        public void setEventstartdate(String eventstartdate) {
            this.eventstartdate = eventstartdate;
        }

        public String getEventenddate() {
            return eventenddate;
        }

        public void setEventenddate(String eventenddate) {
            this.eventenddate = eventenddate;
        }

        public String getCreatedate() {
            return createdate;
        }

        public void setCreatedate(String createdate) {
            this.createdate = createdate;
        }

        public String getModifieddate() {
            return modifieddate;
        }

        public void setModifieddate(String modifieddate) {
            this.modifieddate = modifieddate;
        }
    }
}