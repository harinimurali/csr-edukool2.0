package com.edukool.csrschool.models;

/**
 * Created by keerthana on 8/24/2018.
 */

public class RecentActivititesModels {

    private String id;
    private String activity;
    private String title;
    private String subject;
    private String classname;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RecentActivititesModels() {
    }

  /*  public RecentActivititesModels(String subject, String classes, String time) {
        this.subject = subject;
        this.classes = classes;
        this.time = time;
    }*/

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}